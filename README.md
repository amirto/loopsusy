LoopSUSY
====================================

This package produces the analysis ntuples starting from MelAnalysis outputs

Setup
------------------------------------

This package is based on EventLoop.
In order to use it, you need first to set up your work area and checkout the code:

```bash
mkdir workarea
cd workarea
mkdir build
mkdir source
cd source
kinit username@CERN.CH
git clone https://:@gitlab.cern.ch:8443/amirto/loopsusy.git
```
On CERN machines, the kinit command is called with:
```bash
kinit username
```

Then, to compile:

```bash
cd ../build
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup AnalysisBase,21.2.49,here
cmake ../source
make
```

and you are ready to go!

