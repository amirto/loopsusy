#define myhistos_cxx
#include "LoopSUSYCore/myhistos.h"

myhistos::myhistos() {
    TH1F* histoele = new TH1F();
    histoele->SetDefaultSumw2();
    TH1F* histomu = new TH1F();
    histomu->SetDefaultSumw2();
    TH1F* histoall = new TH1F();
    histoall->SetDefaultSumw2();

    myhistvec.push_back(histoall);
    myhistvec.push_back(histoele);
    myhistvec.push_back(histomu);
}

myhistos::myhistos(std::string title, int nbins, double xmin, double xmax, bool sinlep, bool sin_plus_dilep) {

    if (!sinlep) {
        TH1F* histoos = new TH1F((title + "OS").c_str(), "", nbins, xmin, xmax);
        histoos->SetDefaultSumw2();
        TH1F* histoss = new TH1F((title + "SS").c_str(), "", nbins, xmin, xmax);
        histoss->SetDefaultSumw2();
        TH1F* histoeeos = new TH1F((title + "eeOS").c_str(), "", nbins, xmin, xmax);
        histoeeos->SetDefaultSumw2();
        TH1F* histoemos = new TH1F((title + "emOS").c_str(), "", nbins, xmin, xmax);
        histoemos->SetDefaultSumw2();
        TH1F* histommos = new TH1F((title + "mmOS").c_str(), "", nbins, xmin, xmax);
        histommos->SetDefaultSumw2();

        TH1F* histoeess = new TH1F((title + "eeSS").c_str(), "", nbins, xmin, xmax);
        histoeess->SetDefaultSumw2();
        TH1F* histoemss = new TH1F((title + "emSS").c_str(), "", nbins, xmin, xmax);
        histoemss->SetDefaultSumw2();
        TH1F* histommss = new TH1F((title + "mmSS").c_str(), "", nbins, xmin, xmax);
        histommss->SetDefaultSumw2();


        myhistvec.push_back(histoos);
        myhistvec.push_back(histoss);
        myhistvec.push_back(histoeeos);
        myhistvec.push_back(histoeess);
        myhistvec.push_back(histoemos);
        myhistvec.push_back(histoemss);
        myhistvec.push_back(histommos);
        myhistvec.push_back(histommss);

        if (sin_plus_dilep) {
            TH1F* histoe = new TH1F((title + "ELE").c_str(), "", nbins, xmin, xmax);
            histoe->SetDefaultSumw2();
            TH1F* histom = new TH1F((title + "MU").c_str(), "", nbins, xmin, xmax);
            histom->SetDefaultSumw2();
            TH1F* histoall = new TH1F((title + "ALL").c_str(), "", nbins, xmin, xmax);
            histoall->SetDefaultSumw2();
            myhistvec.push_back(histoall);
            myhistvec.push_back(histoe);
            myhistvec.push_back(histom);
        }
    }
    else {
        TH1F* histoele = new TH1F((title + "ELE").c_str(), "", nbins, xmin, xmax);
        histoele->SetDefaultSumw2();
        TH1F* histomu = new TH1F((title + "MU").c_str(), "", nbins, xmin, xmax);
        histomu->SetDefaultSumw2();
        TH1F* histoall = new TH1F((title + "ALL").c_str(), "", nbins, xmin, xmax);
        histoall->SetDefaultSumw2();

        myhistvec.push_back(histoall);
        myhistvec.push_back(histoele);
        myhistvec.push_back(histomu);
    }
}

myhistos::myhistos(std::string title, int nbins, const float *xbins, bool sinlep) {

    if (!sinlep) {
        TH1F* histoos = new TH1F((title + "OS").c_str(), "", nbins, xbins);
        histoos->SetDefaultSumw2();
        TH1F* histoss = new TH1F((title + "SS").c_str(), "", nbins, xbins);
        histoss->SetDefaultSumw2();
        TH1F* histoeeos = new TH1F((title + "eeOS").c_str(), "", nbins, xbins);
        histoeeos->SetDefaultSumw2();
        TH1F* histoemos = new TH1F((title + "emOS").c_str(), "", nbins, xbins);
        histoemos->SetDefaultSumw2();
        TH1F* histommos = new TH1F((title + "mmOS").c_str(), "", nbins, xbins);
        histommos->SetDefaultSumw2();

        TH1F* histoeess = new TH1F((title + "eeSS").c_str(), "", nbins, xbins);
        histoeess->SetDefaultSumw2();
        TH1F* histoemss = new TH1F((title + "emSS").c_str(), "", nbins, xbins);
        histoemss->SetDefaultSumw2();
        TH1F* histommss = new TH1F((title + "mmSS").c_str(), "", nbins, xbins);
        histommss->SetDefaultSumw2();

        myhistvec.push_back(histoos);
        myhistvec.push_back(histoss);
        myhistvec.push_back(histoeeos);
        myhistvec.push_back(histoeess);
        myhistvec.push_back(histoemos);
        myhistvec.push_back(histoemss);
        myhistvec.push_back(histommos);
        myhistvec.push_back(histommss);

    }
    else {
        TH1F* histoele = new TH1F((title + "ELE").c_str(), "", nbins, xbins);
        histoele->SetDefaultSumw2();
        TH1F* histomu = new TH1F((title + "MU").c_str(), "", nbins, xbins);
        histomu->SetDefaultSumw2();
        TH1F* histoall = new TH1F((title + "ALL").c_str(), "", nbins, xbins);
        histoall->SetDefaultSumw2();

        myhistvec.push_back(histoall);
        myhistvec.push_back(histoele);
        myhistvec.push_back(histomu);
    }
}

myhistos::myhistos(std::string title, int nxbins, double xmin, double xmax, int nybins, double ymin, double ymax, bool sinlep) {

    if (!sinlep) {
        TH2F* histoos = new TH2F((title + "OS").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histoos->SetDefaultSumw2();
        TH2F* histoss = new TH2F((title + "SS").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histoss->SetDefaultSumw2();
        TH2F* histoeeos = new TH2F((title + "eeOS").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histoeeos->SetDefaultSumw2();
        TH2F* histoemos = new TH2F((title + "emOS").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histoemos->SetDefaultSumw2();
        TH2F* histommos = new TH2F((title + "mmOS").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histommos->SetDefaultSumw2();

        TH2F* histoeess = new TH2F((title + "eeSS").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histoeess->SetDefaultSumw2();
        TH2F* histoemss = new TH2F((title + "emSS").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histoemss->SetDefaultSumw2();
        TH2F* histommss = new TH2F((title + "mmSS").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histommss->SetDefaultSumw2();

        myhistvec2D.push_back(histoos);
        myhistvec2D.push_back(histoss);
        myhistvec2D.push_back(histoeeos);
        myhistvec2D.push_back(histoeess);
        myhistvec2D.push_back(histoemos);
        myhistvec2D.push_back(histoemss);
        myhistvec2D.push_back(histommos);
        myhistvec2D.push_back(histommss);
    }
    else {
        TH2F* histoele = new TH2F((title + "ELE").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histoele->SetDefaultSumw2();
        TH2F* histomu = new TH2F((title + "MU").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histomu->SetDefaultSumw2();
        TH2F* histoall = new TH2F((title + "ALL").c_str(), "", nxbins, xmin, xmax, nybins, ymin, ymax);
        histoall->SetDefaultSumw2();

        myhistvec2D.push_back(histoall);
        myhistvec2D.push_back(histoele);
        myhistvec2D.push_back(histomu);
    }
}

myhistos::myhistos(std::string title, int nbins, double xmin, double xmax, int lep_multiplicity) {

    TH1F* histo_all = new TH1F((title + "all").c_str(), "", nbins, xmin, xmax);
    histo_all->SetDefaultSumw2();
    myhistvec.push_back(histo_all);

    if (lep_multiplicity == 1) {
        TH1F* histoe = new TH1F((title + "ELE").c_str(), "", nbins, xmin, xmax);
        histoe->SetDefaultSumw2();
        myhistvec.push_back(histoe);
        TH1F* histom = new TH1F((title + "MU").c_str(), "", nbins, xmin, xmax);
        histom->SetDefaultSumw2();
        myhistvec.push_back(histom);
    }
    else if (lep_multiplicity == 2) {
        TH1F* histoos = new TH1F((title + "OS").c_str(), "", nbins, xmin, xmax);
        histoos->SetDefaultSumw2();
        myhistvec.push_back(histoos);
        TH1F* histoss = new TH1F((title + "SS").c_str(), "", nbins, xmin, xmax);
        histoss->SetDefaultSumw2();
        myhistvec.push_back(histoss);
        TH1F* histoeeos = new TH1F((title + "eeOS").c_str(), "", nbins, xmin, xmax);
        histoeeos->SetDefaultSumw2();
        myhistvec.push_back(histoeeos);
        TH1F* histoemos = new TH1F((title + "emOS").c_str(), "", nbins, xmin, xmax);
        histoemos->SetDefaultSumw2();
        myhistvec.push_back(histoemos);
        TH1F* histommos = new TH1F((title + "mmOS").c_str(), "", nbins, xmin, xmax);
        histommos->SetDefaultSumw2();
        myhistvec.push_back(histommos);

        TH1F* histoeess = new TH1F((title + "eeSS").c_str(), "", nbins, xmin, xmax);
        histoeess->SetDefaultSumw2();
        myhistvec.push_back(histoeess);
        TH1F* histoemss = new TH1F((title + "emSS").c_str(), "", nbins, xmin, xmax);
        histoemss->SetDefaultSumw2();
        myhistvec.push_back(histoemss);
        TH1F* histommss = new TH1F((title + "mmSS").c_str(), "", nbins, xmin, xmax);
        histommss->SetDefaultSumw2();
        myhistvec.push_back(histommss);
    }
    else if (lep_multiplicity == 3) {
        TH1F* histoee_e = new TH1F((title + "OSee_e").c_str(), "", nbins, xmin, xmax);
        histoee_e->SetDefaultSumw2();
        myhistvec.push_back(histoee_e);
        TH1F* histoee_m = new TH1F((title + "OSee_m").c_str(), "", nbins, xmin, xmax);
        histoee_m->SetDefaultSumw2();
        myhistvec.push_back(histoee_m);
        TH1F* histomm_e = new TH1F((title + "OSmm_e").c_str(), "", nbins, xmin, xmax);
        histomm_e->SetDefaultSumw2();
        myhistvec.push_back(histomm_e);
        TH1F* histomm_m = new TH1F((title + "OSmm_m").c_str(), "", nbins, xmin, xmax);
        histomm_m->SetDefaultSumw2();
        myhistvec.push_back(histomm_m);
    }

}


myhistos::myhistos(std::string title, int nbins, const float *xbins, int lep_multiplicity) {

    TH1F* histo_all = new TH1F((title + "all").c_str(), "", nbins, xbins);
    histo_all->SetDefaultSumw2();
    myhistvec.push_back(histo_all);

    if (lep_multiplicity == 1) {
        TH1F* histoe = new TH1F((title + "ELE").c_str(), "", nbins, xbins);
        histoe->SetDefaultSumw2();
        myhistvec.push_back(histoe);
        TH1F* histom = new TH1F((title + "MU").c_str(), "", nbins, xbins);
        histom->SetDefaultSumw2();
        myhistvec.push_back(histom);
    }
    else if (lep_multiplicity == 2) {
        TH1F* histoos = new TH1F((title + "OS").c_str(), "", nbins, xbins);
        histoos->SetDefaultSumw2();
        myhistvec.push_back(histoos);
        TH1F* histoss = new TH1F((title + "SS").c_str(), "", nbins, xbins);
        histoss->SetDefaultSumw2();
        myhistvec.push_back(histoss);
        TH1F* histoeeos = new TH1F((title + "eeOS").c_str(), "", nbins, xbins);
        histoeeos->SetDefaultSumw2();
        myhistvec.push_back(histoeeos);
        TH1F* histoemos = new TH1F((title + "emOS").c_str(), "", nbins, xbins);
        histoemos->SetDefaultSumw2();
        myhistvec.push_back(histoemos);
        TH1F* histommos = new TH1F((title + "mmOS").c_str(), "", nbins, xbins);
        histommos->SetDefaultSumw2();
        myhistvec.push_back(histommos);

        TH1F* histoeess = new TH1F((title + "eeSS").c_str(), "", nbins, xbins);
        histoeess->SetDefaultSumw2();
        myhistvec.push_back(histoeess);
        TH1F* histoemss = new TH1F((title + "emSS").c_str(), "", nbins, xbins);
        histoemss->SetDefaultSumw2();
        myhistvec.push_back(histoemss);
        TH1F* histommss = new TH1F((title + "mmSS").c_str(), "", nbins, xbins);
        histommss->SetDefaultSumw2();
        myhistvec.push_back(histommss);
    }
    else if (lep_multiplicity == 3) {
        TH1F* histoee_e = new TH1F((title + "OSee_e").c_str(), "", nbins, xbins);
        histoee_e->SetDefaultSumw2();
        myhistvec.push_back(histoee_e);
        TH1F* histoee_m = new TH1F((title + "OSee_m").c_str(), "", nbins, xbins);
        histoee_m->SetDefaultSumw2();
        myhistvec.push_back(histoee_m);
        TH1F* histomm_e = new TH1F((title + "OSmm_e").c_str(), "", nbins, xbins);
        histomm_e->SetDefaultSumw2();
        myhistvec.push_back(histomm_e);
        TH1F* histomm_m = new TH1F((title + "OSmm_m").c_str(), "", nbins, xbins);
        histomm_m->SetDefaultSumw2();
        myhistvec.push_back(histomm_m);
    }

}

myhistos::~myhistos() { }

void myhistos::Fill(int ibin, double value, double weight) {
    myhistvec[ibin]->Fill(value, weight);
}

void myhistos::Fill(int ibin, double xvalue, double yvalue, double weight) {
    myhistvec2D[ibin]->Fill(xvalue, yvalue, weight);
}

void myhistos::Write() {

    std::vector<TH1F*>::const_iterator hit = myhistvec.begin();
    for ( ; hit != myhistvec.end(); hit++) {
        (*hit)->Write();
    }

}

void myhistos::Write2D() {

    std::vector<TH2F*>::const_iterator hit = myhistvec2D.begin();
    for ( ; hit != myhistvec2D.end(); hit++) {
        (*hit)->Write();
    }

}

