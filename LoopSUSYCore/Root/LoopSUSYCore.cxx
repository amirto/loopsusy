#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/Job.h>


#include <LoopSUSYCore/LoopSUSYCore.h>
#include <LoopSUSYCore/myhistos.h>

#include <string>

#include "TFile.h"
#include "TH1D.h"

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(LoopSUSYCore)

LoopSUSYCore :: LoopSUSYCore ()
{
// Here you put any code for the base initialization of variables,
// e.g. initialize all pointers to 0.Note that you should only put
// the most basic initialization here, since this method will be
// called on both the submission and the worker node.Most of your
// initialization code will go into histInitialize() and
// initialize().

//Info("LoopSUSYCore()","Constructor called");

}


EL::StatusCode LoopSUSYCore::changeInput(bool firstFile)
{
// Here you do everything you need to do when we change input files,
// e.g. resetting branch addresses ontrees.If you are using
// D3PDReader or a similar service this method is not needed.
const char *APP_NAME = "changeInput()";
/*Info( APP_NAME, "Setting up branches for syst: %s", m_syst.c_str());

if (firstFile) {
	 ChannelNumber = (int)wk()->metaData()->castDouble("mc_chan");
	 Info( APP_NAME, "This is mc_chan %d", ChannelNumber);

	 if (ChannelNumber == -1) {
		m_isData = true;
		doFakes = false;
	 } else if (ChannelNumber == -5) {
		m_isData = true;
		doFakes = true;
	 }
	 else {
		m_isData = false;
		doFakes = false;
	 }
}*/

// Set object pointer
   TrigMatch_2e24_lhvloose_nod0 = 0;
   TrigMatch_mu22_mu8noL1 = 0;
   TrigMatch_e17_lhloose_nod0_mu14 = 0;
   TrigMatch_2e15_lhvloose_nod0_L12EM13VH = 0;
   TrigMatch_2e17_lhvloose_nod0 = 0;
   TrigMatch_mu20_mu8noL1 = 0;
   TrigMatch_2e12_lhloose_L12EM10VH = 0;
   TrigMatch_mu18_mu8noL1 = 0;
   TrigMatch_e17_lhloose_mu14 = 0;

   labeljets_reco = 0;
   ptjets = 0;
   etajets = 0;
   phijets = 0;
   massjets = 0;
   isB85jets = 0;
   BtagWeightjets = 0;
   JVTjets = 0;
   passORjet = 0;
   etaleptons = 0;
   ptleptons = 0;
   massleptons = 0;
   ptvarcone30leptons = 0;
   ptvarcone20leptons = 0;
   topoetcone20leptons = 0;
   phileptons = 0;
   flavlep = 0;
   Originlep = 0;
   Typelep = 0;
   bkgTruthType = 0;
   bkgTruthOrigin = 0;
   bkgMotherPdgId = 0;
   firstEgMotherTruthType = 0;
   firstEgMotherTruthOrigin = 0;
   firstEgMotherPdgId = 0;
   passORlep = 0;
   isSignallep = 0;
   isHighPtlep = 0;
   isTightlep = 0;
   LepIsoFixedCutLoose = 0;
   LepIsoFixedCutTight = 0;
   LepIsoFixedCutTightTrackOnly = 0;
   LepIsoGradient = 0;
   LepIsoGradientLoose = 0;
   LepIsoLoose = 0;
   LepIsoLooseTrackOnly = 0;
   LepIsoFCLoose = 0;
   d0sig = 0;
   z0sinTheta = 0;
   

//Set branch addresses and branch pointers
   tree = wk()->tree();
   if (m_isData) {
   	tree->SetBranchAddress("TrigMatch_2e24_lhvloose_nod0", &TrigMatch_2e24_lhvloose_nod0, &b_TrigMatch_2e24_lhvloose_nod0);
   	tree->SetBranchAddress("TrigMatch_mu22_mu8noL1", &TrigMatch_mu22_mu8noL1, &b_TrigMatch_mu22_mu8noL1);
   	tree->SetBranchAddress("TrigMatch_e17_lhloose_nod0_mu14", &TrigMatch_e17_lhloose_nod0_mu14, &b_TrigMatch_e17_lhloose_nod0_mu14);
   	tree->SetBranchAddress("TrigMatch_2e15_lhvloose_nod0_L12EM13VH", &TrigMatch_2e15_lhvloose_nod0_L12EM13VH, &b_TrigMatch_2e15_lhvloose_nod0_L12EM13VH);
   	tree->SetBranchAddress("TrigMatch_2e17_lhvloose_nod0", &TrigMatch_2e17_lhvloose_nod0, &b_TrigMatch_2e17_lhvloose_nod0);
   	tree->SetBranchAddress("TrigMatch_mu20_mu8noL1", &TrigMatch_mu20_mu8noL1, &b_TrigMatch_mu20_mu8noL1);
   	tree->SetBranchAddress("TrigMatch_2e12_lhloose_L12EM10VH", &TrigMatch_2e12_lhloose_L12EM10VH, &b_TrigMatch_2e12_lhloose_L12EM10VH);
   	tree->SetBranchAddress("TrigMatch_mu18_mu8noL1", &TrigMatch_mu18_mu8noL1, &b_TrigMatch_mu18_mu8noL1);
   	tree->SetBranchAddress("TrigMatch_e17_lhloose_mu14", &TrigMatch_e17_lhloose_mu14, &b_TrigMatch_e17_lhloose_mu14);
   }
   
   tree->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   tree->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   tree->SetBranchAddress("GenFiltMET", &GenFiltMET, &b_GenFiltMET);
   tree->SetBranchAddress("GenFiltHT", &GenFiltHT, &b_GenFiltHT);
   tree->SetBranchAddress("LumiBlockNumber", &LumiBlockNumber, &b_LumiBlock);
   tree->SetBranchAddress("xsec", &xsec, &b_xsec);
   tree->SetBranchAddress("WeightEvents", &WeightEvents, &b_WeightEvents);
   tree->SetBranchAddress("WeightEventsPU", &WeightEventsPU, &b_WeightEventsPU);
   tree->SetBranchAddress("WeightEventselSF", &WeightEventselSF, &b_WeightEventselSF);
   tree->SetBranchAddress("WeightEventsmuSF", &WeightEventsmuSF, &b_WeightEventsmuSF);
   tree->SetBranchAddress("WeightEventsSF_global", &WeightEventsSF_global, &b_WeightEventsSF_global);
   tree->SetBranchAddress("WeightEvents_trigger_global", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global);
   tree->SetBranchAddress("WeightEventsbTag", &WeightEventsbTag, &b_WeightEventsbTag);
   tree->SetBranchAddress("WeightEventsJVT", &WeightEventsJVT, &b_WeightEventsJVT);
   tree->SetBranchAddress("AverageInteractionsPerCrossing", &AverageInteractionsPerCrossing, &b_AverageInteractionsPerCrossing);
   tree->SetBranchAddress("NumPrimaryVertices", &NumPrimaryVertices, &b_NumPrimaryVertices);
   tree->SetBranchAddress("DecayType", &DecayType, &b_DecayType);
   tree->SetBranchAddress("HFclass", &HFclass, &b_HFclass);
   tree->SetBranchAddress("passMETtrig", &passMETtrig, &b_passMETtrig);
   tree->SetBranchAddress("TruthPDGID1", &TruthPDGID1, &b_TruthPDGID1);
   tree->SetBranchAddress("TruthPDGID2", &TruthPDGID2, &b_TruthPDGID2);
   tree->SetBranchAddress("TruthPDFID1", &TruthPDFID1, &b_TruthPDFID1);
   tree->SetBranchAddress("TruthPDFID2", &TruthPDFID2, &b_TruthPDFID2);
   tree->SetBranchAddress("TruthX1", &TruthX1, &b_TruthX1);
   tree->SetBranchAddress("TruthX2", &TruthX2, &b_TruthX2);
   tree->SetBranchAddress("TruthXF1", &TruthXF1, &b_TruthXF1);
   tree->SetBranchAddress("TruthXF2", &TruthXF2, &b_TruthXF2);
   tree->SetBranchAddress("TruthQ", &TruthQ, &b_TruthQ);
   tree->SetBranchAddress("labeljets_reco", &labeljets_reco, &b_labeljets_reco);
   tree->SetBranchAddress("ptjets", &ptjets, &b_ptjets);
   tree->SetBranchAddress("etajets", &etajets, &b_etajets);
   tree->SetBranchAddress("phijets", &phijets, &b_phijets);
   tree->SetBranchAddress("massjets", &massjets, &b_massjets);
   tree->SetBranchAddress("isB85jets", &isB85jets, &b_isB85jets);
   tree->SetBranchAddress("BtagWeightjets", &BtagWeightjets, &b_BtagWeightjets);
   tree->SetBranchAddress("JVTjets", &JVTjets, &b_JVTjets);
   tree->SetBranchAddress("passORjet", &passORjet, &b_passORjet);
   tree->SetBranchAddress("etaleptons", &etaleptons, &b_etaleptons);
   tree->SetBranchAddress("ptleptons", &ptleptons, &b_ptleptons);
   tree->SetBranchAddress("massleptons", &massleptons, &b_massleptons);
   tree->SetBranchAddress("ptvarcone30leptons", &ptvarcone30leptons, &b_ptvarcone30leptons);
   tree->SetBranchAddress("ptvarcone20leptons", &ptvarcone20leptons, &b_ptvarcone20leptons);
   tree->SetBranchAddress("topoetcone20leptons", &topoetcone20leptons, &b_topoetcone20leptons);
   tree->SetBranchAddress("phileptons", &phileptons, &b_phileptons);
   tree->SetBranchAddress("flavlep", &flavlep, &b_flavlep);
   tree->SetBranchAddress("Originlep", &Originlep, &b_Originlep);
   tree->SetBranchAddress("Typelep", &Typelep, &b_Typelep);
   tree->SetBranchAddress("bkgTruthType", &bkgTruthType, &b_bkgTruthType);
   tree->SetBranchAddress("bkgTruthOrigin", &bkgTruthOrigin, &b_bkgTruthOrigin);
   tree->SetBranchAddress("bkgMotherPdgId", &bkgMotherPdgId, &b_bkgMotherPdgId);
   tree->SetBranchAddress("firstEgMotherTruthType", &firstEgMotherTruthType, &b_firstEgMotherTruthType);
   tree->SetBranchAddress("firstEgMotherTruthOrigin", &firstEgMotherTruthOrigin, &b_firstEgMotherTruthOrigin);
   tree->SetBranchAddress("firstEgMotherPdgId", &firstEgMotherPdgId, &b_firstEgMotherPdgId);
   tree->SetBranchAddress("passORlep", &passORlep, &b_passORlep);
   tree->SetBranchAddress("isSignallep", &isSignallep, &b_isSignallep);
   tree->SetBranchAddress("isHighPtlep", &isHighPtlep, &b_isHighPtlep);
   tree->SetBranchAddress("isTightlep", &isTightlep, &b_isTightlep);
   tree->SetBranchAddress("LepIsoFixedCutLoose", &LepIsoFixedCutLoose, &b_LepIsoFixedCutLoose);
   tree->SetBranchAddress("LepIsoFixedCutTight", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight);
   tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly);
   tree->SetBranchAddress("LepIsoGradient", &LepIsoGradient, &b_LepIsoGradient);
   tree->SetBranchAddress("LepIsoGradientLoose", &LepIsoGradientLoose, &b_LepIsoGradientLoose);
   tree->SetBranchAddress("LepIsoLoose", &LepIsoLoose, &b_LepIsoLoose);
   tree->SetBranchAddress("LepIsoLooseTrackOnly", &LepIsoLooseTrackOnly, &b_LepIsoLooseTrackOnly);
   tree->SetBranchAddress("LepIsoFCLoose", &LepIsoFCLoose, &b_LepIsoFCLoose);
   tree->SetBranchAddress("d0sig", &d0sig, &b_d0sig);
   tree->SetBranchAddress("z0sinTheta", &z0sinTheta, &b_z0sinTheta);
   tree->SetBranchAddress("cleaningVeto", &cleaningVeto, &b_cleaningVeto);
   tree->SetBranchAddress("EtMiss_tstPhi", &EtMiss_tstPhi, &b_EtMiss_tstPhi);
   tree->SetBranchAddress("EtMiss_tst", &EtMiss_tst, &b_EtMiss_tst);
   tree->SetBranchAddress("EtMiss_ElePhi", &EtMiss_ElePhi, &b_EtMiss_ElePhi);
   tree->SetBranchAddress("EtMiss_Ele", &EtMiss_Ele, &b_EtMiss_Ele);
   tree->SetBranchAddress("EtMiss_MuPhi", &EtMiss_MuPhi, &b_EtMiss_MuPhi);
   tree->SetBranchAddress("EtMiss_Mu", &EtMiss_Mu, &b_EtMiss_Mu);
   tree->SetBranchAddress("EtMiss_JetPhi", &EtMiss_JetPhi, &b_EtMiss_JetPhi);
   tree->SetBranchAddress("EtMiss_Jet", &EtMiss_Jet, &b_EtMiss_Jet);
   tree->SetBranchAddress("EtMiss_GamPhi", &EtMiss_GamPhi, &b_EtMiss_GamPhi);
   tree->SetBranchAddress("EtMiss_Gam", &EtMiss_Gam, &b_EtMiss_Gam);
   tree->SetBranchAddress("EtMiss_SoftPhi", &EtMiss_SoftPhi, &b_EtMiss_SoftPhi);
   tree->SetBranchAddress("EtMiss_Soft", &EtMiss_Soft, &b_EtMiss_Soft);
   tree->SetBranchAddress("EtMiss_significance", &EtMiss_significance, &b_EtMiss_significance);
   tree->SetBranchAddress("Etmiss_PVSoftTrkPhi", &Etmiss_PVSoftTrkPhi, &b_Etmiss_PVSoftTrkPhi);
   tree->SetBranchAddress("Etmiss_PVSoftTrk", &Etmiss_PVSoftTrk, &b_Etmiss_PVSoftTrk);
   tree->SetBranchAddress("HLT_2e12_lhloose_L12EM10VH", &HLT_2e12_lhloose_L12EM10VH, &b_HLT_2e12_lhloose_L12EM10VH);
   tree->SetBranchAddress("HLT_2e15_lhvloose_nod0_L12EM13VH", &HLT_2e15_lhvloose_nod0_L12EM13VH, &b_HLT_2e15_lhvloose_nod0_L12EM13VH);
   tree->SetBranchAddress("HLT_2e17_lhvloose_nod0", &HLT_2e17_lhvloose_nod0, &b_HLT_2e17_lhvloose_nod0);
   tree->SetBranchAddress("HLT_2e17_lhvloose_nod0_L12EM15VHI", &HLT_2e17_lhvloose_nod0_L12EM15VHI, &b_HLT_2e17_lhvloose_nod0_L12EM15VHI);
   tree->SetBranchAddress("HLT_2e24_lhvloose_nod0", &HLT_2e24_lhvloose_nod0, &b_HLT_2e24_lhvloose_nod0);
   tree->SetBranchAddress("HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH", &HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH, &b_HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH);
   tree->SetBranchAddress("HLT_e17_lhloose_mu14", &HLT_e17_lhloose_mu14, &b_HLT_e17_lhloose_mu14);
   tree->SetBranchAddress("HLT_e17_lhloose_nod0_mu14", &HLT_e17_lhloose_nod0_mu14, &b_HLT_e17_lhloose_nod0_mu14);
   tree->SetBranchAddress("HLT_mu22_mu8noL1", &HLT_mu22_mu8noL1, &b_HLT_mu22_mu8noL1);
   tree->SetBranchAddress("HLT_mu20_mu8noL1", &HLT_mu20_mu8noL1, &b_HLT_mu20_mu8noL1);
   tree->SetBranchAddress("HLT_mu18_mu8noL1", &HLT_mu18_mu8noL1, &b_HLT_mu18_mu8noL1);
   tree->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   tree->SetBranchAddress("HLT_e24_lhmedium_nod0_L1EM20VH", &HLT_e24_lhmedium_nod0_L1EM20VH, &b_HLT_e24_lhmedium_nod0_L1EM20VH);
   tree->SetBranchAddress("HLT_e24_lhtight_nod0_ivarloose", &HLT_e24_lhtight_nod0_ivarloose, &b_HLT_e24_lhtight_nod0_ivarloose);
   tree->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   tree->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   tree->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   tree->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   tree->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   tree->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   tree->SetBranchAddress("HLT_mu24_ivarloose", &HLT_mu24_ivarloose, &b_HLT_mu24_ivarloose);
   tree->SetBranchAddress("HLT_mu24_ivarloose_L1MU15", &HLT_mu24_ivarloose_L1MU15, &b_HLT_mu24_ivarloose_L1MU15);
   tree->SetBranchAddress("HLT_mu24_ivarmedium", &HLT_mu24_ivarmedium, &b_HLT_mu24_ivarmedium);
   tree->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   tree->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   tree->SetBranchAddress("HLT_xe70_mht", &HLT_xe70_mht, &b_HLT_xe70_mht);
   tree->SetBranchAddress("HLT_xe70_tc_lcw", &HLT_xe70_tc_lcw, &b_HLT_xe70_tc_lcw);
   tree->SetBranchAddress("HLT_xe80_tc_lcw_L1XE50", &HLT_xe80_tc_lcw_L1XE50, &b_HLT_xe80_tc_lcw_L1XE50);
   tree->SetBranchAddress("HLT_xe90_mht_L1XE50", &HLT_xe90_mht_L1XE50, &b_HLT_xe90_mht_L1XE50);
   tree->SetBranchAddress("HLT_xe100_mht_L1XE50", &HLT_xe100_mht_L1XE50, &b_HLT_xe100_mht_L1XE50);
   tree->SetBranchAddress("HLT_xe110_mht_L1XE50", &HLT_xe110_mht_L1XE50, &b_HLT_xe110_mht_L1XE50);
   
	if (systname=="EG_RESdown"){
   	tree->SetBranchAddress("cleaningVeto_EG_RESOLUTION_ALL__1down", &cleaningVeto, &b_cleaningVeto_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("ptleptons_EG_RESOLUTION_ALL__1down", &ptleptons, &b_ptleptons_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("etaleptons_EG_RESOLUTION_ALL__1down", &etaleptons, &b_etaleptons_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("phileptons_EG_RESOLUTION_ALL__1down", &phileptons, &b_phileptons_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("massleptons_EG_RESOLUTION_ALL__1down", &massleptons, &b_massleptons_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("flavleptons_EG_RESOLUTION_ALL__1down", &flavlep, &b_flavleptons_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("passORlep_EG_RESOLUTION_ALL__1down", &passORlep, &b_passORlep_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("isSignallep_EG_RESOLUTION_ALL__1down", &isSignallep, &b_isSignallep_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("Originlep_EG_RESOLUTION_ALL__1down", &Originlep, &b_Originlep_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("isTightlep_EG_RESOLUTION_ALL__1down", &isTightlep, &b_isTightlep_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("LepIsoFixedCutTight_EG_RESOLUTION_ALL__1down", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_EG_RESOLUTION_ALL__1down", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("LepIsoGradientLoose_EG_RESOLUTION_ALL__1down", &LepIsoGradientLoose, &b_LepIsoGradientLoose_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("d0sig_EG_RESOLUTION_ALL__1down", &d0sig, &b_d0sig_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("z0sinTheta_EG_RESOLUTION_ALL__1down", &z0sinTheta, &b_z0sinTheta_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("passORjet_EG_RESOLUTION_ALL__1down", &passORjet, &b_passORjet_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("WeightEventselSF_EG_RESOLUTION_ALL__1down", &WeightEventselSF, &b_WeightEventselSF_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("WeightEventsmuSF_EG_RESOLUTION_ALL__1down", &WeightEventsmuSF, &b_WeightEventsmuSF_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("WeightEventsSF_global_EG_RESOLUTION_ALL__1down", &WeightEventsSF_global, &b_WeightEventsSF_global_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("WeightEvents_trigger_global_EG_RESOLUTION_ALL__1down", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("EtMiss_tst_EG_RESOLUTION_ALL__1down", &EtMiss_tst, &b_EtMiss_tst_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("EtMiss_significance_EG_RESOLUTION_ALL__1down", &EtMiss_significance, &b_EtMiss_significance_EG_RESOLUTION_ALL__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_EG_RESOLUTION_ALL__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_EG_RESOLUTION_ALL__1down);	
	}
	if (systname=="EG_RESup"){
		tree->SetBranchAddress("cleaningVeto_EG_RESOLUTION_ALL__1up", &cleaningVeto, &b_cleaningVeto_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("ptleptons_EG_RESOLUTION_ALL__1up", &ptleptons, &b_ptleptons_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("etaleptons_EG_RESOLUTION_ALL__1up", &etaleptons, &b_etaleptons_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("phileptons_EG_RESOLUTION_ALL__1up", &phileptons, &b_phileptons_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("massleptons_EG_RESOLUTION_ALL__1up", &massleptons, &b_massleptons_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("flavleptons_EG_RESOLUTION_ALL__1up", &flavlep, &b_flavleptons_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("passORlep_EG_RESOLUTION_ALL__1up", &passORlep, &b_passORlep_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("isSignallep_EG_RESOLUTION_ALL__1up", &isSignallep, &b_isSignallep_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("Originlep_EG_RESOLUTION_ALL__1up", &Originlep, &b_Originlep_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("isTightlep_EG_RESOLUTION_ALL__1up", &isTightlep, &b_isTightlep_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("LepIsoFixedCutTight_EG_RESOLUTION_ALL__1up", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_EG_RESOLUTION_ALL__1up", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("LepIsoGradientLoose_EG_RESOLUTION_ALL__1up", &LepIsoGradientLoose, &b_LepIsoGradientLoose_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("d0sig_EG_RESOLUTION_ALL__1up", &d0sig, &b_d0sig_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("z0sinTheta_EG_RESOLUTION_ALL__1up", &z0sinTheta, &b_z0sinTheta_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("passORjet_EG_RESOLUTION_ALL__1up", &passORjet, &b_passORjet_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("WeightEventselSF_EG_RESOLUTION_ALL__1up", &WeightEventselSF, &b_WeightEventselSF_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("WeightEventsmuSF_EG_RESOLUTION_ALL__1up", &WeightEventsmuSF, &b_WeightEventsmuSF_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("WeightEventsSF_global_EG_RESOLUTION_ALL__1up", &WeightEventsSF_global, &b_WeightEventsSF_global_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("WeightEvents_trigger_global_EG_RESOLUTION_ALL__1up", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("EtMiss_tst_EG_RESOLUTION_ALL__1up", &EtMiss_tst, &b_EtMiss_tst_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("EtMiss_significance_EG_RESOLUTION_ALL__1up", &EtMiss_significance, &b_EtMiss_significance_EG_RESOLUTION_ALL__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_EG_RESOLUTION_ALL__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_EG_RESOLUTION_ALL__1up);
	}
	if (systname == "EG_SCALEup") {
      tree->SetBranchAddress("cleaningVeto_EG_SCALE_ALL__1up", &cleaningVeto, &b_cleaningVeto_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("ptleptons_EG_SCALE_ALL__1up", &ptleptons, &b_ptleptons_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("etaleptons_EG_SCALE_ALL__1up", &etaleptons, &b_etaleptons_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("phileptons_EG_SCALE_ALL__1up", &phileptons, &b_phileptons_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("massleptons_EG_SCALE_ALL__1up", &massleptons, &b_massleptons_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("flavleptons_EG_SCALE_ALL__1up", &flavlep, &b_flavleptons_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("passORlep_EG_SCALE_ALL__1up", &passORlep, &b_passORlep_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("isSignallep_EG_SCALE_ALL__1up", &isSignallep, &b_isSignallep_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("Originlep_EG_SCALE_ALL__1up", &Originlep, &b_Originlep_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("isTightlep_EG_SCALE_ALL__1up", &isTightlep, &b_isTightlep_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("LepIsoFixedCutTight_EG_SCALE_ALL__1up", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_EG_SCALE_ALL__1up", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("LepIsoGradientLoose_EG_SCALE_ALL__1up", &LepIsoGradientLoose, &b_LepIsoGradientLoose_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("d0sig_EG_SCALE_ALL__1up", &d0sig, &b_d0sig_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("z0sinTheta_EG_SCALE_ALL__1up", &z0sinTheta, &b_z0sinTheta_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("passORjet_EG_SCALE_ALL__1up", &passORjet, &b_passORjet_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("WeightEventselSF_EG_SCALE_ALL__1up", &WeightEventselSF, &b_WeightEventselSF_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("WeightEventsmuSF_EG_SCALE_ALL__1up", &WeightEventsmuSF, &b_WeightEventsmuSF_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("WeightEventsSF_global_EG_SCALE_ALL__1up", &WeightEventsSF_global, &b_WeightEventsSF_global_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("WeightEvents_trigger_global_EG_SCALE_ALL__1up", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("EtMiss_tst_EG_SCALE_ALL__1up", &EtMiss_tst, &b_EtMiss_tst_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("EtMiss_significance_EG_SCALE_ALL__1up", &EtMiss_significance, &b_EtMiss_significance_EG_SCALE_ALL__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_EG_SCALE_ALL__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_EG_SCALE_ALL__1up);
   }
   if (systname == "EG_SCALEdown") {
      tree->SetBranchAddress("cleaningVeto_EG_SCALE_ALL__1down", &cleaningVeto, &b_cleaningVeto_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("ptleptons_EG_SCALE_ALL__1down", &ptleptons, &b_ptleptons_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("etaleptons_EG_SCALE_ALL__1down", &etaleptons, &b_etaleptons_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("phileptons_EG_SCALE_ALL__1down", &phileptons, &b_phileptons_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("massleptons_EG_SCALE_ALL__1down", &massleptons, &b_massleptons_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("flavleptons_EG_SCALE_ALL__1down", &flavlep, &b_flavleptons_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("passORlep_EG_SCALE_ALL__1down", &passORlep, &b_passORlep_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("isSignallep_EG_SCALE_ALL__1down", &isSignallep, &b_isSignallep_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("Originlep_EG_SCALE_ALL__1down", &Originlep, &b_Originlep_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("isTightlep_EG_SCALE_ALL__1down", &isTightlep, &b_isTightlep_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("LepIsoFixedCutTight_EG_SCALE_ALL__1down", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_EG_SCALE_ALL__1down", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("LepIsoGradientLoose_EG_SCALE_ALL__1down", &LepIsoGradientLoose, &b_LepIsoGradientLoose_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("d0sig_EG_SCALE_ALL__1down", &d0sig, &b_d0sig_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("z0sinTheta_EG_SCALE_ALL__1down", &z0sinTheta, &b_z0sinTheta_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("passORjet_EG_SCALE_ALL__1down", &passORjet, &b_passORjet_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("WeightEventselSF_EG_SCALE_ALL__1down", &WeightEventselSF, &b_WeightEventselSF_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("WeightEventsmuSF_EG_SCALE_ALL__1down", &WeightEventsmuSF, &b_WeightEventsmuSF_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("WeightEventsSF_global_EG_SCALE_ALL__1down", &WeightEventsSF_global, &b_WeightEventsSF_global_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("WeightEvents_trigger_global_EG_SCALE_ALL__1down", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("EtMiss_tst_EG_SCALE_ALL__1down", &EtMiss_tst, &b_EtMiss_tst_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("EtMiss_significance_EG_SCALE_ALL__1down", &EtMiss_significance, &b_EtMiss_significance_EG_SCALE_ALL__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_EG_SCALE_ALL__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_EG_SCALE_ALL__1down);
   }
   if (systname == "JET_EtaIntercalibration_up") {
      tree->SetBranchAddress("cleaningVeto_JET_EtaIntercalibration_NonClosure__1up", &cleaningVeto, &b_cleaningVeto_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("ptjets_JET_EtaIntercalibration_NonClosure__1up", &ptjets, &b_ptjets_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("etajets_JET_EtaIntercalibration_NonClosure__1up", &etajets, &b_etajets_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("phijets_JET_EtaIntercalibration_NonClosure__1up", &phijets, &b_phijets_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("massjets_JET_EtaIntercalibration_NonClosure__1up", &massjets, &b_massjets_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("isB85jets_JET_EtaIntercalibration_NonClosure__1up", &isB85jets, &b_isB85jets_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("BtagWeightjets_JET_EtaIntercalibration_NonClosure__1up", &BtagWeightjets, &b_BtagWeightjets_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("JVTjets_JET_EtaIntercalibration_NonClosure__1up", &JVTjets, &b_JVTjets_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("passORjet_JET_EtaIntercalibration_NonClosure__1up", &passORjet, &b_passORjet_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("WeightEventsbTag_JET_EtaIntercalibration_NonClosure__1up", &WeightEventsbTag, &b_WeightEventsbTag_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("WeightEventsJVT_JET_EtaIntercalibration_NonClosure__1up", &WeightEventsJVT, &b_WeightEventsJVT_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("passORlep_JET_EtaIntercalibration_NonClosure__1up", &passORlep, &b_passORlep_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("EtMiss_tst_JET_EtaIntercalibration_NonClosure__1up", &EtMiss_tst, &b_EtMiss_tst_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("EtMiss_significance_JET_EtaIntercalibration_NonClosure__1up", &EtMiss_significance, &b_EtMiss_significance_JET_EtaIntercalibration_NonClosure__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_EtaIntercalibration_NonClosure__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_EtaIntercalibration_NonClosure__1up);
   }
   if (systname == "JET_EtaIntercalibration_down") {
      tree->SetBranchAddress("cleaningVeto_JET_EtaIntercalibration_NonClosure__1down", &cleaningVeto, &b_cleaningVeto_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("ptjets_JET_EtaIntercalibration_NonClosure__1down", &ptjets, &b_ptjets_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("etajets_JET_EtaIntercalibration_NonClosure__1down", &etajets, &b_etajets_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("phijets_JET_EtaIntercalibration_NonClosure__1down", &phijets, &b_phijets_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("massjets_JET_EtaIntercalibration_NonClosure__1down", &massjets, &b_massjets_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("isB85jets_JET_EtaIntercalibration_NonClosure__1down", &isB85jets, &b_isB85jets_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("BtagWeightjets_JET_EtaIntercalibration_NonClosure__1down", &BtagWeightjets, &b_BtagWeightjets_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("JVTjets_JET_EtaIntercalibration_NonClosure__1down", &JVTjets, &b_JVTjets_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("passORjet_JET_EtaIntercalibration_NonClosure__1down", &passORjet, &b_passORjet_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("WeightEventsbTag_JET_EtaIntercalibration_NonClosure__1down", &WeightEventsbTag, &b_WeightEventsbTag_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("WeightEventsJVT_JET_EtaIntercalibration_NonClosure__1down", &WeightEventsJVT, &b_WeightEventsJVT_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("passORlep_JET_EtaIntercalibration_NonClosure__1down", &passORlep, &b_passORlep_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("EtMiss_tst_JET_EtaIntercalibration_NonClosure__1down", &EtMiss_tst, &b_EtMiss_tst_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("EtMiss_significance_JET_EtaIntercalibration_NonClosure__1down", &EtMiss_significance, &b_EtMiss_significance_JET_EtaIntercalibration_NonClosure__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_EtaIntercalibration_NonClosure__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_EtaIntercalibration_NonClosure__1down);
   }
   if (systname == "JET_GroupedNP1up") {
      tree->SetBranchAddress("cleaningVeto_JET_GroupedNP_1__1up", &cleaningVeto, &b_cleaningVeto_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("ptjets_JET_GroupedNP_1__1up", &ptjets, &b_ptjets_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("etajets_JET_GroupedNP_1__1up", &etajets, &b_etajets_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("phijets_JET_GroupedNP_1__1up", &phijets, &b_phijets_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("massjets_JET_GroupedNP_1__1up", &massjets, &b_massjets_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("isB85jets_JET_GroupedNP_1__1up", &isB85jets, &b_isB85jets_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("BtagWeightjets_JET_GroupedNP_1__1up", &BtagWeightjets, &b_BtagWeightjets_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("JVTjets_JET_GroupedNP_1__1up", &JVTjets, &b_JVTjets_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("passORjet_JET_GroupedNP_1__1up", &passORjet, &b_passORjet_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("WeightEventsbTag_JET_GroupedNP_1__1up", &WeightEventsbTag, &b_WeightEventsbTag_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("WeightEventsJVT_JET_GroupedNP_1__1up", &WeightEventsJVT, &b_WeightEventsJVT_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("passORlep_JET_GroupedNP_1__1up", &passORlep, &b_passORlep_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("EtMiss_tst_JET_GroupedNP_1__1up", &EtMiss_tst, &b_EtMiss_tst_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("EtMiss_significance_JET_GroupedNP_1__1up", &EtMiss_significance, &b_EtMiss_significance_JET_GroupedNP_1__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_GroupedNP_1__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_GroupedNP_1__1up);
   }
   if (systname == "JET_GroupedNP1down") {
      tree->SetBranchAddress("cleaningVeto_JET_GroupedNP_1__1down", &cleaningVeto, &b_cleaningVeto_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("ptjets_JET_GroupedNP_1__1down", &ptjets, &b_ptjets_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("etajets_JET_GroupedNP_1__1down", &etajets, &b_etajets_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("phijets_JET_GroupedNP_1__1down", &phijets, &b_phijets_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("massjets_JET_GroupedNP_1__1down", &massjets, &b_massjets_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("isB85jets_JET_GroupedNP_1__1down", &isB85jets, &b_isB85jets_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("BtagWeightjets_JET_GroupedNP_1__1down", &BtagWeightjets, &b_BtagWeightjets_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("JVTjets_JET_GroupedNP_1__1down", &JVTjets, &b_JVTjets_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("passORjet_JET_GroupedNP_1__1down", &passORjet, &b_passORjet_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("WeightEventsbTag_JET_GroupedNP_1__1down", &WeightEventsbTag, &b_WeightEventsbTag_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("WeightEventsJVT_JET_GroupedNP_1__1down", &WeightEventsJVT, &b_WeightEventsJVT_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("passORlep_JET_GroupedNP_1__1down", &passORlep, &b_passORlep_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("EtMiss_tst_JET_GroupedNP_1__1down", &EtMiss_tst, &b_EtMiss_tst_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("EtMiss_significance_JET_GroupedNP_1__1down", &EtMiss_significance, &b_EtMiss_significance_JET_GroupedNP_1__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_GroupedNP_1__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_GroupedNP_1__1down); 
   }
   if (systname == "JET_GroupedNP2up") {
      tree->SetBranchAddress("cleaningVeto_JET_GroupedNP_2__1up", &cleaningVeto, &b_cleaningVeto_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("ptjets_JET_GroupedNP_2__1up", &ptjets, &b_ptjets_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("etajets_JET_GroupedNP_2__1up", &etajets, &b_etajets_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("phijets_JET_GroupedNP_2__1up", &phijets, &b_phijets_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("massjets_JET_GroupedNP_2__1up", &massjets, &b_massjets_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("isB85jets_JET_GroupedNP_2__1up", &isB85jets, &b_isB85jets_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("BtagWeightjets_JET_GroupedNP_2__1up", &BtagWeightjets, &b_BtagWeightjets_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("JVTjets_JET_GroupedNP_2__1up", &JVTjets, &b_JVTjets_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("passORjet_JET_GroupedNP_2__1up", &passORjet, &b_passORjet_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("WeightEventsbTag_JET_GroupedNP_2__1up", &WeightEventsbTag, &b_WeightEventsbTag_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("WeightEventsJVT_JET_GroupedNP_2__1up", &WeightEventsJVT, &b_WeightEventsJVT_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("passORlep_JET_GroupedNP_2__1up", &passORlep, &b_passORlep_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("EtMiss_tst_JET_GroupedNP_2__1up", &EtMiss_tst, &b_EtMiss_tst_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("EtMiss_significance_JET_GroupedNP_2__1up", &EtMiss_significance, &b_EtMiss_significance_JET_GroupedNP_2__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_GroupedNP_2__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_GroupedNP_2__1up);
   }
   if (systname == "JET_GroupedNP2down") {
      tree->SetBranchAddress("cleaningVeto_JET_GroupedNP_2__1down", &cleaningVeto, &b_cleaningVeto_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("ptjets_JET_GroupedNP_2__1down", &ptjets, &b_ptjets_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("etajets_JET_GroupedNP_2__1down", &etajets, &b_etajets_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("phijets_JET_GroupedNP_2__1down", &phijets, &b_phijets_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("massjets_JET_GroupedNP_2__1down", &massjets, &b_massjets_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("isB85jets_JET_GroupedNP_2__1down", &isB85jets, &b_isB85jets_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("BtagWeightjets_JET_GroupedNP_2__1down", &BtagWeightjets, &b_BtagWeightjets_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("JVTjets_JET_GroupedNP_2__1down", &JVTjets, &b_JVTjets_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("passORjet_JET_GroupedNP_2__1down", &passORjet, &b_passORjet_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("WeightEventsbTag_JET_GroupedNP_2__1down", &WeightEventsbTag, &b_WeightEventsbTag_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("WeightEventsJVT_JET_GroupedNP_2__1down", &WeightEventsJVT, &b_WeightEventsJVT_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("passORlep_JET_GroupedNP_2__1down", &passORlep, &b_passORlep_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("EtMiss_tst_JET_GroupedNP_2__1down", &EtMiss_tst, &b_EtMiss_tst_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("EtMiss_significance_JET_GroupedNP_2__1down", &EtMiss_significance, &b_EtMiss_significance_JET_GroupedNP_2__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_GroupedNP_2__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_GroupedNP_2__1down);   
   }
   if (systname == "JET_GroupedNP3up") {
      tree->SetBranchAddress("cleaningVeto_JET_GroupedNP_3__1up", &cleaningVeto, &b_cleaningVeto_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("ptjets_JET_GroupedNP_3__1up", &ptjets, &b_ptjets_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("etajets_JET_GroupedNP_3__1up", &etajets, &b_etajets_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("phijets_JET_GroupedNP_3__1up", &phijets, &b_phijets_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("massjets_JET_GroupedNP_3__1up", &massjets, &b_massjets_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("isB85jets_JET_GroupedNP_3__1up", &isB85jets, &b_isB85jets_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("BtagWeightjets_JET_GroupedNP_3__1up", &BtagWeightjets, &b_BtagWeightjets_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("JVTjets_JET_GroupedNP_3__1up", &JVTjets, &b_JVTjets_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("passORjet_JET_GroupedNP_3__1up", &passORjet, &b_passORjet_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("WeightEventsbTag_JET_GroupedNP_3__1up", &WeightEventsbTag, &b_WeightEventsbTag_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("WeightEventsJVT_JET_GroupedNP_3__1up", &WeightEventsJVT, &b_WeightEventsJVT_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("passORlep_JET_GroupedNP_3__1up", &passORlep, &b_passORlep_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("EtMiss_tst_JET_GroupedNP_3__1up", &EtMiss_tst, &b_EtMiss_tst_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("EtMiss_significance_JET_GroupedNP_3__1up", &EtMiss_significance, &b_EtMiss_significance_JET_GroupedNP_3__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_GroupedNP_3__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_GroupedNP_3__1up);   
   }
   if (systname == "JET_GroupedNP3down") {
      tree->SetBranchAddress("cleaningVeto_JET_GroupedNP_3__1down", &cleaningVeto, &b_cleaningVeto_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("ptjets_JET_GroupedNP_3__1down", &ptjets, &b_ptjets_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("etajets_JET_GroupedNP_3__1down", &etajets, &b_etajets_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("phijets_JET_GroupedNP_3__1down", &phijets, &b_phijets_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("massjets_JET_GroupedNP_3__1down", &massjets, &b_massjets_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("isB85jets_JET_GroupedNP_3__1down", &isB85jets, &b_isB85jets_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("BtagWeightjets_JET_GroupedNP_3__1down", &BtagWeightjets, &b_BtagWeightjets_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("JVTjets_JET_GroupedNP_3__1down", &JVTjets, &b_JVTjets_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("passORjet_JET_GroupedNP_3__1down", &passORjet, &b_passORjet_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("WeightEventsbTag_JET_GroupedNP_3__1down", &WeightEventsbTag, &b_WeightEventsbTag_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("WeightEventsJVT_JET_GroupedNP_3__1down", &WeightEventsJVT, &b_WeightEventsJVT_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("passORlep_JET_GroupedNP_3__1down", &passORlep, &b_passORlep_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("EtMiss_tst_JET_GroupedNP_3__1down", &EtMiss_tst, &b_EtMiss_tst_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("EtMiss_significance_JET_GroupedNP_3__1down", &EtMiss_significance, &b_EtMiss_significance_JET_GroupedNP_3__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_GroupedNP_3__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_GroupedNP_3__1down);   
   }
   if (systname == "JET_JER_DataVsMCup") {
      tree->SetBranchAddress("cleaningVeto_JET_JER_DataVsMC__1up", &cleaningVeto, &b_cleaningVeto_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("ptjets_JET_JER_DataVsMC__1up", &ptjets, &b_ptjets_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("etajets_JET_JER_DataVsMC__1up", &etajets, &b_etajets_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("phijets_JET_JER_DataVsMC__1up", &phijets, &b_phijets_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("massjets_JET_JER_DataVsMC__1up", &massjets, &b_massjets_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("isB85jets_JET_JER_DataVsMC__1up", &isB85jets, &b_isB85jets_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("BtagWeightjets_JET_JER_DataVsMC__1up", &BtagWeightjets, &b_BtagWeightjets_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("JVTjets_JET_JER_DataVsMC__1up", &JVTjets, &b_JVTjets_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("passORjet_JET_JER_DataVsMC__1up", &passORjet, &b_passORjet_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("WeightEventsbTag_JET_JER_DataVsMC__1up", &WeightEventsbTag, &b_WeightEventsbTag_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("WeightEventsJVT_JET_JER_DataVsMC__1up", &WeightEventsJVT, &b_WeightEventsJVT_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("passORlep_JET_JER_DataVsMC__1up", &passORlep, &b_passORlep_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("EtMiss_tst_JET_JER_DataVsMC__1up", &EtMiss_tst, &b_EtMiss_tst_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("EtMiss_significance_JET_JER_DataVsMC__1up", &EtMiss_significance, &b_EtMiss_significance_JET_JER_DataVsMC__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_JER_DataVsMC__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_JER_DataVsMC__1up);
   }
   if (systname == "JET_JER_DataVsMCdown") {
      tree->SetBranchAddress("cleaningVeto_JET_JER_DataVsMC__1down", &cleaningVeto, &b_cleaningVeto_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("ptjets_JET_JER_DataVsMC__1down", &ptjets, &b_ptjets_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("etajets_JET_JER_DataVsMC__1down", &etajets, &b_etajets_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("phijets_JET_JER_DataVsMC__1down", &phijets, &b_phijets_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("massjets_JET_JER_DataVsMC__1down", &massjets, &b_massjets_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("isB85jets_JET_JER_DataVsMC__1down", &isB85jets, &b_isB85jets_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("BtagWeightjets_JET_JER_DataVsMC__1down", &BtagWeightjets, &b_BtagWeightjets_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("JVTjets_JET_JER_DataVsMC__1down", &JVTjets, &b_JVTjets_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("passORjet_JET_JER_DataVsMC__1down", &passORjet, &b_passORjet_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("WeightEventsbTag_JET_JER_DataVsMC__1down", &WeightEventsbTag, &b_WeightEventsbTag_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("WeightEventsJVT_JET_JER_DataVsMC__1down", &WeightEventsJVT, &b_WeightEventsJVT_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("passORlep_JET_JER_DataVsMC__1down", &passORlep, &b_passORlep_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("EtMiss_tst_JET_JER_DataVsMC__1down", &EtMiss_tst, &b_EtMiss_tst_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("EtMiss_significance_JET_JER_DataVsMC__1down", &EtMiss_significance, &b_EtMiss_significance_JET_JER_DataVsMC__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_JET_JER_DataVsMC__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_JET_JER_DataVsMC__1down);
   }
   if (systname == "MET_SoftTrk_ResoPara") {
      tree->SetBranchAddress("cleaningVeto_MET_SoftTrk_ResoPara", &cleaningVeto, &b_cleaningVeto_MET_SoftTrk_ResoPara);
      tree->SetBranchAddress("EtMiss_tst_MET_SoftTrk_ResoPara", &EtMiss_tst, &b_EtMiss_tst_MET_SoftTrk_ResoPara);
      tree->SetBranchAddress("EtMiss_significance_MET_SoftTrk_ResoPara", &EtMiss_significance, &b_EtMiss_significance_MET_SoftTrk_ResoPara);
      tree->SetBranchAddress("EtMiss_tstPhi_MET_SoftTrk_ResoPara", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MET_SoftTrk_ResoPara);   
   }
   if (systname == "MET_SoftTrk_ResoPerp") {
      tree->SetBranchAddress("cleaningVeto_MET_SoftTrk_ResoPerp", &cleaningVeto, &b_cleaningVeto_MET_SoftTrk_ResoPerp);
      tree->SetBranchAddress("EtMiss_tst_MET_SoftTrk_ResoPerp", &EtMiss_tst, &b_EtMiss_tst_MET_SoftTrk_ResoPerp);
      tree->SetBranchAddress("EtMiss_significance_MET_SoftTrk_ResoPerp", &EtMiss_significance, &b_EtMiss_significance_MET_SoftTrk_ResoPerp);
      tree->SetBranchAddress("EtMiss_tstPhi_MET_SoftTrk_ResoPerp", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MET_SoftTrk_ResoPerp);   
   }
   if (systname == "MET_SoftTrk_ScaleDown") {
      tree->SetBranchAddress("cleaningVeto_MET_SoftTrk_ScaleDown", &cleaningVeto, &b_cleaningVeto_MET_SoftTrk_ScaleDown);
      tree->SetBranchAddress("EtMiss_tst_MET_SoftTrk_ScaleDown", &EtMiss_tst, &b_EtMiss_tst_MET_SoftTrk_ScaleDown);
      tree->SetBranchAddress("EtMiss_significance_MET_SoftTrk_ScaleDown", &EtMiss_significance, &b_EtMiss_significance_MET_SoftTrk_ScaleDown);
      tree->SetBranchAddress("EtMiss_tstPhi_MET_SoftTrk_ScaleDown", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MET_SoftTrk_ScaleDown);   
   }
   if (systname == "MET_SoftTrk_ScaleUp") {
      tree->SetBranchAddress("cleaningVeto_MET_SoftTrk_ScaleUp", &cleaningVeto, &b_cleaningVeto_MET_SoftTrk_ScaleUp);
      tree->SetBranchAddress("EtMiss_tst_MET_SoftTrk_ScaleUp", &EtMiss_tst, &b_EtMiss_tst_MET_SoftTrk_ScaleUp);
      tree->SetBranchAddress("EtMiss_significance_MET_SoftTrk_ScaleUp", &EtMiss_significance, &b_EtMiss_significance_MET_SoftTrk_ScaleUp);
      tree->SetBranchAddress("EtMiss_tstPhi_MET_SoftTrk_ScaleUp", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MET_SoftTrk_ScaleUp);   
   }
   if (systname == "MUONS_IDup"){
      tree->SetBranchAddress("cleaningVeto_MUON_ID__1up", &cleaningVeto, &b_cleaningVeto_MUON_ID__1up);
      tree->SetBranchAddress("ptleptons_MUON_ID__1up", &ptleptons, &b_ptleptons_MUON_ID__1up);
      tree->SetBranchAddress("etaleptons_MUON_ID__1up", &etaleptons, &b_etaleptons_MUON_ID__1up);
      tree->SetBranchAddress("phileptons_MUON_ID__1up", &phileptons, &b_phileptons_MUON_ID__1up);
      tree->SetBranchAddress("massleptons_MUON_ID__1up", &massleptons, &b_massleptons_MUON_ID__1up);
      tree->SetBranchAddress("flavleptons_MUON_ID__1up", &flavlep, &b_flavleptons_MUON_ID__1up);
      tree->SetBranchAddress("passORlep_MUON_ID__1up", &passORlep, &b_passORlep_MUON_ID__1up);
      tree->SetBranchAddress("isSignallep_MUON_ID__1up", &isSignallep, &b_isSignallep_MUON_ID__1up);
      tree->SetBranchAddress("Originlep_MUON_ID__1up", &Originlep, &b_Originlep_MUON_ID__1up);
      tree->SetBranchAddress("isTightlep_MUON_ID__1up", &isTightlep, &b_isTightlep_MUON_ID__1up);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_ID__1up", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_ID__1up);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_ID__1up", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_ID__1up);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_ID__1up", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_ID__1up);
      tree->SetBranchAddress("d0sig_MUON_ID__1up", &d0sig, &b_d0sig_MUON_ID__1up);
      tree->SetBranchAddress("z0sinTheta_MUON_ID__1up", &z0sinTheta, &b_z0sinTheta_MUON_ID__1up);
      tree->SetBranchAddress("passORjet_MUON_ID__1up", &passORjet, &b_passORjet_MUON_ID__1up);
      tree->SetBranchAddress("WeightEventselSF_MUON_ID__1up", &WeightEventselSF, &b_WeightEventselSF_MUON_ID__1up);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_ID__1up", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_ID__1up);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_ID__1up", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_ID__1up);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_ID__1up", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_ID__1up);
      tree->SetBranchAddress("EtMiss_tst_MUON_ID__1up", &EtMiss_tst, &b_EtMiss_tst_MUON_ID__1up);
      tree->SetBranchAddress("EtMiss_significance_MUON_ID__1up", &EtMiss_significance, &b_EtMiss_significance_MUON_ID__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_ID__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_ID__1up); 
   }
   if (systname == "MUONS_IDdown"){
      tree->SetBranchAddress("cleaningVeto_MUON_ID__1down", &cleaningVeto, &b_cleaningVeto_MUON_ID__1down);
      tree->SetBranchAddress("ptleptons_MUON_ID__1down", &ptleptons, &b_ptleptons_MUON_ID__1down);
      tree->SetBranchAddress("etaleptons_MUON_ID__1down", &etaleptons, &b_etaleptons_MUON_ID__1down);
      tree->SetBranchAddress("phileptons_MUON_ID__1down", &phileptons, &b_phileptons_MUON_ID__1down);
      tree->SetBranchAddress("massleptons_MUON_ID__1down", &massleptons, &b_massleptons_MUON_ID__1down);
      tree->SetBranchAddress("flavleptons_MUON_ID__1down", &flavlep, &b_flavleptons_MUON_ID__1down);
      tree->SetBranchAddress("passORlep_MUON_ID__1down", &passORlep, &b_passORlep_MUON_ID__1down);
      tree->SetBranchAddress("isSignallep_MUON_ID__1down", &isSignallep, &b_isSignallep_MUON_ID__1down);
      tree->SetBranchAddress("Originlep_MUON_ID__1down", &Originlep, &b_Originlep_MUON_ID__1down);
      tree->SetBranchAddress("isTightlep_MUON_ID__1down", &isTightlep, &b_isTightlep_MUON_ID__1down);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_ID__1down", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_ID__1down);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_ID__1down", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_ID__1down);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_ID__1down", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_ID__1down);
      tree->SetBranchAddress("d0sig_MUON_ID__1down", &d0sig, &b_d0sig_MUON_ID__1down);
      tree->SetBranchAddress("z0sinTheta_MUON_ID__1down", &z0sinTheta, &b_z0sinTheta_MUON_ID__1down);
      tree->SetBranchAddress("passORjet_MUON_ID__1down", &passORjet, &b_passORjet_MUON_ID__1down);
      tree->SetBranchAddress("WeightEventselSF_MUON_ID__1down", &WeightEventselSF, &b_WeightEventselSF_MUON_ID__1down);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_ID__1down", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_ID__1down);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_ID__1down", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_ID__1down);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_ID__1down", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_ID__1down);
      tree->SetBranchAddress("EtMiss_tst_MUON_ID__1down", &EtMiss_tst, &b_EtMiss_tst_MUON_ID__1down);
      tree->SetBranchAddress("EtMiss_significance_MUON_ID__1down", &EtMiss_significance, &b_EtMiss_significance_MUON_ID__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_ID__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_ID__1down);
   }
   if (systname == "MUONS_SCALEdown") {
      tree->SetBranchAddress("cleaningVeto_MUON_MS__1down", &cleaningVeto, &b_cleaningVeto_MUON_MS__1down);
      tree->SetBranchAddress("ptleptons_MUON_MS__1down", &ptleptons, &b_ptleptons_MUON_MS__1down);
      tree->SetBranchAddress("etaleptons_MUON_MS__1down", &etaleptons, &b_etaleptons_MUON_MS__1down);
      tree->SetBranchAddress("phileptons_MUON_MS__1down", &phileptons, &b_phileptons_MUON_MS__1down);
      tree->SetBranchAddress("massleptons_MUON_MS__1down", &massleptons, &b_massleptons_MUON_MS__1down);
      tree->SetBranchAddress("flavleptons_MUON_MS__1down", &flavlep, &b_flavleptons_MUON_MS__1down);
      tree->SetBranchAddress("passORlep_MUON_MS__1down", &passORlep, &b_passORlep_MUON_MS__1down);
      tree->SetBranchAddress("isSignallep_MUON_MS__1down", &isSignallep, &b_isSignallep_MUON_MS__1down);
      tree->SetBranchAddress("Originlep_MUON_MS__1down", &Originlep, &b_Originlep_MUON_MS__1down);
      tree->SetBranchAddress("isTightlep_MUON_MS__1down", &isTightlep, &b_isTightlep_MUON_MS__1down);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_MS__1down", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_MS__1down);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_MS__1down", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_MS__1down);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_MS__1down", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_MS__1down);
      tree->SetBranchAddress("d0sig_MUON_MS__1down", &d0sig, &b_d0sig_MUON_MS__1down);
      tree->SetBranchAddress("z0sinTheta_MUON_MS__1down", &z0sinTheta, &b_z0sinTheta_MUON_MS__1down);
      tree->SetBranchAddress("passORjet_MUON_MS__1down", &passORjet, &b_passORjet_MUON_MS__1down);
      tree->SetBranchAddress("WeightEventselSF_MUON_MS__1down", &WeightEventselSF, &b_WeightEventselSF_MUON_MS__1down);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_MS__1down", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_MS__1down);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_MS__1down", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_MS__1down);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_MS__1down", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_MS__1down);
      tree->SetBranchAddress("EtMiss_tst_MUON_MS__1down", &EtMiss_tst, &b_EtMiss_tst_MUON_MS__1down);
      tree->SetBranchAddress("EtMiss_significance_MUON_MS__1down", &EtMiss_significance, &b_EtMiss_significance_MUON_MS__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_MS__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_MS__1down);   
   }
   if (systname == "MUONS_SCALEup") {
      tree->SetBranchAddress("cleaningVeto_MUON_MS__1up", &cleaningVeto, &b_cleaningVeto_MUON_MS__1up);
      tree->SetBranchAddress("ptleptons_MUON_MS__1up", &ptleptons, &b_ptleptons_MUON_MS__1up);
      tree->SetBranchAddress("etaleptons_MUON_MS__1up", &etaleptons, &b_etaleptons_MUON_MS__1up);
      tree->SetBranchAddress("phileptons_MUON_MS__1up", &phileptons, &b_phileptons_MUON_MS__1up);
      tree->SetBranchAddress("massleptons_MUON_MS__1up", &massleptons, &b_massleptons_MUON_MS__1up);
      tree->SetBranchAddress("flavleptons_MUON_MS__1up", &flavlep, &b_flavleptons_MUON_MS__1up);
      tree->SetBranchAddress("passORlep_MUON_MS__1up", &passORlep, &b_passORlep_MUON_MS__1up);
      tree->SetBranchAddress("isSignallep_MUON_MS__1up", &isSignallep, &b_isSignallep_MUON_MS__1up);
      tree->SetBranchAddress("Originlep_MUON_MS__1up", &Originlep, &b_Originlep_MUON_MS__1up);
      tree->SetBranchAddress("isTightlep_MUON_MS__1up", &isTightlep, &b_isTightlep_MUON_MS__1up);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_MS__1up", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_MS__1up);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_MS__1up", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_MS__1up);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_MS__1up", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_MS__1up);
      tree->SetBranchAddress("d0sig_MUON_MS__1up", &d0sig, &b_d0sig_MUON_MS__1up);
      tree->SetBranchAddress("z0sinTheta_MUON_MS__1up", &z0sinTheta, &b_z0sinTheta_MUON_MS__1up);
      tree->SetBranchAddress("passORjet_MUON_MS__1up", &passORjet, &b_passORjet_MUON_MS__1up);
      tree->SetBranchAddress("WeightEventselSF_MUON_MS__1up", &WeightEventselSF, &b_WeightEventselSF_MUON_MS__1up);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_MS__1up", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_MS__1up);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_MS__1up", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_MS__1up);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_MS__1up", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_MS__1up);
      tree->SetBranchAddress("EtMiss_tst_MUON_MS__1up", &EtMiss_tst, &b_EtMiss_tst_MUON_MS__1up);
      tree->SetBranchAddress("EtMiss_significance_MUON_MS__1up", &EtMiss_significance, &b_EtMiss_significance_MUON_MS__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_MS__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_MS__1up);
   }
   if (systname == "MUONS_SAG_REBdown") {
      tree->SetBranchAddress("cleaningVeto_MUON_SAGITTA_RESBIAS__1down", &cleaningVeto, &b_cleaningVeto_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("ptleptons_MUON_SAGITTA_RESBIAS__1down", &ptleptons, &b_ptleptons_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("etaleptons_MUON_SAGITTA_RESBIAS__1down", &etaleptons, &b_etaleptons_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("phileptons_MUON_SAGITTA_RESBIAS__1down", &phileptons, &b_phileptons_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("massleptons_MUON_SAGITTA_RESBIAS__1down", &massleptons, &b_massleptons_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("flavleptons_MUON_SAGITTA_RESBIAS__1down", &flavlep, &b_flavleptons_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("passORlep_MUON_SAGITTA_RESBIAS__1down", &passORlep, &b_passORlep_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("isSignallep_MUON_SAGITTA_RESBIAS__1down", &isSignallep, &b_isSignallep_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("Originlep_MUON_SAGITTA_RESBIAS__1down", &Originlep, &b_Originlep_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("isTightlep_MUON_SAGITTA_RESBIAS__1down", &isTightlep, &b_isTightlep_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_SAGITTA_RESBIAS__1down", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_SAGITTA_RESBIAS__1down", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_SAGITTA_RESBIAS__1down", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("d0sig_MUON_SAGITTA_RESBIAS__1down", &d0sig, &b_d0sig_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("z0sinTheta_MUON_SAGITTA_RESBIAS__1down", &z0sinTheta, &b_z0sinTheta_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("passORjet_MUON_SAGITTA_RESBIAS__1down", &passORjet, &b_passORjet_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("WeightEventselSF_MUON_SAGITTA_RESBIAS__1down", &WeightEventselSF, &b_WeightEventselSF_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_SAGITTA_RESBIAS__1down", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_SAGITTA_RESBIAS__1down", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_SAGITTA_RESBIAS__1down", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("EtMiss_tst_MUON_SAGITTA_RESBIAS__1down", &EtMiss_tst, &b_EtMiss_tst_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("EtMiss_significance_MUON_SAGITTA_RESBIAS__1down", &EtMiss_significance, &b_EtMiss_significance_MUON_SAGITTA_RESBIAS__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_SAGITTA_RESBIAS__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_SAGITTA_RESBIAS__1down);
   }
   if (systname == "MUONS_SAG_REBup") {
      tree->SetBranchAddress("cleaningVeto_MUON_SAGITTA_RESBIAS__1up", &cleaningVeto, &b_cleaningVeto_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("ptleptons_MUON_SAGITTA_RESBIAS__1up", &ptleptons, &b_ptleptons_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("etaleptons_MUON_SAGITTA_RESBIAS__1up", &etaleptons, &b_etaleptons_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("phileptons_MUON_SAGITTA_RESBIAS__1up", &phileptons, &b_phileptons_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("massleptons_MUON_SAGITTA_RESBIAS__1up", &massleptons, &b_massleptons_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("flavleptons_MUON_SAGITTA_RESBIAS__1up", &flavlep, &b_flavleptons_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("passORlep_MUON_SAGITTA_RESBIAS__1up", &passORlep, &b_passORlep_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("isSignallep_MUON_SAGITTA_RESBIAS__1up", &isSignallep, &b_isSignallep_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("Originlep_MUON_SAGITTA_RESBIAS__1up", &Originlep, &b_Originlep_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("isTightlep_MUON_SAGITTA_RESBIAS__1up", &isTightlep, &b_isTightlep_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_SAGITTA_RESBIAS__1up", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_SAGITTA_RESBIAS__1up", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_SAGITTA_RESBIAS__1up", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("d0sig_MUON_SAGITTA_RESBIAS__1up", &d0sig, &b_d0sig_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("z0sinTheta_MUON_SAGITTA_RESBIAS__1up", &z0sinTheta, &b_z0sinTheta_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("passORjet_MUON_SAGITTA_RESBIAS__1up", &passORjet, &b_passORjet_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("WeightEventselSF_MUON_SAGITTA_RESBIAS__1up", &WeightEventselSF, &b_WeightEventselSF_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_SAGITTA_RESBIAS__1up", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_SAGITTA_RESBIAS__1up", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_SAGITTA_RESBIAS__1up", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("EtMiss_tst_MUON_SAGITTA_RESBIAS__1up", &EtMiss_tst, &b_EtMiss_tst_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("EtMiss_significance_MUON_SAGITTA_RESBIAS__1up", &EtMiss_significance, &b_EtMiss_significance_MUON_SAGITTA_RESBIAS__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_SAGITTA_RESBIAS__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_SAGITTA_RESBIAS__1up);
   }
   if (systname == "MUONS_SAG_RHOdown") {
      tree->SetBranchAddress("cleaningVeto_MUON_SAGITTA_RHO__1down", &cleaningVeto, &b_cleaningVeto_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("ptleptons_MUON_SAGITTA_RHO__1down", &ptleptons, &b_ptleptons_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("etaleptons_MUON_SAGITTA_RHO__1down", &etaleptons, &b_etaleptons_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("phileptons_MUON_SAGITTA_RHO__1down", &phileptons, &b_phileptons_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("massleptons_MUON_SAGITTA_RHO__1down", &massleptons, &b_massleptons_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("flavleptons_MUON_SAGITTA_RHO__1down", &flavlep, &b_flavleptons_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("passORlep_MUON_SAGITTA_RHO__1down", &passORlep, &b_passORlep_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("isSignallep_MUON_SAGITTA_RHO__1down", &isSignallep, &b_isSignallep_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("Originlep_MUON_SAGITTA_RHO__1down", &Originlep, &b_Originlep_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("isTightlep_MUON_SAGITTA_RHO__1down", &isTightlep, &b_isTightlep_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_SAGITTA_RHO__1down", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_SAGITTA_RHO__1down", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_SAGITTA_RHO__1down", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("d0sig_MUON_SAGITTA_RHO__1down", &d0sig, &b_d0sig_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("z0sinTheta_MUON_SAGITTA_RHO__1down", &z0sinTheta, &b_z0sinTheta_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("passORjet_MUON_SAGITTA_RHO__1down", &passORjet, &b_passORjet_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("WeightEventselSF_MUON_SAGITTA_RHO__1down", &WeightEventselSF, &b_WeightEventselSF_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_SAGITTA_RHO__1down", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_SAGITTA_RHO__1down", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_SAGITTA_RHO__1down", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("EtMiss_tst_MUON_SAGITTA_RHO__1down", &EtMiss_tst, &b_EtMiss_tst_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("EtMiss_significance_MUON_SAGITTA_RHO__1down", &EtMiss_significance, &b_EtMiss_significance_MUON_SAGITTA_RHO__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_SAGITTA_RHO__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_SAGITTA_RHO__1down);
   }
   if (systname == "MUONS_SAG_RHOup") {
      tree->SetBranchAddress("cleaningVeto_MUON_SAGITTA_RHO__1up", &cleaningVeto, &b_cleaningVeto_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("ptleptons_MUON_SAGITTA_RHO__1up", &ptleptons, &b_ptleptons_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("etaleptons_MUON_SAGITTA_RHO__1up", &etaleptons, &b_etaleptons_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("phileptons_MUON_SAGITTA_RHO__1up", &phileptons, &b_phileptons_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("massleptons_MUON_SAGITTA_RHO__1up", &massleptons, &b_massleptons_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("flavleptons_MUON_SAGITTA_RHO__1up", &flavlep, &b_flavleptons_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("passORlep_MUON_SAGITTA_RHO__1up", &passORlep, &b_passORlep_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("isSignallep_MUON_SAGITTA_RHO__1up", &isSignallep, &b_isSignallep_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("Originlep_MUON_SAGITTA_RHO__1up", &Originlep, &b_Originlep_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("isTightlep_MUON_SAGITTA_RHO__1up", &isTightlep, &b_isTightlep_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_SAGITTA_RHO__1up", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_SAGITTA_RHO__1up", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_SAGITTA_RHO__1up", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("d0sig_MUON_SAGITTA_RHO__1up", &d0sig, &b_d0sig_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("z0sinTheta_MUON_SAGITTA_RHO__1up", &z0sinTheta, &b_z0sinTheta_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("passORjet_MUON_SAGITTA_RHO__1up", &passORjet, &b_passORjet_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("WeightEventselSF_MUON_SAGITTA_RHO__1up", &WeightEventselSF, &b_WeightEventselSF_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_SAGITTA_RHO__1up", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_SAGITTA_RHO__1up", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_SAGITTA_RHO__1up", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("EtMiss_tst_MUON_SAGITTA_RHO__1up", &EtMiss_tst, &b_EtMiss_tst_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("EtMiss_significance_MUON_SAGITTA_RHO__1up", &EtMiss_significance, &b_EtMiss_significance_MUON_SAGITTA_RHO__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_SAGITTA_RHO__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_SAGITTA_RHO__1up);
   }
   if (systname == "MUONS_MSdown") {
      tree->SetBranchAddress("cleaningVeto_MUON_SCALE__1down", &cleaningVeto, &b_cleaningVeto_MUON_SCALE__1down);
      tree->SetBranchAddress("ptleptons_MUON_SCALE__1down", &ptleptons, &b_ptleptons_MUON_SCALE__1down);
      tree->SetBranchAddress("etaleptons_MUON_SCALE__1down", &etaleptons, &b_etaleptons_MUON_SCALE__1down);
      tree->SetBranchAddress("phileptons_MUON_SCALE__1down", &phileptons, &b_phileptons_MUON_SCALE__1down);
      tree->SetBranchAddress("massleptons_MUON_SCALE__1down", &massleptons, &b_massleptons_MUON_SCALE__1down);
      tree->SetBranchAddress("flavleptons_MUON_SCALE__1down", &flavlep, &b_flavleptons_MUON_SCALE__1down);
      tree->SetBranchAddress("passORlep_MUON_SCALE__1down", &passORlep, &b_passORlep_MUON_SCALE__1down);
      tree->SetBranchAddress("isSignallep_MUON_SCALE__1down", &isSignallep, &b_isSignallep_MUON_SCALE__1down);
      tree->SetBranchAddress("Originlep_MUON_SCALE__1down", &Originlep, &b_Originlep_MUON_SCALE__1down);
      tree->SetBranchAddress("isTightlep_MUON_SCALE__1down", &isTightlep, &b_isTightlep_MUON_SCALE__1down);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_SCALE__1down", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_SCALE__1down);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_SCALE__1down", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_SCALE__1down);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_SCALE__1down", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_SCALE__1down);
      tree->SetBranchAddress("d0sig_MUON_SCALE__1down", &d0sig, &b_d0sig_MUON_SCALE__1down);
      tree->SetBranchAddress("z0sinTheta_MUON_SCALE__1down", &z0sinTheta, &b_z0sinTheta_MUON_SCALE__1down);
      tree->SetBranchAddress("passORjet_MUON_SCALE__1down", &passORjet, &b_passORjet_MUON_SCALE__1down);
      tree->SetBranchAddress("WeightEventselSF_MUON_SCALE__1down", &WeightEventselSF, &b_WeightEventselSF_MUON_SCALE__1down);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_SCALE__1down", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_SCALE__1down);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_SCALE__1down", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_SCALE__1down);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_SCALE__1down", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_SCALE__1down);
      tree->SetBranchAddress("EtMiss_tst_MUON_SCALE__1down", &EtMiss_tst, &b_EtMiss_tst_MUON_SCALE__1down);
      tree->SetBranchAddress("EtMiss_significance_MUON_SCALE__1down", &EtMiss_significance, &b_EtMiss_significance_MUON_SCALE__1down);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_SCALE__1down", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_SCALE__1down);   
   }
   if (systname == "MUONS_MSup") {
      tree->SetBranchAddress("cleaningVeto_MUON_SCALE__1up", &cleaningVeto, &b_cleaningVeto_MUON_SCALE__1up);
      tree->SetBranchAddress("ptleptons_MUON_SCALE__1up", &ptleptons, &b_ptleptons_MUON_SCALE__1up);
      tree->SetBranchAddress("etaleptons_MUON_SCALE__1up", &etaleptons, &b_etaleptons_MUON_SCALE__1up);
      tree->SetBranchAddress("phileptons_MUON_SCALE__1up", &phileptons, &b_phileptons_MUON_SCALE__1up);
      tree->SetBranchAddress("massleptons_MUON_SCALE__1up", &massleptons, &b_massleptons_MUON_SCALE__1up);
      tree->SetBranchAddress("flavleptons_MUON_SCALE__1up", &flavlep, &b_flavleptons_MUON_SCALE__1up);
      tree->SetBranchAddress("passORlep_MUON_SCALE__1up", &passORlep, &b_passORlep_MUON_SCALE__1up);
      tree->SetBranchAddress("isSignallep_MUON_SCALE__1up", &isSignallep, &b_isSignallep_MUON_SCALE__1up);
      tree->SetBranchAddress("Originlep_MUON_SCALE__1up", &Originlep, &b_Originlep_MUON_SCALE__1up);
      tree->SetBranchAddress("isTightlep_MUON_SCALE__1up", &isTightlep, &b_isTightlep_MUON_SCALE__1up);
      tree->SetBranchAddress("LepIsoFixedCutTight_MUON_SCALE__1up", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight_MUON_SCALE__1up);
      tree->SetBranchAddress("LepIsoFixedCutTightTrackOnly_MUON_SCALE__1up", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly_MUON_SCALE__1up);
      tree->SetBranchAddress("LepIsoGradientLoose_MUON_SCALE__1up", &LepIsoGradientLoose, &b_LepIsoGradientLoose_MUON_SCALE__1up);
      tree->SetBranchAddress("d0sig_MUON_SCALE__1up", &d0sig, &b_d0sig_MUON_SCALE__1up);
      tree->SetBranchAddress("z0sinTheta_MUON_SCALE__1up", &z0sinTheta, &b_z0sinTheta_MUON_SCALE__1up);
      tree->SetBranchAddress("passORjet_MUON_SCALE__1up", &passORjet, &b_passORjet_MUON_SCALE__1up);
      tree->SetBranchAddress("WeightEventselSF_MUON_SCALE__1up", &WeightEventselSF, &b_WeightEventselSF_MUON_SCALE__1up);
      tree->SetBranchAddress("WeightEventsmuSF_MUON_SCALE__1up", &WeightEventsmuSF, &b_WeightEventsmuSF_MUON_SCALE__1up);
      tree->SetBranchAddress("WeightEventsSF_global_MUON_SCALE__1up", &WeightEventsSF_global, &b_WeightEventsSF_global_MUON_SCALE__1up);
      tree->SetBranchAddress("WeightEvents_trigger_global_MUON_SCALE__1up", &WeightEvents_trigger_global, &b_WeightEvents_trigger_global_MUON_SCALE__1up);
      tree->SetBranchAddress("EtMiss_tst_MUON_SCALE__1up", &EtMiss_tst, &b_EtMiss_tst_MUON_SCALE__1up);
      tree->SetBranchAddress("EtMiss_significance_MUON_SCALE__1up", &EtMiss_significance, &b_EtMiss_significance_MUON_SCALE__1up);
      tree->SetBranchAddress("EtMiss_tstPhi_MUON_SCALE__1up", &EtMiss_tstPhi, &b_EtMiss_tstPhi_MUON_SCALE__1up);
      tree->SetBranchAddress("WeightEventsPU_PRW_DATASF__1down", &WeightEventsPU_PRW_DATASF__1down, &b_WeightEventsPU_PRW_DATASF__1down);
      tree->SetBranchAddress("WeightEventsPU_PRW_DATASF__1up", &WeightEventsPU_PRW_DATASF__1up, &b_WeightEventsPU_PRW_DATASF__1up);
   }
   if(systname == "PRW_DOWN"){
      tree->SetBranchAddress("WeightEventsPU_PRW_DATASF__1down", &WeightEventsPU_PRW_DATASF__1down, &b_WeightEventsPU_PRW_DATASF__1down);
   }
   if(systname == "PRW_UP") {
      tree->SetBranchAddress("WeightEventsPU_PRW_DATASF__1up", &WeightEventsPU_PRW_DATASF__1up, &b_WeightEventsPU_PRW_DATASF__1up);
   }

   tree->SetBranchAddress("WeightEventselSF_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventselSF_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventselSF_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventsSF_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventsSF_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEvents_trigger_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEvents_trigger_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   
   tree->SetBranchAddress("WeightEventselSF_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventselSF_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventselSF_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventsSF_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventsSF_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEvents_trigger_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEvents_trigger_global_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   
   tree->SetBranchAddress("WeightEventselSF_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventselSF_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventselSF_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventsSF_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventsSF_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEvents_trigger_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEvents_trigger_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   
   tree->SetBranchAddress("WeightEventselSF_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventselSF_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventselSF_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventsSF_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventsSF_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEvents_trigger_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEvents_trigger_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);

   tree->SetBranchAddress("WeightEventselSF_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventselSF_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventselSF_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventsSF_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventsSF_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEvents_trigger_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEvents_trigger_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   
   tree->SetBranchAddress("WeightEventselSF_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventselSF_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventselSF_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventsSF_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventsSF_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEvents_trigger_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEvents_trigger_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   
   tree->SetBranchAddress("WeightEventselSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventselSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventselSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventsSF_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventsSF_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEvents_trigger_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEvents_trigger_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);

   tree->SetBranchAddress("WeightEventselSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventselSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventselSF_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventsSF_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventsSF_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEvents_trigger_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEvents_trigger_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   
   tree->SetBranchAddress("WeightEventselSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventselSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventselSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventsSF_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventsSF_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEvents_trigger_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEvents_trigger_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   
   tree->SetBranchAddress("WeightEventselSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventselSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventselSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventsSF_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventsSF_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEvents_trigger_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEvents_trigger_global_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   
   tree->SetBranchAddress("WeightEventselSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventselSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventselSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   
   tree->SetBranchAddress("WeightEventselSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventselSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventselSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);

   tree->SetBranchAddress("WeightEventsPU_FT_EFF_B_systematics__1down", &WeightEventsPU_FT_EFF_B_systematics__1down, &b_WeightEventsPU_FT_EFF_B_systematics__1down);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_B_systematics__1down", &WeightEventsbTag_FT_EFF_B_systematics__1down, &b_WeightEventsbTag_FT_EFF_B_systematics__1down);
   tree->SetBranchAddress("WeightEventsPU_FT_EFF_B_systematics__1up", &WeightEventsPU_FT_EFF_B_systematics__1up, &b_WeightEventsPU_FT_EFF_B_systematics__1up);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_B_systematics__1up", &WeightEventsbTag_FT_EFF_B_systematics__1up, &b_WeightEventsbTag_FT_EFF_B_systematics__1up);
   tree->SetBranchAddress("WeightEventsPU_FT_EFF_C_systematics__1down", &WeightEventsPU_FT_EFF_C_systematics__1down, &b_WeightEventsPU_FT_EFF_C_systematics__1down);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_C_systematics__1down", &WeightEventsbTag_FT_EFF_C_systematics__1down, &b_WeightEventsbTag_FT_EFF_C_systematics__1down);
   tree->SetBranchAddress("WeightEventsPU_FT_EFF_C_systematics__1up", &WeightEventsPU_FT_EFF_C_systematics__1up, &b_WeightEventsPU_FT_EFF_C_systematics__1up);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_C_systematics__1up", &WeightEventsbTag_FT_EFF_C_systematics__1up, &b_WeightEventsbTag_FT_EFF_C_systematics__1up);
   tree->SetBranchAddress("WeightEventsPU_FT_EFF_Light_systematics__1down", &WeightEventsPU_FT_EFF_Light_systematics__1down, &b_WeightEventsPU_FT_EFF_Light_systematics__1down);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_Light_systematics__1down", &WeightEventsbTag_FT_EFF_Light_systematics__1down, &b_WeightEventsbTag_FT_EFF_Light_systematics__1down);
   tree->SetBranchAddress("WeightEventsPU_FT_EFF_Light_systematics__1up", &WeightEventsPU_FT_EFF_Light_systematics__1up, &b_WeightEventsPU_FT_EFF_Light_systematics__1up);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_Light_systematics__1up", &WeightEventsbTag_FT_EFF_Light_systematics__1up, &b_WeightEventsbTag_FT_EFF_Light_systematics__1up);
   tree->SetBranchAddress("WeightEventsPU_FT_EFF_extrapolation__1down", &WeightEventsPU_FT_EFF_extrapolation__1down, &b_WeightEventsPU_FT_EFF_extrapolation__1down);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_extrapolation__1down", &WeightEventsbTag_FT_EFF_extrapolation__1down, &b_WeightEventsbTag_FT_EFF_extrapolation__1down);
   tree->SetBranchAddress("WeightEventsPU_FT_EFF_extrapolation__1up", &WeightEventsPU_FT_EFF_extrapolation__1up, &b_WeightEventsPU_FT_EFF_extrapolation__1up);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_extrapolation__1up", &WeightEventsbTag_FT_EFF_extrapolation__1up, &b_WeightEventsbTag_FT_EFF_extrapolation__1up);
   tree->SetBranchAddress("WeightEventsPU_FT_EFF_extrapolation_from_charm__1down", &WeightEventsPU_FT_EFF_extrapolation_from_charm__1down, &b_WeightEventsPU_FT_EFF_extrapolation_from_charm__1down);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_extrapolation_from_charm__1down", &WeightEventsbTag_FT_EFF_extrapolation_from_charm__1down, &b_WeightEventsbTag_FT_EFF_extrapolation_from_charm__1down);
   tree->SetBranchAddress("WeightEventsPU_FT_EFF_extrapolation_from_charm__1up", &WeightEventsPU_FT_EFF_extrapolation_from_charm__1up, &b_WeightEventsPU_FT_EFF_extrapolation_from_charm__1up);
   tree->SetBranchAddress("WeightEventsbTag_FT_EFF_extrapolation_from_charm__1up", &WeightEventsbTag_FT_EFF_extrapolation_from_charm__1up, &b_WeightEventsbTag_FT_EFF_extrapolation_from_charm__1up);

   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_BADMUON_STAT__1down", &WeightEventsmuSF_MUON_EFF_BADMUON_STAT__1down, &b_WeightEventsmuSF_MUON_EFF_BADMUON_STAT__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_BADMUON_STAT__1down", &WeightEventsSF_global_MUON_EFF_BADMUON_STAT__1down, &b_WeightEventsSF_global_MUON_EFF_BADMUON_STAT__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_BADMUON_STAT__1down", &WeightEvents_trigger_global_MUON_EFF_BADMUON_STAT__1down, &b_WeightEvents_trigger_global_MUON_EFF_BADMUON_STAT__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_BADMUON_STAT__1up", &WeightEventsmuSF_MUON_EFF_BADMUON_STAT__1up, &b_WeightEventsmuSF_MUON_EFF_BADMUON_STAT__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_BADMUON_STAT__1up", &WeightEventsSF_global_MUON_EFF_BADMUON_STAT__1up, &b_WeightEventsSF_global_MUON_EFF_BADMUON_STAT__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_BADMUON_STAT__1up", &WeightEvents_trigger_global_MUON_EFF_BADMUON_STAT__1up, &b_WeightEvents_trigger_global_MUON_EFF_BADMUON_STAT__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_BADMUON_SYS__1down", &WeightEventsmuSF_MUON_EFF_BADMUON_SYS__1down, &b_WeightEventsmuSF_MUON_EFF_BADMUON_SYS__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_BADMUON_SYS__1down", &WeightEventsSF_global_MUON_EFF_BADMUON_SYS__1down, &b_WeightEventsSF_global_MUON_EFF_BADMUON_SYS__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_BADMUON_SYS__1down", &WeightEvents_trigger_global_MUON_EFF_BADMUON_SYS__1down, &b_WeightEvents_trigger_global_MUON_EFF_BADMUON_SYS__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_BADMUON_SYS__1up", &WeightEventsmuSF_MUON_EFF_BADMUON_SYS__1up, &b_WeightEventsmuSF_MUON_EFF_BADMUON_SYS__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_BADMUON_SYS__1up", &WeightEventsSF_global_MUON_EFF_BADMUON_SYS__1up, &b_WeightEventsSF_global_MUON_EFF_BADMUON_SYS__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_BADMUON_SYS__1up", &WeightEvents_trigger_global_MUON_EFF_BADMUON_SYS__1up, &b_WeightEvents_trigger_global_MUON_EFF_BADMUON_SYS__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_ISO_STAT__1down", &WeightEventsmuSF_MUON_EFF_ISO_STAT__1down, &b_WeightEventsmuSF_MUON_EFF_ISO_STAT__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_ISO_STAT__1down", &WeightEventsSF_global_MUON_EFF_ISO_STAT__1down, &b_WeightEventsSF_global_MUON_EFF_ISO_STAT__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_ISO_STAT__1down", &WeightEvents_trigger_global_MUON_EFF_ISO_STAT__1down, &b_WeightEvents_trigger_global_MUON_EFF_ISO_STAT__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_ISO_STAT__1up", &WeightEventsmuSF_MUON_EFF_ISO_STAT__1up, &b_WeightEventsmuSF_MUON_EFF_ISO_STAT__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_ISO_STAT__1up", &WeightEventsSF_global_MUON_EFF_ISO_STAT__1up, &b_WeightEventsSF_global_MUON_EFF_ISO_STAT__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_ISO_STAT__1up", &WeightEvents_trigger_global_MUON_EFF_ISO_STAT__1up, &b_WeightEvents_trigger_global_MUON_EFF_ISO_STAT__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_ISO_SYS__1down", &WeightEventsmuSF_MUON_EFF_ISO_SYS__1down, &b_WeightEventsmuSF_MUON_EFF_ISO_SYS__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_ISO_SYS__1down", &WeightEventsSF_global_MUON_EFF_ISO_SYS__1down, &b_WeightEventsSF_global_MUON_EFF_ISO_SYS__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_ISO_SYS__1down", &WeightEvents_trigger_global_MUON_EFF_ISO_SYS__1down, &b_WeightEvents_trigger_global_MUON_EFF_ISO_SYS__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_ISO_SYS__1up", &WeightEventsmuSF_MUON_EFF_ISO_SYS__1up, &b_WeightEventsmuSF_MUON_EFF_ISO_SYS__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_ISO_SYS__1up", &WeightEventsSF_global_MUON_EFF_ISO_SYS__1up, &b_WeightEventsSF_global_MUON_EFF_ISO_SYS__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_ISO_SYS__1up", &WeightEvents_trigger_global_MUON_EFF_ISO_SYS__1up, &b_WeightEvents_trigger_global_MUON_EFF_ISO_SYS__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_RECO_STAT__1down", &WeightEventsmuSF_MUON_EFF_RECO_STAT__1down, &b_WeightEventsmuSF_MUON_EFF_RECO_STAT__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_RECO_STAT__1down", &WeightEventsSF_global_MUON_EFF_RECO_STAT__1down, &b_WeightEventsSF_global_MUON_EFF_RECO_STAT__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_RECO_STAT__1down", &WeightEvents_trigger_global_MUON_EFF_RECO_STAT__1down, &b_WeightEvents_trigger_global_MUON_EFF_RECO_STAT__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_RECO_STAT__1up", &WeightEventsmuSF_MUON_EFF_RECO_STAT__1up, &b_WeightEventsmuSF_MUON_EFF_RECO_STAT__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_RECO_STAT__1up", &WeightEventsSF_global_MUON_EFF_RECO_STAT__1up, &b_WeightEventsSF_global_MUON_EFF_RECO_STAT__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_RECO_STAT__1up", &WeightEvents_trigger_global_MUON_EFF_RECO_STAT__1up, &b_WeightEvents_trigger_global_MUON_EFF_RECO_STAT__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_RECO_STAT_LOWPT__1down", &WeightEventsmuSF_MUON_EFF_RECO_STAT_LOWPT__1down, &b_WeightEventsmuSF_MUON_EFF_RECO_STAT_LOWPT__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_RECO_STAT_LOWPT__1down", &WeightEventsSF_global_MUON_EFF_RECO_STAT_LOWPT__1down, &b_WeightEventsSF_global_MUON_EFF_RECO_STAT_LOWPT__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_RECO_STAT_LOWPT__1down", &WeightEvents_trigger_global_MUON_EFF_RECO_STAT_LOWPT__1down, &b_WeightEvents_trigger_global_MUON_EFF_RECO_STAT_LOWPT__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_RECO_STAT_LOWPT__1up", &WeightEventsmuSF_MUON_EFF_RECO_STAT_LOWPT__1up, &b_WeightEventsmuSF_MUON_EFF_RECO_STAT_LOWPT__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_RECO_STAT_LOWPT__1up", &WeightEventsSF_global_MUON_EFF_RECO_STAT_LOWPT__1up, &b_WeightEventsSF_global_MUON_EFF_RECO_STAT_LOWPT__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_RECO_STAT_LOWPT__1up", &WeightEvents_trigger_global_MUON_EFF_RECO_STAT_LOWPT__1up, &b_WeightEvents_trigger_global_MUON_EFF_RECO_STAT_LOWPT__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_RECO_SYS__1down", &WeightEventsmuSF_MUON_EFF_RECO_SYS__1down, &b_WeightEventsmuSF_MUON_EFF_RECO_SYS__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_RECO_SYS__1down", &WeightEventsSF_global_MUON_EFF_RECO_SYS__1down, &b_WeightEventsSF_global_MUON_EFF_RECO_SYS__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_RECO_SYS__1down", &WeightEvents_trigger_global_MUON_EFF_RECO_SYS__1down, &b_WeightEvents_trigger_global_MUON_EFF_RECO_SYS__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_RECO_SYS__1up", &WeightEventsmuSF_MUON_EFF_RECO_SYS__1up, &b_WeightEventsmuSF_MUON_EFF_RECO_SYS__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_RECO_SYS__1up", &WeightEventsSF_global_MUON_EFF_RECO_SYS__1up, &b_WeightEventsSF_global_MUON_EFF_RECO_SYS__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_RECO_SYS__1up", &WeightEvents_trigger_global_MUON_EFF_RECO_SYS__1up, &b_WeightEvents_trigger_global_MUON_EFF_RECO_SYS__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_RECO_SYS_LOWPT__1down", &WeightEventsmuSF_MUON_EFF_RECO_SYS_LOWPT__1down, &b_WeightEventsmuSF_MUON_EFF_RECO_SYS_LOWPT__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_RECO_SYS_LOWPT__1down", &WeightEventsSF_global_MUON_EFF_RECO_SYS_LOWPT__1down, &b_WeightEventsSF_global_MUON_EFF_RECO_SYS_LOWPT__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_RECO_SYS_LOWPT__1down", &WeightEvents_trigger_global_MUON_EFF_RECO_SYS_LOWPT__1down, &b_WeightEvents_trigger_global_MUON_EFF_RECO_SYS_LOWPT__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_RECO_SYS_LOWPT__1up", &WeightEventsmuSF_MUON_EFF_RECO_SYS_LOWPT__1up, &b_WeightEventsmuSF_MUON_EFF_RECO_SYS_LOWPT__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_RECO_SYS_LOWPT__1up", &WeightEventsSF_global_MUON_EFF_RECO_SYS_LOWPT__1up, &b_WeightEventsSF_global_MUON_EFF_RECO_SYS_LOWPT__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_RECO_SYS_LOWPT__1up", &WeightEvents_trigger_global_MUON_EFF_RECO_SYS_LOWPT__1up, &b_WeightEvents_trigger_global_MUON_EFF_RECO_SYS_LOWPT__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_TTVA_STAT__1down", &WeightEventsmuSF_MUON_EFF_TTVA_STAT__1down, &b_WeightEventsmuSF_MUON_EFF_TTVA_STAT__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_TTVA_STAT__1down", &WeightEventsSF_global_MUON_EFF_TTVA_STAT__1down, &b_WeightEventsSF_global_MUON_EFF_TTVA_STAT__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_TTVA_STAT__1down", &WeightEvents_trigger_global_MUON_EFF_TTVA_STAT__1down, &b_WeightEvents_trigger_global_MUON_EFF_TTVA_STAT__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_TTVA_STAT__1up", &WeightEventsmuSF_MUON_EFF_TTVA_STAT__1up, &b_WeightEventsmuSF_MUON_EFF_TTVA_STAT__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_TTVA_STAT__1up", &WeightEventsSF_global_MUON_EFF_TTVA_STAT__1up, &b_WeightEventsSF_global_MUON_EFF_TTVA_STAT__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_TTVA_STAT__1up", &WeightEvents_trigger_global_MUON_EFF_TTVA_STAT__1up, &b_WeightEvents_trigger_global_MUON_EFF_TTVA_STAT__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_TTVA_SYS__1down", &WeightEventsmuSF_MUON_EFF_TTVA_SYS__1down, &b_WeightEventsmuSF_MUON_EFF_TTVA_SYS__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_TTVA_SYS__1down", &WeightEventsSF_global_MUON_EFF_TTVA_SYS__1down, &b_WeightEventsSF_global_MUON_EFF_TTVA_SYS__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_TTVA_SYS__1down", &WeightEvents_trigger_global_MUON_EFF_TTVA_SYS__1down, &b_WeightEvents_trigger_global_MUON_EFF_TTVA_SYS__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_TTVA_SYS__1up", &WeightEventsmuSF_MUON_EFF_TTVA_SYS__1up, &b_WeightEventsmuSF_MUON_EFF_TTVA_SYS__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_TTVA_SYS__1up", &WeightEventsSF_global_MUON_EFF_TTVA_SYS__1up, &b_WeightEventsSF_global_MUON_EFF_TTVA_SYS__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_TTVA_SYS__1up", &WeightEvents_trigger_global_MUON_EFF_TTVA_SYS__1up, &b_WeightEvents_trigger_global_MUON_EFF_TTVA_SYS__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_TrigStatUncertainty__1down", &WeightEventsmuSF_MUON_EFF_TrigStatUncertainty__1down, &b_WeightEventsmuSF_MUON_EFF_TrigStatUncertainty__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_TrigStatUncertainty__1down", &WeightEventsSF_global_MUON_EFF_TrigStatUncertainty__1down, &b_WeightEventsSF_global_MUON_EFF_TrigStatUncertainty__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_TrigStatUncertainty__1down", &WeightEvents_trigger_global_MUON_EFF_TrigStatUncertainty__1down, &b_WeightEvents_trigger_global_MUON_EFF_TrigStatUncertainty__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_TrigStatUncertainty__1up", &WeightEventsmuSF_MUON_EFF_TrigStatUncertainty__1up, &b_WeightEventsmuSF_MUON_EFF_TrigStatUncertainty__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_TrigStatUncertainty__1up", &WeightEventsSF_global_MUON_EFF_TrigStatUncertainty__1up, &b_WeightEventsSF_global_MUON_EFF_TrigStatUncertainty__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_TrigStatUncertainty__1up", &WeightEvents_trigger_global_MUON_EFF_TrigStatUncertainty__1up, &b_WeightEvents_trigger_global_MUON_EFF_TrigStatUncertainty__1up);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_TrigSystUncertainty__1down", &WeightEventsmuSF_MUON_EFF_TrigSystUncertainty__1down, &b_WeightEventsmuSF_MUON_EFF_TrigSystUncertainty__1down);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_TrigSystUncertainty__1down", &WeightEventsSF_global_MUON_EFF_TrigSystUncertainty__1down, &b_WeightEventsSF_global_MUON_EFF_TrigSystUncertainty__1down);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_TrigSystUncertainty__1down", &WeightEvents_trigger_global_MUON_EFF_TrigSystUncertainty__1down, &b_WeightEvents_trigger_global_MUON_EFF_TrigSystUncertainty__1down);
   tree->SetBranchAddress("WeightEventsmuSF_MUON_EFF_TrigSystUncertainty__1up", &WeightEventsmuSF_MUON_EFF_TrigSystUncertainty__1up, &b_WeightEventsmuSF_MUON_EFF_TrigSystUncertainty__1up);
   tree->SetBranchAddress("WeightEventsSF_global_MUON_EFF_TrigSystUncertainty__1up", &WeightEventsSF_global_MUON_EFF_TrigSystUncertainty__1up, &b_WeightEventsSF_global_MUON_EFF_TrigSystUncertainty__1up);
   tree->SetBranchAddress("WeightEvents_trigger_global_MUON_EFF_TrigSystUncertainty__1up", &WeightEvents_trigger_global_MUON_EFF_TrigSystUncertainty__1up, &b_WeightEvents_trigger_global_MUON_EFF_TrigSystUncertainty__1up);

   tree->SetBranchAddress("WeightEventsJVT_JET_JvtEfficiency__1down", &WeightEventsJVT_JET_JvtEfficiency__1down, &b_WeightEventsJVT_JET_JvtEfficiency__1down);
   tree->SetBranchAddress("WeightEventsJVT_JET_JvtEfficiency__1up", &WeightEventsJVT_JET_JvtEfficiency__1up, &b_WeightEventsJVT_JET_JvtEfficiency__1up);
   tree->SetBranchAddress("WeightEventsJVT_JET_fJvtEfficiency__1down", &WeightEventsJVT_JET_fJvtEfficiency__1down, &b_WeightEventsJVT_JET_fJvtEfficiency__1down);
   tree->SetBranchAddress("WeightEventsJVT_JET_fJvtEfficiency__1up", &WeightEventsJVT_JET_fJvtEfficiency__1up, &b_WeightEventsJVT_JET_fJvtEfficiency__1up);


   

   
   

return EL::StatusCode::SUCCESS;

}

void LoopSUSYCore::Register_myhistos(myhistos *histo)
{

for (int i = 0; i < histo->GetNmyhistos(); i++) {
	 wk()->addOutput(histo->GetHisto(i));
}
for (int i = 0; i < histo->GetNmyhistos2D(); i++) {
	 wk()->addOutput(histo->GetHisto2D(i));
}

return;

}

double LoopSUSYCore::LumiWeightCalc(double targetLumi) {

double wei_lumi = 1.;

TH1D* hnev = (TH1D*)wk()->inputFile()->Get("NumberEvents");
double num_ev = hnev->GetBinContent(2);

wei_lumi = targetLumi / num_ev;

delete hnev;

return wei_lumi;
}

void LoopSUSYCore::SetSysts(string option=""){
	systname=option;
}
void LoopSUSYCore::SetType(string option=""){
	if (option=="Data_central") m_isData=true;
	else m_isData=false;
}


string LoopSUSYCore::GetSysts(){
	return systname;
}

