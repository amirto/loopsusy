#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/Job.h>


#include <LoopSUSYCore/LoopSUSYCoreTruth.h>
#include <LoopSUSYCore/myhistos.h>

#include <string>

#include "TFile.h"
#include "TH1D.h"

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(LoopSUSYCoreTruth)

LoopSUSYCoreTruth :: LoopSUSYCoreTruth ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  //Info("LoopSUSYCoreTruth()","Constructor called");

}


EL::StatusCode LoopSUSYCoreTruth::changeInput(bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  const char *APP_NAME = "changeInput()";
  Info( APP_NAME, "Setting up branches");

  if (firstFile) {
    ChannelNumber = (int)wk()->metaData()->castDouble("mc_chan");
    Info( APP_NAME, "This is mc_chan %d", ChannelNumber);
  }

  // Set object pointer
  ptjets_truth = 0;
  etajets_truth = 0;
  phijets_truth = 0;
  massjets_truth = 0;
  labeljets_truth = 0;
  mc_eta = 0;
  mc_pt = 0;
  mc_mass = 0;
  mc_phi = 0;
  mc_pdgid = 0;
  mc_parent_pdgid = 0;

  //Set branch addresses and branch pointers
  tree = wk()->tree();

  tree->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
  tree->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
  tree->SetBranchAddress("xsec", &xsec, &b_xsec);
  tree->SetBranchAddress("EventWeight", &EventWeight, &b_EventWeight);
  tree->SetBranchAddress("ptjets_truth", &ptjets_truth, &b_ptjets_truth);
  tree->SetBranchAddress("etajets_truth", &etajets_truth, &b_etajets_truth);
  tree->SetBranchAddress("phijets_truth", &phijets_truth, &b_phijets_truth);
  tree->SetBranchAddress("massjets_truth", &massjets_truth, &b_massjets_truth);
  tree->SetBranchAddress("labeljets_truth", &labeljets_truth, &b_labeljets_truth);
  tree->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
  tree->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
  tree->SetBranchAddress("mc_mass", &mc_mass, &b_mc_mass);
  tree->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
  tree->SetBranchAddress("mc_pdgid", &mc_pdgid, &b_mc_pdgid);
  tree->SetBranchAddress("mc_parent_pdgid", &mc_parent_pdgid, &b_mc_parent_pdgid);
  tree->SetBranchAddress("EtMiss_truthPhi", &EtMiss_truthPhi, &b_EtMiss_truthPhi);
  tree->SetBranchAddress("EtMiss_truth", &EtMiss_truth, &b_EtMiss_truth);

  return EL::StatusCode::SUCCESS;

}

void LoopSUSYCoreTruth::Register_myhistos(myhistos *histo)
{

  for (int i = 0; i < histo->GetNmyhistos(); i++) {
    wk()->addOutput(histo->GetHisto(i));
  }
  for (int i = 0; i < histo->GetNmyhistos2D(); i++) {
    wk()->addOutput(histo->GetHisto2D(i));
  }

  return;

}

double LoopSUSYCoreTruth::LumiWeightCalc(double targetLumi) {

  double wei_lumi = 1.;

  TH1D* hnev = (TH1D*)wk()->inputFile()->Get("NumberEvents");
  double num_ev = hnev->GetBinContent(2);

  wei_lumi = targetLumi / num_ev;

  delete hnev;

  return wei_lumi;
}

