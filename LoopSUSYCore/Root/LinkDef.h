#include <LoopSUSYCore/LoopSUSYCore.h>
#include <LoopSUSYCore/LoopSUSYCoreTruth.h>
#include <LoopSUSYCore/myhistos.h>
#include <LoopSUSYCore/mctlib.h>
#include <LoopSUSYCore/TMctLib.h>
#include <LoopSUSYCore/mt2w_bisect.h>

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ namespace mt2w_bisect;

#endif

#ifdef __CINT__
#pragma link C++ class LoopSUSYCore+;
#pragma link C++ class LoopSUSYCoreTruth+;
#pragma link C++ class myhistos+;
#pragma link C++ class mctlib+;
#pragma link C++ class TMctLib+;
#pragma link C++ class mt2w_bisect::mt2w+;
#pragma link C++ class MT2+;
#pragma link C++ class MT2_ROOT+;
#endif
