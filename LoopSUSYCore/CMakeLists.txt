################################################################################
# Package: LoopSUSYCore
################################################################################

# Declare the package name:
atlas_subdir( LoopSUSYCore )

set( extra_deps )
if( NOT XAOD_STANDALONE )
   set( extra_deps GaudiKernel PhysicsAnalysis/POOLRootAccess
      Control/AthAnalysisBaseComps Control/AthenaBaseComps
      Control/AthenaKernel PhysicsAnalysis/SUSYPhys/SUSYTools PhysicsAnalysis/D3PDTools/EventLoop PhysicsAnalysis/D3PDTools/EventLoopGrid )
   set( extra_libs GaudiKernel AthAnalysisBaseCompsLib )
endif()

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   PhysicsAnalysis/D3PDTools/EventLoop
   PhysicsAnalysis/D3PDTools/EventLoopGrid
   PhysicsAnalysis/SUSYPhys/SUSYTools
   PhysicsAnalysis/AnalysisCommon/PileupReweighting
   LoopSUSYCore
   ${extra_deps} 
    PRIVATE
   Tools/PathResolver
    )

# External dependencies:
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO )
find_package( GTest )

# Libraries in the package:
if( XAOD_STANDALONE )
   atlas_add_root_dictionary( LoopSUSYCoreLib LoopSUSYCoreLibCintDict
      ROOT_HEADERS LoopSUSYCore/LoopSUSYCore.h  LoopSUSYCore/LoopSUSYCoreTruth.h  LoopSUSYCore/MT2.h  LoopSUSYCore/MT2_ROOT.h  LoopSUSYCore/TMctLib.h  LoopSUSYCore/mctlib.h  LoopSUSYCore/mt2w_bisect.h  LoopSUSYCore/myhistos.h Root/LinkDef.h
      EXTERNAL_PACKAGES ROOT )
endif()

set( extra_public_libs )
set( extra_private_libs )
if( XAOD_STANDALONE )
   set( extra_public_libs EventLoop EventLoopGrid SUSYToolsLib xAODCutFlow)
   set( extra_private_libs xAODRootAccessInterfaces xAODRootAccess )
else()
   set( extra_public_libs )
   set( extra_private_libs AthAnalysisBaseCompsLib StoreGateLib )
endif()

# Libraries in the package:
atlas_add_library( LoopSUSYCoreLib
   LoopSUSYCore/*.h Root/*.cxx ${LoopSUSYCoreLibCintDict}
   PUBLIC_HEADERS LoopSUSYCore
   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
   LINK_LIBRARIES PATInterfaces ${extra_public_libs}
   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${BOOST_LIBRARIES}
   PileupReweightingLib PathResolver GoodRunsListsLib ${extra_private_libs} )



if( NOT XAOD_STANDALONE )
   atlas_add_component( LoopSUSYCore
      src/*.h src/*.cxx src/components/*.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} AthenaBaseComps GaudiKernel
      AthenaKernel AthAnalysisBaseCompsLib xAODEventInfo xAODMuon xAODPrimitives
      xAODJet xAODBTagging xAODEgamma xAODMissingET xAODTracking xAODTau
      TauAnalysisToolsLib xAODCore AthContainers AsgTools xAODBase xAODCutFlow
      PATInterfaces PathResolver LoopSUSYCoreLib )
endif()

#atlas_add_executable(   stopNtupGrid util/stopNtupGrid.cxx 
#      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} 
#     LINK_LIBRARIES ${ROOT_LIBRARIES} LoopSUSYCoreLib AsgTools ${extra_public_libs} ${extra_private_libs})
#atlas_add_dictionary( LoopSUSYCoreDict
#   LoopSUSYCore/LoopSUSYCoreDict.h
#   LoopSUSYCore/selection.xml
#   LINK_LIBRARIES LoopSUSYCoreLib )

# Executable(s) in the package:
set( extra_libs )
if( NOT XAOD_STANDALONE )
   set( extra_libs POOLRootAccess )
endif()

# Install files from the package:
#atlas_install_python_modules( python/*.py )
#atlas_install_joboptions( share/*.py )
atlas_install_data( data/* )
