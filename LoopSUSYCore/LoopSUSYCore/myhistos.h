#ifndef myhistos_h
#define myhistos_h

#include <iostream>
#include <vector>
#include "TH1F.h"
#include "TH2F.h"

class myhistos {

public:
  myhistos();
  // myhistos(std::string title, int nbins, double xmin, double xmax, bool sinlep = true);
  myhistos(std::string title, int nbins, double xmin, double xmax, bool sinlep = false , bool sin_plus_dilep = false);
  myhistos(std::string title, int nbins, const float *xbins, bool sinlep = false);
  myhistos(std::string title, int nxbins, double xmin, double xmax, int nybins, double ymin, double ymax, bool sinlep = false);

  myhistos(std::string title, int nbins, double xmin, double xmax, int lep_multiplicity=2);
  myhistos(std::string title, int nbins, const float *xbins, int lep_multiplicity=2);
  
  ~myhistos();

  void Fill(int ibin, double value, double weight);
  void Fill(int ibin, double xvalue, double yvalue, double weight);
  double GetXmax() { return myhistvec[0]->GetXaxis()->GetXmax(); }
  double GetXmin() { return myhistvec[0]->GetXaxis()->GetXmin(); }
  double GetYmax() { return myhistvec[0]->GetYaxis()->GetXmax(); }
  double GetYmin() { return myhistvec[0]->GetYaxis()->GetXmin(); }
  int GetNmyhistos() { return myhistvec.size(); }
  int GetNmyhistos2D() { return myhistvec2D.size(); }
  TH1F* GetHisto(int ibin) { return myhistvec[ibin]; }
  TH2F* GetHisto2D(int ibin) { return myhistvec2D[ibin]; }
  void Write();
  void Write2D();

private:

  std::vector<TH1F*> myhistvec;
  std::vector<TH2F*> myhistvec2D;

};

#endif
