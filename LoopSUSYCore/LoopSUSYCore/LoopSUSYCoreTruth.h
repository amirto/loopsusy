#ifndef LoopSUSYCoreTruth_H
#define LoopSUSYCoreTruth_H

#include <EventLoop/Algorithm.h>
#include <TTree.h>
#include <TBranch.h>

#include <string>
#include <vector>

class myhistos;

using namespace std;

class LoopSUSYCoreTruth : public EL::Algorithm
{
   // put your configuration variables here as public variables.
   // that way they can be set directly from CINT and python.

public:
   // variables that don't get filled at submission time should be
   // protected_from_being send from the submission node to the worker
   // node (done by the //!)

public:
   TTree *tree; //! pointer to the analyzed TTree or TChain

   int ChannelNumber; //!

   // Declaration of leaf types
   ULong64_t       EventNumber; //!
   UInt_t          RunNumber; //!
   Double_t        xsec; //!
   Float_t         EventWeight; //!
   vector<float>   *ptjets_truth; //!
   vector<float>   *etajets_truth; //!
   vector<float>   *phijets_truth; //!
   vector<float>   *massjets_truth; //!
   vector<float>   *labeljets_truth; //!
   vector<float>   *mc_eta; //!
   vector<float>   *mc_pt; //!
   vector<float>   *mc_mass; //!
   vector<float>   *mc_phi; //!
   vector<int>     *mc_pdgid; //!
   vector<int>     *mc_parent_pdgid; //!
   Float_t         EtMiss_truthPhi; //!
   Float_t         EtMiss_truth; //!

   // List of branches
   TBranch        *b_EventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_xsec;   //!
   TBranch        *b_EventWeight;   //!
   TBranch        *b_ptjets_truth;   //!
   TBranch        *b_etajets_truth;   //!
   TBranch        *b_phijets_truth;   //!
   TBranch        *b_massjets_truth;   //!
   TBranch        *b_labeljets_truth;   //!
   TBranch        *b_mc_eta;   //!
   TBranch        *b_mc_pt;   //!
   TBranch        *b_mc_mass;   //!
   TBranch        *b_mc_phi;   //!
   TBranch        *b_mc_pdgid;   //!
   TBranch        *b_mc_parent_pdgid;   //!
   TBranch        *b_EtMiss_truthPhi;   //!
   TBranch        *b_EtMiss_truth;   //!

   // this is a standard constructor
   LoopSUSYCoreTruth (); //!

   // these are the functions inherited_from_Algorithm
   virtual EL::StatusCode changeInput (bool firstFile); //!

   void Register_myhistos(myhistos *histo);
   double LumiWeightCalc(double targetLumi);

   // this is needed to distribute the algorithm to the workers
   ClassDef(LoopSUSYCoreTruth, 1); //!
}; //!

#endif
