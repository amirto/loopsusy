import ROOT
import logging
import shutil
import os
import re
import socket


logging.basicConfig(level=logging.INFO)
from optparse import OptionParser

parser = OptionParser()
parser.add_option("--submitDir", help="dir to store the output", default="submit_dir")
parser.add_option("--inputFile", default="")
parser.add_option("--outFile", default="output")
parser.add_option("--doSysts", default="NONE")
parser.add_option("--signal", default=False)
parser.add_option("--nevents", type=int, help="number of events to process for all the datasets",default=0)
parser.add_option("--actuallye", action='store_true')
parser.add_option("--cutflow", action='store_true')
(options, args) = parser.parse_args()
#rundirectory=options.rundir
if options.inputFile=="":
   logging.critical("No input file provided...exiting")
   exit()

systname=options.doSysts

shutil.rmtree(options.submitDir, True)
   
logging.info("creating new sample handler")
sh=ROOT.SH.SampleHandler()
inputFilePath = options.inputFile
ROOT.SH.ScanDir().filePattern("*_0*").scan(sh,inputFilePath)

myEvents=1
targetLumi=1

print "cutflow",options.cutflow
dataORmc,whichData,whichMC="","",""
samplename=inputFilePath.split("/")[-1]
if "data" in samplename:
   dataORmc="data"
   if "data15" in samplename: whichData="15"
   elif "data16" in samplename: whichData="16"
   elif "data17" in samplename: whichData="17"
   elif "data18" in samplename: whichData="18"
else:
   dataORmc="MC"
   if "mc16a" in samplename: 
      whichMC="a"
      targetLumi=36184.86
   if "mc16c" in samplename: 
      whichMC="c"
      targetLumi=44307.4
   if "mc16d" in samplename:
      if options.actuallye: 
         whichMC="e"
         targetLumi=47824.7
      else:
         whichMC="d"
         targetLumi=44307.4
   if "mc16e" in samplename: 
      whichMC="e"
      targetLumi=47824.7

if dataORmc=="MC":
   for e in os.listdir(options.inputFile):
      if "0000" in e:
         f=ROOT.TFile(options.inputFile+"/"+e)
         h=f.Get("NumberEvents")
         evt=h.GetBinContent(2)
         #print(e,evt)
         myEvents=myEvents+evt
         f.Close()
         del f
         
   ttbarIds=[]
   ttbarIds.append(410472)#PhPy8EG_A14_ttbar_hdamp258p75_dil
   ttbarIds.append(407345)#PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad
   ttbarIds.append(407346)#PhPy8EG_A14_ttbarMET300_400_hdamp258p75_nonallhad
   ttbarIds.append(407347)#PhPy8EG_A14_ttbarMET400_hdamp258p75_nonallhad

   SingletopIds=[]
   SingletopIds.append(410644)#PowhegPythia8EvtGen_A14_singletop_schan_lept_top
   SingletopIds.append(410645)#PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop
   SingletopIds.append(410646)#PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top
   SingletopIds.append(410647)#PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop
   SingletopIds.append(410658)#PhPy8EG_A14_tchan_BW50_lept_top
   SingletopIds.append(410659)#PhPy8EG_A14_tchan_BW50_lept_antitop
   SingletopIds.append(410648)#PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top
   SingletopIds.append(410649)#PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop

   ttVIds=[]
   ttVIds.append(410155)#aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttW
   ttVIds.append(410156)#aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttZnunu
   ttVIds.append(410157)#aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttZqq
   ttVIds.append(410218)#aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttee
   ttVIds.append(410219)#aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttmumu
   ttVIds.append(410220)#aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tttautau
   ttVIds.append(410276)#aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttee_mll_1_5
   ttVIds.append(410277)#aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttmumu_mll_1_5
   ttVIds.append(410278)#aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tttautau_mll_1_5

   OthersIds=[]
   OthersIds.append(410080)#MadGraphPythia8EvtGen_A14NNPDF23_4topSM
   OthersIds.append(410081)#MadGraphPythia8EvtGen_A14NNPDF23_ttbarWW
   OthersIds.append(304014)#MadGraphPythia8EvtGen_A14NNPDF23_3top_SM
   OthersIds.append(407321)#MadGraphPythia8EvtGen_A14NNPDF23LO_ttbarWll
   OthersIds.append(410560)#MadGraphPythia8EvtGen_A14_tZ_4fl_tchan_noAllHad

   ZjetsIds=[]
   ZjetsIds.append(308092)#Sherpa_221_NNPDF30NNLO_Zee2jets_Min_N_Tchannel
   ZjetsIds.append(308093)#Sherpa_221_NNPDF30NNLO_Zmm2jets_Min_N_Tchannel
   ZjetsIds.append(308094)#Sherpa_221_NNPDF30NNLO_Ztautau2jets_Min_N_Tchannel
   ZjetsIds.append(308095)#Sherpa_221_NNPDF30NNLO_Znunu2jets_Min_N_Tchannel
   ZjetsIds.append(364100)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto
   ZjetsIds.append(364101)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto
   ZjetsIds.append(364102)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_Bfilter
   ZjetsIds.append(364103)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto
   ZjetsIds.append(364104)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto
   ZjetsIds.append(364105)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_Bfilter
   ZjetsIds.append(364106)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto
   ZjetsIds.append(364107)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto
   ZjetsIds.append(364108)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_Bfilter
   ZjetsIds.append(364109)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto
   ZjetsIds.append(364110)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto
   ZjetsIds.append(364111)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_Bfilter
   ZjetsIds.append(364112)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000
   ZjetsIds.append(364113)#Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS
   ZjetsIds.append(364114)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto
   ZjetsIds.append(364115)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto
   ZjetsIds.append(364116)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_Bfilter
   ZjetsIds.append(364117)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto
   ZjetsIds.append(364118)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto
   ZjetsIds.append(364119)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_Bfilter
   ZjetsIds.append(364120)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto
   ZjetsIds.append(364121)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto
   ZjetsIds.append(364122)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_Bfilter
   ZjetsIds.append(364123)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto
   ZjetsIds.append(364124)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto
   ZjetsIds.append(364125)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_Bfilter
   ZjetsIds.append(364126)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000
   ZjetsIds.append(364127)#Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS
   ZjetsIds.append(364128)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CVetoBVeto
   ZjetsIds.append(364129)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CFilterBVeto
   ZjetsIds.append(364130)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_Bfilter
   ZjetsIds.append(364131)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto
   ZjetsIds.append(364132)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CFilterBVeto
   ZjetsIds.append(364133)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_Bfilter
   ZjetsIds.append(364134)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CVetoBVeto
   ZjetsIds.append(364135)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CFilterBVeto
   ZjetsIds.append(364136)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_Bfilter
   ZjetsIds.append(364137)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CVetoBVeto
   ZjetsIds.append(364138)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CFilterBVeto
   ZjetsIds.append(364139)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_Bfilter
   ZjetsIds.append(364140)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV500_1000
   ZjetsIds.append(364141)#Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV1000_E_CMS

   WWIds=[]
   WWIds.append(361600)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv
   WWIds.append(361606)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvqq

   WZIds=[]
   WZIds.append(361601)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvll_mll4
   WZIds.append(361602)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvvv_mll4
   WZIds.append(361607)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqll_mll20
   WZIds.append(361608)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqvv
   WZIds.append(361609)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvqq_mqq20

   ZZIds=[]
   ZZIds.append(361603)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZllll_mll4
   ZZIds.append(361604)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4
   ZZIds.append(361605)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvvv_mll4
   ZZIds.append(361610)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20
   ZZIds.append(361611)#PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvqq_mqq20

   TribosonsIds=[]
   TribosonsIds.append(364242)#Sherpa_222_NNPDF30NNLO_WWW_3l3v_EW6
   TribosonsIds.append(364243)#Sherpa_222_NNPDF30NNLO_WWZ_4l2v_EW6
   TribosonsIds.append(364244)#Sherpa_222_NNPDF30NNLO_WWZ_2l4v_EW6
   TribosonsIds.append(364245)#Sherpa_222_NNPDF30NNLO_WZZ_5l1v_EW6
   TribosonsIds.append(364246)#Sherpa_222_NNPDF30NNLO_WZZ_3l3v_EW6
   TribosonsIds.append(364247)#Sherpa_222_NNPDF30NNLO_ZZZ_6l0v_EW6
   TribosonsIds.append(364248)#Sherpa_222_NNPDF30NNLO_ZZZ_4l2v_EW6
   TribosonsIds.append(364249)#Sherpa_222_NNPDF30NNLO_ZZZ_2l4v_EW6
   TribosonsIds.append(407311)#Sherpa_221_NNPDF30NNLO_6l0v_EW6
   TribosonsIds.append(407312)#Sherpa_221_NNPDF30NNLO_5l1v_EW6
   TribosonsIds.append(407313)#Sherpa_221_NNPDF30NNLO_4l2v_EW6
   TribosonsIds.append(407314)#Sherpa_221_NNPDF30NNLO_3l3v_EW6
   TribosonsIds.append(407315)#Sherpa_221_NNPDF30NNLO_2l4v_EW6

   DYIds=[]
   DYIds.append(364198)#Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_Bveto
   DYIds.append(364199)#Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_Bfilter
   DYIds.append(364200)#Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_Bveto
   DYIds.append(364201)#Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_Bfilter
   DYIds.append(364202)#Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_Bveto
   DYIds.append(364203)#Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_Bfilter
   DYIds.append(364204)#Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_Bveto
   DYIds.append(364205)#Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_Bfilter
   DYIds.append(364206)#Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_Bveto
   DYIds.append(364207)#Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_Bfilter
   DYIds.append(364208)#Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_Bveto
   DYIds.append(364209)#Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_Bfilter
   DYIds.append(364210)#Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV0_70_Bveto
   DYIds.append(364211)#Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV0_70_Bfilter
   DYIds.append(364212)#Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV70_280_Bveto
   DYIds.append(364213)#Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV70_280_Bfilter
   DYIds.append(364214)#Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV280_E_CMS_Bveto
   DYIds.append(364215)#Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV280_E_CMS_Bfilter
   DYIds.append(364280)#Sherpa_221_NNPDF30NNLO_Zee_Mll2Ml_MAXHTPTV280_E_CMS
   DYIds.append(364281)#Sherpa_221_NNPDF30NNLO_Zmumu_Mll2Ml_MAXHTPTV280_E_CMS
   DYIds.append(364282)#Sherpa_221_NNPDF30NNLO_Ztautau_Mll2Ml_MAXHTPTV280_E_CMS


   HiggsIds=[]
   HiggsIds.append(342284)#Pythia8EvtGen_A14NNPDF23LO_WH125_inc
   HiggsIds.append(342285)#Pythia8EvtGen_A14NNPDF23LO_ZH125_inc
   
   SignalIds=[]
   SignalIds.append(393650) #  "C1C1_WW_100p0_1p0";
   SignalIds.append(393651) #  "C1C1_WW_100p0_15p0";
   SignalIds.append(393652) #  "C1C1_WW_100p0_25p0";
   SignalIds.append(393653) #  "C1C1_WW_100p0_50p0";
   SignalIds.append(393654) #  "C1C1_WW_100p0_75p0";
   SignalIds.append(393655) #  "C1C1_WW_150p0_1p0";
   SignalIds.append(393656) #  "C1C1_WW_150p0_50p0";
   SignalIds.append(393657) #  "C1C1_WW_150p0_65p0";
   SignalIds.append(393658) #  "C1C1_WW_150p0_75p0";
   SignalIds.append(393659) #  "C1C1_WW_150p0_100p0";
   SignalIds.append(393661) #  "C1C1_WW_200p0_1p0";
   SignalIds.append(393662) #  "C1C1_WW_200p0_50p0";
   SignalIds.append(393663) #  "C1C1_WW_200p0_100p0";
   SignalIds.append(393664) #  "C1C1_WW_200p0_115p0";
   SignalIds.append(393665) #  "C1C1_WW_200p0_125p0";
   SignalIds.append(393666) #  "C1C1_WW_200p0_150p0";
   SignalIds.append(393668) #  "C1C1_WW_250p0_1p0";
   SignalIds.append(393670) #  "C1C1_WW_250p0_100p0";
   SignalIds.append(393671) #  "C1C1_WW_250p0_150p0";
   SignalIds.append(393672) #  "C1C1_WW_250p0_200p0";
   SignalIds.append(393674) #  "C1C1_WW_300p0_1p0";
   SignalIds.append(393675) #  "C1C1_WW_300p0_50p0";
   SignalIds.append(393676) #  "C1C1_WW_300p0_100p0";
   SignalIds.append(393677) #  "C1C1_WW_300p0_150p0";
   SignalIds.append(393678) #  "C1C1_WW_300p0_200p0";
   SignalIds.append(393679) #  "C1C1_WW_300p0_250p0";
   SignalIds.append(393681) #  "C1C1_WW_400p0_1p0";
   SignalIds.append(393682) #  "C1C1_WW_400p0_50p0";
   SignalIds.append(393683) #  "C1C1_WW_400p0_100p0";
   SignalIds.append(393684) #  "C1C1_WW_400p0_150p0";
   SignalIds.append(393685) #  "C1C1_WW_400p0_200p0";
   SignalIds.append(393686) #  "C1C1_WW_400p0_300p0";
   SignalIds.append(393687) #  "C1C1_WW_400p0_350p0";
   SignalIds.append(393688) #  "C1C1_WW_500p0_1p0";
   SignalIds.append(393689) #  "C1C1_WW_500p0_100p0";
   SignalIds.append(393690) #  "C1C1_WW_500p0_200p0";
   SignalIds.append(393692) #  "C1C1_WW_500p0_400p0";
   SignalIds.append(393693) #  "C1C1_WW_125p0_1p0";
   SignalIds.append(393697) #  "C1C1_WW_175p0_1p0";
   SignalIds.append(395259) # C1C1_WW_100p0_1p0
   SignalIds.append(395264) # C1C1_WW_125p0_1p0
   SignalIds.append(395265) # C1C1_WW_125p0_25p0
   SignalIds.append(395267) # C1C1_WW_150p0_1p0
   SignalIds.append(395268) # C1C1_WW_150p0_25p0
   SignalIds.append(395269) # C1C1_WW_150p0_50p0
   SignalIds.append(395274) # C1C1_WW_175p0_1p0
   SignalIds.append(395275) # C1C1_WW_175p0_25p0
   SignalIds.append(395276) # C1C1_WW_175p0_50p0
   SignalIds.append(395277) # C1C1_WW_175p0_75p0
   SignalIds.append(395278) # C1C1_WW_200p0_1p0
   SignalIds.append(395279) # C1C1_WW_200p0_25p0
   SignalIds.append(395280) # C1C1_WW_200p0_50p0
   SignalIds.append(395281) # C1C1_WW_200p0_75p0
   SignalIds.append(395282) # C1C1_WW_200p0_100p0
   SignalIds.append(395287) # C1C1_WW_225p0_1p0
   SignalIds.append(395288) # C1C1_WW_225p0_25p0
   SignalIds.append(395289) # C1C1_WW_225p0_50p0
   SignalIds.append(395290) # C1C1_WW_225p0_75p0
   SignalIds.append(395291) # C1C1_WW_225p0_100p0
   SignalIds.append(395292) # C1C1_WW_250p0_1p0
   SignalIds.append(395293) # C1C1_WW_250p0_25p0
   SignalIds.append(395294) # C1C1_WW_250p0_50p0
   SignalIds.append(395295) # C1C1_WW_250p0_75p0
   SignalIds.append(395296) # C1C1_WW_250p0_100p0
   SignalIds.append(395297) # C1C1_WW_250p0_150p0
   SignalIds.append(395299) # C1C1_WW_275p0_1p0 
   SignalIds.append(395300) # C1C1_WW_275p0_25p0 
   SignalIds.append(395301) # C1C1_WW_275p0_50p0 
   SignalIds.append(395302) # C1C1_WW_275p0_75p0 
   SignalIds.append(395303) # C1C1_WW_275p0_100p0 
   SignalIds.append(395304) # C1C1_WW_300p0_1p0
   SignalIds.append(395305) # C1C1_WW_300p0_25p0
   SignalIds.append(395306) # C1C1_WW_300p0_50p0
   SignalIds.append(395307) # C1C1_WW_300p0_75p0
   SignalIds.append(395308) # C1C1_WW_300p0_100p0
   SignalIds.append(395309) # C1C1_WW_300p0_125p0
   SignalIds.append(395310) # C1C1_WW_300p0_150p0
   SignalIds.append(395311) # C1C1_WW_300p0_200p0
   SignalIds.append(395312) # C1C1_WW_325p0_1p0
   SignalIds.append(395313) # C1C1_WW_325p0_25p0
   SignalIds.append(395314) # C1C1_WW_325p0_50p0
   SignalIds.append(395315) # C1C1_WW_325p0_100p0
   SignalIds.append(395316) # C1C1_WW_350p0_1p0 
   SignalIds.append(395317) # C1C1_WW_350p0_25p0 
   SignalIds.append(395318) # C1C1_WW_350p0_50p0 
   SignalIds.append(395319) # C1C1_WW_350p0_75p0 
   SignalIds.append(395320) # C1C1_WW_350p0_100p0 
   SignalIds.append(395321) # C1C1_WW_375p0_1p0 
   SignalIds.append(395322) # C1C1_WW_375p0_25p0 
   SignalIds.append(395323) # C1C1_WW_375p0_75p0 
   SignalIds.append(395324) # C1C1_WW_400p0_1p0 
   SignalIds.append(395325) # C1C1_WW_400p0_25p0 
   SignalIds.append(395326) # C1C1_WW_400p0_50p0 
   SignalIds.append(395327) # C1C1_WW_400p0_100p0 
   SignalIds.append(395328) # C1C1_WW_400p0_150p0 
   SignalIds.append(395329) # C1C1_WW_425p0_1p0
   SignalIds.append(395330) # C1C1_WW_425p0_25p0
   SignalIds.append(395331) # C1C1_WW_450p0_1p0 
   SignalIds.append(395332) # C1C1_WW_450p0_50p0 
   SignalIds.append(395333) # C1C1_WW_475p0_1p0 
   SignalIds.append(395334) # C1C1_WW_500p0_1p0 
   SignalIds.append(395335) # C1C1_WW_500p0_100p0
   
   selected_dsid=0
   isTriboson=False
   for dsid in ZjetsIds:
      if str(dsid) in options.inputFile: 
         treename="Others"
         selected_dsid=dsid
   for dsid in SingletopIds:
      if str(dsid) in options.inputFile: 
         treename="Singletop"
         selected_dsid=dsid
   for dsid in OthersIds:
      if str(dsid) in options.inputFile: 
         treename="Others"
         selected_dsid=dsid
   for dsid in ttVIds:
      if str(dsid) in options.inputFile: 
         treename="Others"
         selected_dsid=dsid
   for dsid in ttbarIds:
      if str(dsid) in options.inputFile: 
         treename="ttbar"
         selected_dsid=dsid
   for dsid in HiggsIds:
      if str(dsid) in options.inputFile: 
         treename="Others"
         selected_dsid=dsid
   for dsid in WWIds:
      if str(dsid) in options.inputFile: 
         treename="WW"
         selected_dsid=dsid
   for dsid in WZIds:
      if str(dsid) in options.inputFile: 
         treename="WZ"
         selected_dsid=dsid
   for dsid in ZZIds:
      if str(dsid) in options.inputFile: 
         treename="ZZ"
         selected_dsid=dsid
   for dsid in TribosonsIds:
      if str(dsid) in options.inputFile: 
         treename="Others"
         selected_dsid=dsid
         isTriboson=True
   for dsid in DYIds:
      if str(dsid) in options.inputFile: 
         treename="Others"
         selected_dsid=dsid
   for dsid in SignalIds:
      if str(dsid) in options.inputFile: 
         treename="C1C1_WW"
         selected_dsid=dsid
            

   if selected_dsid==0:
      treename="Unknown"
      selected_dsid=int(inputFilePath.split("/")[-1].split(".")[2])
   xsec_db=open("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/xsdb/mc15_13TeV/Backgrounds.txt")
   lines=xsec_db.readlines()
   selected_xsec=""
   localxsec=0
   for l in lines:
      if str(selected_dsid) in l: selected_xsec=l
   if selected_dsid in SignalIds:
   		if selected_dsid==393650:
   			localxsec = 11611.9 * 0.052647
   			treename = treename + "_100_1"
   		if selected_dsid==393651:
   			localxsec = 11611.9 * 0.051548
   			treename = treename + "_100_15"
   		if selected_dsid==393652:
   			localxsec = 11611.9 * 0.047104
   			treename = treename + "_100_25"
   		if selected_dsid==393653:
   			localxsec = 11611.9 * 0.036461
   			treename = treename + "_100_50"
   		if selected_dsid==393654:
   			localxsec = 11611.9 * 0.0198
   			treename = treename + "_100_75"
   		if selected_dsid==393655:
   			localxsec = 2612.31 * 0.059491
   			treename = treename + "_150_1"
   		if selected_dsid==393656:
   			localxsec = 2612.31 * 0.057929
   			treename = treename + "_150_50"
   		if selected_dsid==393657:
   			localxsec = 2612.31 * 0.056603
   			treename = treename + "_150_65"
   		if selected_dsid==393658:
   			localxsec = 2612.31 * 0.052751
   			treename = treename + "_150_75"
   		if selected_dsid==393659:
   			localxsec = 2612.31 * 0.039846
   			treename = treename + "_150_100"
   		if selected_dsid==393661:
   			localxsec = 902.569 * 0.064868
   			treename = treename + "_200_1"
   		if selected_dsid==393662:
   			localxsec = 902.569 * 0.063972
   			treename = treename + "_200_50"
   		if selected_dsid==393663:
   			localxsec = 902.569 * 0.061268
   			treename = treename + "_200_100"
   		if selected_dsid==393664:
   			localxsec = 902.569 * 0.060184
   			treename = treename + "_200_115"
   		if selected_dsid==393665:
   			localxsec = 902.569 * 0.055429
   			treename = treename + "_200_125"
   		if selected_dsid==393666:
   			localxsec = 902.569 * 0.042754
   			treename = treename + "_200_150"
   		if selected_dsid==393668:
   			localxsec = 387.534 * 0.067444
   			treename = treename + "_250_1"
   		if selected_dsid==393670:
   			localxsec = 387.534 * 0.066589
   			treename = treename + "_250_100"
   		if selected_dsid==393671:
   			localxsec = 387.534 * 0.063534
   			treename = treename + "_250_150"
   		if selected_dsid==393672:
   			localxsec = 387.534 * 0.043659
   			treename = treename + "_250_200"
   		if selected_dsid==393674:
   			localxsec = 190.159 * 0.071427
   			treename = treename + "_300_1"
   		if selected_dsid==393675:
   			localxsec = 190.159 * 0.071455
   			treename = treename + "_300_50"
   		if selected_dsid==393676:
   			localxsec = 190.159 * 0.069315
   			treename = treename + "_300_100"
   		if selected_dsid==393677:
   			localxsec = 190.159 * 0.068464
   			treename = treename + "_300_150"
   		if selected_dsid==393678:
   			localxsec = 190.159 * 0.065195
   			treename = treename + "_300_200"
   		if selected_dsid==393679:
   			localxsec = 190.159 * 0.044733
   			treename = treename + "_300_250"
   		if selected_dsid==393681:
   			localxsec = 58.6311 * 0.078643
   			treename = treename + "_400_1"
   		if selected_dsid==393682:
   			localxsec = 58.6311 * 0.077957
   			treename = treename + "_400_50"
   		if selected_dsid==393683:
   			localxsec = 58.6311 * 0.077315
   			treename = treename + "_400_100"
   		if selected_dsid==393684:
   			localxsec = 58.6311 * 0.077176
   			treename = treename + "_400_150"
   		if selected_dsid==393685:
   			localxsec = 58.6311 * 0.073571
   			treename = treename + "_400_200"
   		if selected_dsid==393686:
   			localxsec = 58.6311 * 0.06616
   			treename = treename + "_400_300"
   		if selected_dsid==393687:
   			localxsec = 58.6311 * 0.045722
   			treename = treename + "_400_350"
   		if selected_dsid==393688:
   			localxsec = 22.1265 * 0.08514
   			treename = treename + "_500_1"
   		if selected_dsid==393689:
   			localxsec = 22.1265 * 0.084366
   			treename = treename + "_500_100"
   		if selected_dsid==393690:
   			localxsec = 22.1265 * 0.081471
   			treename = treename + "_500_200"
   		if selected_dsid==393692:
   			localxsec = 22.1265 * 0.06626
   			treename = treename + "_500_400"
   		if selected_dsid==393693:
   			localxsec = 5090.52 * 0.056097
   			treename = treename + "_125_1"
   		if selected_dsid==393697:
   			localxsec = 1482.42 * 0.061725
   			treename = treename + "_175_1"
   		if selected_dsid==395259:
   			localxsec =  0.42033  *  1214.60474
   			treename = treename + "_100_1"
   		if selected_dsid==395264:
   			localxsec =  0.44216  *  532.468392
   			treename = treename + "_125_1"
   		if selected_dsid==395265:
   			localxsec =  0.44261  *  532.468392
   			treename = treename + "_125_25"
   		if selected_dsid==395267:
   			localxsec =  0.45897  *  273.247626
   			treename = treename + "_150_1"
   		if selected_dsid==395268:
   			localxsec =  0.45435  *  273.247626
   			treename = treename + "_150_25"
   		if selected_dsid==395269:
   			localxsec =  0.45626  *  273.247626
   			treename = treename + "_150_50"
   		if selected_dsid==395274:
   			localxsec =  0.47239  *  155.061132
   			treename = treename + "_175_1"
   		if selected_dsid==395275:
   			localxsec =  0.47247  *  155.061132
   			treename = treename + "_175_25"
   		if selected_dsid==395276:
   			localxsec =  0.47315  *  155.061132
   			treename = treename + "_175_50"
   		if selected_dsid==395277:
   			localxsec =  0.46953  *  155.061132
   			treename = treename + "_175_75"
   		if selected_dsid==395278:
   			localxsec =  0.48398  *  94.4087174
   			treename = treename + "_200_1"
   		if selected_dsid==395279:
   			localxsec =  0.47925  *  94.4087174
   			treename = treename + "_200_25"
   		if selected_dsid==395280:
   			localxsec =  0.4823  *  94.4087174
   			treename = treename + "_200_50"
   		if selected_dsid==395281:
   			localxsec =  0.48083  *  94.4087174
   			treename = treename + "_200_75"
   		if selected_dsid==395282:
   			localxsec =  0.48001  *  94.4087174
   			treename = treename + "_200_100"
   		if selected_dsid==395287:
   			localxsec =  0.49328  *  94.4087174
   			treename = treename + "_225_1"
   		if selected_dsid==395288:
   			localxsec =  0.49174  *  94.4087174
   			treename = treename + "_225_25"
   		if selected_dsid==395289:
   			localxsec =  0.48983  *  94.4087174
   			treename = treename + "_225_50"
   		if selected_dsid==395290:
   			localxsec =  0.48888  *  94.4087174
   			treename = treename + "_225_75"
   		if selected_dsid==395291:
   			localxsec =  0.49059  *  94.4087174
   			treename = treename + "_225_100"
   		if selected_dsid==395292:
   			localxsec =  0.50024  *  40.5360564
   			treename = treename + "_250_1"
   		if selected_dsid==395293:
   			localxsec =  0.5019  *  40.5360564
   			treename = treename + "_250_25"
   		if selected_dsid==395294:
   			localxsec =  0.50122  *  40.5360564
   			treename = treename + "_250_50"
   		if selected_dsid==395295:
   			localxsec =  0.5004  *  40.5360564
   			treename = treename + "_250_75"
   		if selected_dsid==395296:
   			localxsec =  0.49369  *  40.5360564
   			treename = treename + "_250_100"
   		if selected_dsid==395297:
   			localxsec =  0.49126  *  40.5360564
   			treename = treename + "_250_150"
   		if selected_dsid==395299:
   			localxsec =  0.50479  * 40.5360564
   			treename = treename + "_275_1"
   		if selected_dsid==395300:
   			localxsec =  0.50841  * 40.5360564
   			treename = treename + "_275_25" 
   		if selected_dsid==395301:
   			localxsec =  0.50571  * 40.5360564
   			treename = treename + "_275_50" 
   		if selected_dsid==395302:
   			localxsec =  0.50343  * 40.5360564
   			treename = treename + "_275_75" 
   		if selected_dsid==395303:
   			localxsec =  0.50352  * 40.5360564
   			treename = treename + "_275_100" 
   		if selected_dsid==395304:
   			localxsec =  0.51006  *  19.8906314
   			treename = treename + "_300_1"
   		if selected_dsid==395305:
   			localxsec =  0.50637  *  19.8906314
   			treename = treename + "_300_25"
   		if selected_dsid==395306:
   			localxsec =  0.51033  *  19.8906314
   			treename = treename + "_300_50"
   		if selected_dsid==395307:
   			localxsec =  0.50892  *  19.8906314
   			treename = treename + "_300_75"
   		if selected_dsid==395308:
   			localxsec =  0.51245  *  19.8906314
   			treename = treename + "_300_100"
   		if selected_dsid==395309:
   			localxsec =  0.50869  *  19.8906314
   			treename = treename + "_300_125"
   		if selected_dsid==395310:
   			localxsec =  0.50534  *  19.8906314
   			treename = treename + "_300_150"
   		if selected_dsid==395311:
   			localxsec =  0.50031  *  19.8906314
   			treename = treename + "_300_200"
   		if selected_dsid==395312:
   			localxsec =  0.51375  *  14.4437956
   			treename = treename + "_325_1"
   		if selected_dsid==395313:
   			localxsec =  0.5103  *  14.4437956
   			treename = treename + "_325_25"
   		if selected_dsid==395314:
   			localxsec =  0.51557  *  14.4437956
   			treename = treename + "_325_50"
   		if selected_dsid==395315:
   			localxsec =  0.51339  *  14.4437956
   			treename = treename + "_325_100"
   		if selected_dsid==395316:
   			localxsec =  0.52902  *  10.6900154
   			treename = treename + "_350_1"
   		if selected_dsid==395317:
   			localxsec =  0.52349  *  10.6900154
   			treename = treename + "_350_25"
   		if selected_dsid==395318:
   			localxsec =  0.52177  *  10.6900154
   			treename = treename + "_350_50"
   		if selected_dsid==395319:
   			localxsec =  0.52829  *  10.6900154
   			treename = treename + "_350_75" 
   		if selected_dsid==395320:
   			localxsec =  0.51838  *  10.6900154
   			treename = treename + "_350_100" 
   		if selected_dsid==395321:
   			localxsec =  0.52224  *  8.03685732
   			treename = treename + "_375_1"
   		if selected_dsid==395322:
   			localxsec =  0.52355  *  8.03685732
   			treename = treename + "_375_25"
   		if selected_dsid==395323:
   			localxsec =  0.52465  *  8.03685732
   			treename = treename + "_375_75"
   		if selected_dsid==395324:
   			localxsec =  0.5299  * 6.13281306
   			treename = treename + "_400_1"
   		if selected_dsid==395325:
   			localxsec =  0.52643  * 6.13281306
   			treename = treename + "_400_25"
   		if selected_dsid==395326:
   			localxsec =  0.53146  * 6.13281306
   			treename = treename + "_400_50"
   		if selected_dsid==395327:
   			localxsec =  0.52565  * 6.13281306
   			treename = treename + "_400_100"
   		if selected_dsid==395328:
   			localxsec =  0.52807  * 6.13281306
   			treename = treename + "_400_150"
   		if selected_dsid==395329:
   			localxsec =  0.53529  *  4.72989694
   			treename = treename + "_425_1"
   		if selected_dsid==395330:
   			localxsec =  0.53461  *  4.72989694
   			treename = treename + "_425_25"
   		if selected_dsid==395331:
   			localxsec =  0.54007  *  3.69387578
   			treename = treename + "_450_1"
   		if selected_dsid==395332:
   			localxsec =  0.53556  *  3.69387578
   			treename = treename + "_450_50"
   		if selected_dsid==395333:
   			localxsec =  0.53541  * 2.91145732
   			treename = treename + "_475_1"
   		if selected_dsid==395334:
   			localxsec =  0.54127  * 2.3144319
   			treename = treename + "_500_1"
   		if selected_dsid==395335:
   			localxsec =  0.54104  * 2.3144319
   			treename = treename + "_500_100"
   		
   		localxsec=localxsec/1000
   		print(selected_dsid,treename,localxsec)
   else:
   		localxsec=float(selected_xsec.split()[-2])*float(selected_xsec.split()[-3])*float(selected_xsec.split()[-4])
elif dataORmc=="data":
   treename="Data"



outlist=inputFilePath.split("/")[-1].split(".")
outname=outlist[2]+".mc16"+whichMC+"."+outlist[3]+"_"+systname+".root"
#outname=inputFilePath.split("/")[-1].replace("user.amirto.","").replace(".C1C1WW_21.2.12_","").replace("v4A","").replace("v4B","").replace("_output","_syst"+systname).replace("..","")
isThere=False
if not os.path.exists(os.path.join(os.getcwd(),options.outFile)):
    os.makedirs(os.path.join(os.getcwd(),options.outFile))
for e in os.listdir(os.path.join(os.getcwd(),options.outFile)):
   if outname in e: isThere=True 

if isThere:
   logging.warning("Dataset already processed...Exiting")
   exit()

sh.setMetaString("nc_tree", "myTree")

logging.info("creating algorithms")
alg = ROOT.TreeSUSY()

alg.targetLumi = targetLumi
if systname=="WEIGHTS" or systname=="NONE":
   alg.outtree = treename+"_central"
else:
   alg.outtree = treename+"_"+systname
alg.systname = systname
if dataORmc=="MC":
   print(localxsec)
   alg.myEvents = myEvents
   alg.xsec_local = localxsec
   alg.sample_name = samplename
       
alg.dataORmc = dataORmc
alg.verbose=False
alg.whichData = whichData
alg.doCutFlow = bool(options.cutflow)
if dataORmc=="MC": alg.isTriboson = isTriboson
else: alg.isTriboson = False

job = ROOT.EL.Job()
job.sampleHandler(sh)

nevents=options.nevents
if nevents!=0: job.options().setDouble(ROOT.EL.Job.optMaxEvents, nevents)

logging.info("adding algorithms")
job.algsAdd(alg)

logging.info("creating driver")
driver = ROOT.EL.DirectDriver()

logging.info("submit job")
driver.submit(job, options.submitDir)

for e in os.listdir(options.submitDir+"/data-output"):
   file_to_move=e
   

if isThere:
   logging.warning("\nThere is already a sample with the same name in the output directory.\nI'm not overwriting it.\nThe processed sample will be saved in submit_dir/data-output")
else:
   shutil.move(options.submitDir+"/data-output/"+file_to_move, os.path.join(os.getcwd(),options.outFile,outname))
   
shutil.rmtree(options.submitDir, True)

