#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <AsgTools/MessageCheck.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventInfoContainer.h"
#include "xAODEventInfo/EventInfoAuxContainer.h"

#include <LoopSUSY_twoLep/TreeSUSY.h>
#include "EventLoop/OutputStream.h"
#include "LoopSUSYCore/TMctLib.h"
#include "LoopSUSYCore/MT2_ROOT.h"
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"
#include "PATInterfaces/SystematicSet.h"

#include "PathResolver/PathResolver.h"

#include <TSystem.h>
#include <cmath>

#include <cstdlib>
#include <map>
#include <string>
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include <iostream>
#include <algorithm>

// GRL
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "GoodRunsLists/DQHelperFunctions.h"


using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(TreeSUSY)
const double GeV = 1000.;
const float mZ = 91.2;

TreeSUSY :: TreeSUSY ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  //Info("TreeSUSY()","Constructor called");

}



EL::StatusCode TreeSUSY :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  // tell EventLoop about our output:
  EL::OutputStream out ("output");
  job.outputAdd (out);
  LoopSUSYCore::SetSysts(systname);
  LoopSUSYCore::SetType(outtree);
  //std::cout<< LoopSUSYCore::GetSysts() << std::endl;

  //Info("setupJob()","SetupJob called");
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TreeSUSY :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  const char *APP_NAME = "histInitialize()";
  Info( APP_NAME, "registering histos");



  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeSUSY :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeSUSY::initialize ()
{
  const char *APP_NAME = "initialize()";
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  std::cout << "-------------------------------------------------" << std::endl;
  std::cout << "NumberEvents: " << myEvents << " - OutTree: "<< outtree << " - syst: "<< systname << std::endl;
  std::cout << "-------------------------------------------------" << std::endl;
  TFile *file_ntup = wk()->getOutputFile ("output");
  newTree = new TTree();
  newTree->SetName(outtree.c_str());
  newTree->SetDirectory(file_ntup);
  
  newTree->Branch("RunNumber", &RunNumber);

  newTree->Branch("lept1Pt", &lept1Pt);
  newTree->Branch("lept2Pt", &lept2Pt);
  newTree->Branch("lept3Pt", &lept3Pt);
  newTree->Branch("lept1Eta", &lept1Eta);
  newTree->Branch("lept2Eta", &lept2Eta);
  newTree->Branch("lept3Eta", &lept3Eta);
  newTree->Branch("lept1Phi", &lept1Phi);
  newTree->Branch("lept2Phi", &lept2Phi);
  newTree->Branch("lept3Phi", &lept3Phi);
  newTree->Branch("lept1E", &lept1E);
  newTree->Branch("lept2E", &lept2E);
  newTree->Branch("lept3E", &lept3E);
  newTree->Branch("lept1Flav", &lept1Flav);
  newTree->Branch("lept2Flav", &lept2Flav);
  newTree->Branch("lept3Flav", &lept3Flav);

  newTree->Branch("metPhi", &metPhi);
  newTree->Branch("MET", &MET);
  newTree->Branch("metPhi_ele", &metPhi_ele);
  newTree->Branch("MET_ele", &MET_ele);
  newTree->Branch("metPhi_mu", &metPhi_mu);
  newTree->Branch("MET_mu", &MET_mu);
  newTree->Branch("metPhi_jet", &metPhi_jet);
  newTree->Branch("MET_jet", &MET_jet);
  newTree->Branch("metPhi_gam", &metPhi_gam);
  newTree->Branch("MET_gam", &MET_gam);
  newTree->Branch("metPhi_soft", &metPhi_soft);
  newTree->Branch("MET_soft", &MET_soft);
  newTree->Branch("METsig", &METsig);


  newTree->Branch("nLeps", &nLeps);
  
  newTree->Branch("pbllmod", &pbllmod);

  if (dataORmc=="MC"){
    newTree->Branch("eventWeight", &eventWeight);
    newTree->Branch("eventWeightNoPU", &eventWeightNoPU);
  }

  newTree->Branch("L2Mll", &L2Mll);
  newTree->Branch("L2MT2", &L2MT2);
  newTree->Branch("L2isEMU", &L2isEMU);
  newTree->Branch("L2isEE", &L2isEE);
  newTree->Branch("L2isMUMU", &L2isMUMU);

  newTree->Branch("L2nCentralLightJet", &L2nCentralLightJet);
  newTree->Branch("L2nCentralBJet", &L2nCentralBJet);
  newTree->Branch("L2nForwardJet", &L2nForwardJet);
  newTree->Branch("L2nCentralLightJet30", &L2nCentralLightJet30);
  newTree->Branch("L2nCentralBJet30", &L2nCentralBJet30);
  newTree->Branch("L2nForwardJet30", &L2nForwardJet30);
  newTree->Branch("L2nCentralLightJet40", &L2nCentralLightJet40);
  newTree->Branch("L2nCentralBJet40", &L2nCentralBJet40);
  newTree->Branch("L2nForwardJet40", &L2nForwardJet40);
  newTree->Branch("L2nCentralLightJet50", &L2nCentralLightJet50);
  newTree->Branch("L2nCentralBJet50", &L2nCentralBJet50);
  newTree->Branch("L2nForwardJet50", &L2nForwardJet50);
  newTree->Branch("L2nCentralLightJet60", &L2nCentralLightJet60);
  newTree->Branch("L2nCentralBJet60", &L2nCentralBJet60);
  newTree->Branch("L2nForwardJet60", &L2nForwardJet60);
 
  newTree->Branch("jet1Pt", &jet1Pt);
  newTree->Branch("jet2Pt", &jet2Pt);
  newTree->Branch("jet3Pt", &jet3Pt);
  newTree->Branch("jet1Eta", &jet1Eta);
  newTree->Branch("jet2Eta", &jet2Eta);
  newTree->Branch("jet3Eta", &jet3Eta);
  newTree->Branch("jet1Phi", &jet1Phi);
  newTree->Branch("jet2Phi", &jet2Phi);
  newTree->Branch("jet3Phi", &jet3Phi);
  
  newTree->Branch("mu", &AverageInteractionsPerCrossing);
  
  if (systname=="WEIGHTS"){
    newTree->Branch("syst_FT_EFF_B_down", &syst_FT_EFF_B_down);
    newTree->Branch("syst_FT_EFF_B_up", &syst_FT_EFF_B_up);
    newTree->Branch("syst_FT_EFF_C_down", &syst_FT_EFF_C_down);
    newTree->Branch("syst_FT_EFF_C_up", &syst_FT_EFF_C_up);
    newTree->Branch("syst_FT_EFF_Light_down", &syst_FT_EFF_Light_down);
    newTree->Branch("syst_FT_EFF_Light_up", &syst_FT_EFF_Light_up);
    newTree->Branch("syst_FT_EFF_extrapolation_down", &syst_FT_EFF_extrapolation_down);
    newTree->Branch("syst_FT_EFF_extrapolation_up", &syst_FT_EFF_extrapolation_up);
    newTree->Branch("syst_FT_EFF_extrapolation_from_charm_down", &syst_FT_EFF_extrapolation_from_charm_down);
    newTree->Branch("syst_FT_EFF_extrapolation_from_charm_up", &syst_FT_EFF_extrapolation_from_charm_up);
    newTree->Branch("syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down", &syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down);
    newTree->Branch("syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up", &syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up);
    newTree->Branch("syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down", &syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down);
    newTree->Branch("syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up", &syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up);
    newTree->Branch("syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down", &syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down);
    newTree->Branch("syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up", &syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up);
    newTree->Branch("syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down", &syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down);
    newTree->Branch("syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up", &syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up);
    newTree->Branch("syst_MUON_EFF_BADMUON_STAT_down", &syst_MUON_EFF_BADMUON_STAT_down);
    newTree->Branch("syst_MUON_EFF_BADMUON_STAT_up", &syst_MUON_EFF_BADMUON_STAT_up);
    newTree->Branch("syst_MUON_EFF_BADMUON_SYS_down", &syst_MUON_EFF_BADMUON_SYS_down);
    newTree->Branch("syst_MUON_EFF_BADMUON_SYS_up", &syst_MUON_EFF_BADMUON_SYS_up);
    newTree->Branch("syst_MUON_EFF_ISO_STAT_down", &syst_MUON_EFF_ISO_STAT_down);
    newTree->Branch("syst_MUON_EFF_ISO_STAT_up", &syst_MUON_EFF_ISO_STAT_up);
    newTree->Branch("syst_MUON_EFF_ISO_SYS_down", &syst_MUON_EFF_ISO_SYS_down);
    newTree->Branch("syst_MUON_EFF_ISO_SYS_up", &syst_MUON_EFF_ISO_SYS_up);
    newTree->Branch("syst_MUON_EFF_RECO_STAT_down", &syst_MUON_EFF_RECO_STAT_down);
    newTree->Branch("syst_MUON_EFF_RECO_STAT_up", &syst_MUON_EFF_RECO_STAT_up);
    newTree->Branch("syst_MUON_EFF_RECO_STAT_LOWPT_down", &syst_MUON_EFF_RECO_STAT_LOWPT_down);
    newTree->Branch("syst_MUON_EFF_RECO_STAT_LOWPT_up", &syst_MUON_EFF_RECO_STAT_LOWPT_up);
    newTree->Branch("syst_MUON_EFF_RECO_SYS_down", &syst_MUON_EFF_RECO_SYS_down);
    newTree->Branch("syst_MUON_EFF_RECO_SYS_up", &syst_MUON_EFF_RECO_SYS_up);
    newTree->Branch("syst_MUON_EFF_RECO_SYS_LOWPT_down", &syst_MUON_EFF_RECO_SYS_LOWPT_down);
    newTree->Branch("syst_MUON_EFF_RECO_SYS_LOWPT_up", &syst_MUON_EFF_RECO_SYS_LOWPT_up);
    newTree->Branch("syst_MUON_EFF_TTVA_STAT_down", &syst_MUON_EFF_TTVA_STAT_down);
    newTree->Branch("syst_MUON_EFF_TTVA_STAT_up", &syst_MUON_EFF_TTVA_STAT_up);
    newTree->Branch("syst_MUON_EFF_TTVA_SYS_down", &syst_MUON_EFF_TTVA_SYS_down);
    newTree->Branch("syst_MUON_EFF_TTVA_SYS_up", &syst_MUON_EFF_TTVA_SYS_up);
    newTree->Branch("syst_MUON_EFF_TrigStatUncertainty_down", &syst_MUON_EFF_TrigStatUncertainty_down);
    newTree->Branch("syst_MUON_EFF_TrigStatUncertainty_up", &syst_MUON_EFF_TrigStatUncertainty_up);
    newTree->Branch("syst_MUON_EFF_TrigSystUncertainty_down", &syst_MUON_EFF_TrigSystUncertainty_down);
    newTree->Branch("syst_MUON_EFF_TrigSystUncertainty_up", &syst_MUON_EFF_TrigSystUncertainty_up);
    newTree->Branch("syst_JET_JvtEfficiency_1down", &syst_JET_JvtEfficiency_1down);
    newTree->Branch("syst_JET_JvtEfficiency_1up", &syst_JET_JvtEfficiency_1up);
    newTree->Branch("syst_JET_fJvtEfficiency_1down", &syst_JET_fJvtEfficiency_1down);
    newTree->Branch("syst_JET_fJvtEfficiency_1up", &syst_JET_fJvtEfficiency_1up);
  }
  
  //std::cout << "not broke" << std::endl;

  if(doCutFlow) {
    h_cutFlow = new TH1F("cutflow","cutflow", 35, 0, 35);
    h_cutFlow->SetDirectory(file_ntup);
    h_cutFlow->GetXaxis()->SetBinLabel(1,"Start");
    h_cutFlow->GetXaxis()->SetBinLabel(2,"OR");
    h_cutFlow->GetXaxis()->SetBinLabel(3,"Origin");
    h_cutFlow->GetXaxis()->SetBinLabel(4,"2lep");
    h_cutFlow->GetXaxis()->SetBinLabel(5,"OS");
    h_cutFlow->GetXaxis()->SetBinLabel(6,"pt");
    h_cutFlow->GetXaxis()->SetBinLabel(7,"mll");
    h_cutFlow->GetXaxis()->SetBinLabel(8,"DF");
    h_cutFlow->GetXaxis()->SetBinLabel(9,"SF");
    
    h_cutFlow->GetXaxis()->SetBinLabel(10,"DF0b");
    h_cutFlow->GetXaxis()->SetBinLabel(11,"DF0b0j");
    h_cutFlow->GetXaxis()->SetBinLabel(12,"DF0b1j");
    h_cutFlow->GetXaxis()->SetBinLabel(13,"DF0b0jMET");
    h_cutFlow->GetXaxis()->SetBinLabel(14,"DF0b1jMET");
    h_cutFlow->GetXaxis()->SetBinLabel(15,"DF0b0jMETsig");
    h_cutFlow->GetXaxis()->SetBinLabel(16,"DF0b1jMETsig");
    h_cutFlow->GetXaxis()->SetBinLabel(17,"DF0b0jMETsigMT2");
    h_cutFlow->GetXaxis()->SetBinLabel(18,"DF0b1jMETsigMT2");
    
    h_cutFlow->GetXaxis()->SetBinLabel(19,"SFmll");
    h_cutFlow->GetXaxis()->SetBinLabel(20,"SF0b");
    h_cutFlow->GetXaxis()->SetBinLabel(21,"SF0b0j");
    h_cutFlow->GetXaxis()->SetBinLabel(22,"SF0b1j");
    h_cutFlow->GetXaxis()->SetBinLabel(23,"SF0b0jMET");
    h_cutFlow->GetXaxis()->SetBinLabel(24,"SF0b1jMET");
    h_cutFlow->GetXaxis()->SetBinLabel(25,"SF0b0jMETsig");
    h_cutFlow->GetXaxis()->SetBinLabel(26,"SF0b1jMETsig");
    h_cutFlow->GetXaxis()->SetBinLabel(27,"SF0b0jMETsigMT2");
    h_cutFlow->GetXaxis()->SetBinLabel(28,"SF0b1jMETsigMT2");
    
    h_cutFlow->GetXaxis()->SetBinLabel(29,"Trigger");
    h_cutFlow->GetXaxis()->SetBinLabel(30,"Signal");
  }
  
    
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode TreeSUSY::execute ()
{
  

  //const char *APP_NAME = "execute()";


  //The basic idea of this method is to read/compute all the important variables and then move to histogram and tree filling at the end
  //Some initialization and first checks
  
  lept1Pt= 0;
  lept2Pt= 0;
  lept3Pt= 0;
  lept1Phi= 0;
  lept2Phi= 0;
  lept3Phi= 0;
  lept1Eta= 0;
  lept2Eta= 0;
  lept3Eta= 0;
  lept1E= 0;
  lept2E= 0;
  lept3E= 0;
  lept1Flav= 0;
  lept2Flav= 0;
  lept3Flav= 0;
  metPhi= 0;
  MET= 0;
  metPhi_ele= 0;
  MET_ele= 0;
  metPhi_mu= 0;
  MET_mu= 0;
  metPhi_jet= 0;
  MET_jet= 0;
  metPhi_gam= 0;
  MET_gam= 0;
  metPhi_soft= 0;
  MET_soft= 0;
  METsig= 0;
  eventWeight= 0;
  L2Mll= 0;
  L2MT2= 0;
  L2isEMU= 0;
  L2isMUMU= 0;
  L2isEE= 0;
  L2nCentralLightJet= 0;
  L2nCentralBJet= 0;
  pbllmod=0;
  
  
//Start Loop
  tree->GetEntry(wk()->treeEntry());
  
  std::vector<std::vector<double>> leptons;
  std::vector<std::vector<float>> jets;
//This setup the systematic according to systname. It should be checked everytime MelAnalysis or ST is updated
  
  if(xsec==-1){
    xsec=xsec_local;
  }


  eventWeight = WeightEvents * WeightEventsPU * WeightEventsSF_global * WeightEvents_trigger_global * WeightEventsJVT * WeightEventsbTag * targetLumi*xsec/myEvents;
 

  /*for (int i=0; i<(int)ptleptons->size(); i++){
    ppt.push_back(ptleptons->at(i));
  }*/
  
  SetVariation();
  //leptons = SetVariation(0);
  eventWeight = WeightEvents * WeightEventsPU * WeightEventsSF_global * WeightEvents_trigger_global * WeightEventsJVT * WeightEventsbTag * targetLumi*xsec/myEvents;
  eventWeightNoPU = WeightEvents * WeightEventsSF_global * WeightEvents_trigger_global * WeightEventsJVT * WeightEventsbTag * targetLumi*xsec/myEvents;


  if(doCutFlow) h_cutFlow->Fill("Start",eventWeight);
  

//Event Cleaning
  if(sample_name.find("410472") != std::string::npos && GenFiltMET>200000) return EL::StatusCode::SUCCESS;
  if(sample_name.find("407345") != std::string::npos && (GenFiltMET<200000 || GenFiltMET>300000)) return EL::StatusCode::SUCCESS;
  if(sample_name.find("407346") != std::string::npos && (GenFiltMET<300000 || GenFiltMET>400000)) return EL::StatusCode::SUCCESS;
  if(sample_name.find("407347") != std::string::npos && GenFiltMET<400000) return EL::StatusCode::SUCCESS;
  
  if(cleaningVeto) return EL::StatusCode::SUCCESS;
  if(verbose) std::cout << "pass Filt" << endl;


  
  
//Lepton Quality Check
//Tribosons don't have originlep defined, data neither.
  vector<int> v,vOR,vOri;



  if (dataORmc=="MC"){
    for(int i=0;i<(int)ptleptons->size();i++) {
      if(i < (int)passORlep->size()) {
      if(passORlep->at(i)) {
        vOR.push_back(i);
        if (!isTriboson)  {
          if(i < (int)Originlep->size()) {
            if(Originlep->at(i) == 10 || Originlep->at(i) == 12 ||Originlep->at(i) == 13 ||Originlep->at(i) == 14 || Originlep->at(i) == 22 || Originlep->at(i) == 43) {
              vOri.push_back(i);
              if (fabs(flavlep->at(i)) == 11 && fabs(etaleptons->at(i)) < 2.47) v.push_back(i);
              if (fabs(flavlep->at(i)) == 13 && fabs(etaleptons->at(i)) < 2.7)  v.push_back(i);
            }
          }
        }
        else {
          if (fabs(flavlep->at(i)) == 11 && fabs(etaleptons->at(i)) < 2.47) v.push_back(i);
          if (fabs(flavlep->at(i)) == 13 && fabs(etaleptons->at(i)) < 2.7)  v.push_back(i);
        }
      }
    }}
  }
  else if (dataORmc=="data"){
    for(int i=0;i<(int)ptleptons->size();i++){
      if(passORlep->at(i)) {
         vOR.push_back(i);
        if (1==1) {
          if(1==1) {
            if(1==1) {
              vOri.push_back(i);
              if (fabs(flavlep->at(i)) == 11 && fabs(etaleptons->at(i)) < 2.47)v.push_back(i);
              if (fabs(flavlep->at(i)) == 13 && fabs(etaleptons->at(i)) < 2.7)v.push_back(i);
            }
          }
        }
      }
    }
  }
  
  if(vOR.size()<2) return EL::StatusCode::SUCCESS;
  if(verbose) std::cout<< "l OR>=2" << std::endl;
  if(doCutFlow) h_cutFlow->Fill("OR",eventWeight);
  
  if(vOri.size()<2) return EL::StatusCode::SUCCESS;
  if(verbose) std::cout<< "l ORi>=2" << std::endl;
  if(doCutFlow) h_cutFlow->Fill("Origin",eventWeight);

//Exactly 2 or 3 leptons in the event
  nLeps=v.size();
  if(nLeps!=2 && nLeps!=3) return EL::StatusCode::SUCCESS;
  if(verbose) std::cout<< "l==2 or 3" << std::endl;
  
  if(nLeps==2) if(doCutFlow) h_cutFlow->Fill("2lep",eventWeight);


//Definition of Basic Kinematic Varaiables for Leptons  
//I define a single vector of vectors with all the variables in because it makes sorting easier.
//Just remember that lepton.at(i) has all the relevant kinematic of the i-th lepton inside.
  
  /*std::cout << "ptleptons " << ptleptons->size() << std::endl;
  std::cout << "phileptons " << phileptons->size() << std::endl;
  std::cout << "etaleptons " << etaleptons->size() << std::endl;
  std::cout << "massleptons " << massleptons->size() << std::endl;
  std::cout << "flavlep " << flavlep->size() << std::endl;
  std::cout << "isSignallep " << isSignallep->size() << std::endl;
  std::cout << "LepIsoFixedCutLoose " << LepIsoFixedCutLoose->size() << std::endl;*/
  for (int i=0; i<ptleptons->size(); i++){
    std::vector<double> lepton;
    lepton.push_back(ptleptons->at(i)); //pt is leptons.at(i).at(0)
    if (phileptons->size()==ptleptons->size()) lepton.push_back(phileptons->at(i)); 
    else lepton.push_back(0); //phi is leptons.at(i).at(1)
    if (etaleptons->size()==ptleptons->size()) lepton.push_back(etaleptons->at(i)); 
    else lepton.push_back(0); //eta is leptons.at(i).at(2)
    if (massleptons->size()==ptleptons->size()) lepton.push_back(massleptons->at(i));
    else lepton.push_back(0); //mass is leptons.at(i).at(3)
    if (flavlep->size()==ptleptons->size()) lepton.push_back(flavlep->at(i)); 
    else lepton.push_back(0); //flavour is leptons.at(i).at(4). 11 is electrons, 13 is muons
    if (isSignallep->size()==ptleptons->size()) lepton.push_back(isSignallep->at(i));
    else lepton.push_back(0); //Signal is leptons.at(i).at(5)
    if(LepIsoFixedCutLoose->size()==ptleptons->size()) lepton.push_back(LepIsoFixedCutLoose->at(i));
    else lepton.push_back(0);//Isolation is leptons.at(i).at(6)
    leptons.push_back(lepton);
    lepton.clear();
  }


//Order the leptons by pt
  std::sort(leptons.rbegin(),leptons.rend());

//check that the ordering is fine, this loop never fired once because the ordering works.
//Just keep it here in case the apocalypse happens.
  for (int i=1; i<(int)leptons.size(); i++){
    if(leptons.at(i-1).at(0)<leptons.at(i).at(0)){
      std::cout << "WRONG ORDER" << std::endl;
      std::cout << "lep " << i-1 << "; pt: " << leptons.at(i-1).at(0) << "; lep " << i << "; pt: " << leptons.at(i).at(0) << std::endl;
    }
  }
  
//Define standard double variables starting from the vector, the Tree will be filled with these ones.
  lept1Pt=leptons.at(0).at(0);
  lept2Pt=leptons.at(1).at(0);
  lept1Phi=leptons.at(0).at(1);
  lept2Phi=leptons.at(1).at(1);
  lept1Eta=leptons.at(0).at(2);
  lept2Eta=leptons.at(1).at(2);
  lept1Flav=leptons.at(0).at(4);
  lept2Flav=leptons.at(1).at(4);
  if (nLeps==3){
    lept3Pt=leptons.at(2).at(0);
    lept3Phi=leptons.at(2).at(1);
    lept3Eta=leptons.at(2).at(2);
    lept3Flav=leptons.at(2).at(4);
  }


//TRIGGER STRATEGY, it's defined in a funcion at the bottom of the class
  int trigger_return=0;
  trigger_return=Trigger(v);
  
  //if (trigger_return==0) std::cout << trigger_return << std::endl;

  if (trigger_return==0) return EL::StatusCode::SUCCESS;
  
  if (doCutFlow) {if(nLeps==2) { h_cutFlow->Fill("Trigger",eventWeight);}}
  
  if (verbose) std::cout << "Triggered" << std::endl;
  

  
//Signal and Isolation Requests, we want signal lepton that are isolated
  if(!((bool)leptons.at(0).at(5) && (bool)leptons.at(1).at(5))) return EL::StatusCode::SUCCESS;
  if(nLeps==3 && !leptons.at(2).at(5)) return EL::StatusCode::SUCCESS;
  
  if(!((bool)leptons.at(0).at(6) && (bool)leptons.at(1).at(6))) return EL::StatusCode::SUCCESS;
  if(nLeps==3 && !leptons.at(2).at(6)) return EL::StatusCode::SUCCESS;

  if(verbose) std::cout << "Signal and Isolation" << std::endl;
  if(doCutFlow) { if(nLeps==2) {h_cutFlow->Fill("Signal",eventWeight);}}


//DF, SF, SS. We want 2 lepton OS events. 3 lepton events we just ask that not all 3 are SS
  if(fabs(lept1Flav*lept2Flav)==121) L2isEE=1;
  if(fabs(lept1Flav*lept2Flav)==143) L2isEMU=1;
  if(fabs(lept1Flav*lept2Flav)==169) L2isMUMU=1;
  
  if(lept1Flav*lept2Flav > 0 && nLeps==2) return EL::StatusCode::SUCCESS;  
  if(nLeps==3 && ((lept1Flav>0 && lept2Flav>0 && lept3Flav>0) || (lept1Flav<0 && lept2Flav<0 && lept3Flav<0)) ) return EL::StatusCode::SUCCESS;

  if(verbose) std::cout << "Opposite Sign" << std::endl;
  if(doCutFlow) { if(nLeps==2)  {h_cutFlow->Fill("OS",eventWeight);}}

//Extra lepton and MET stuff
  TLorentzVector lept1_lv,lept2_lv,lept3_lv,MET_lv;
  
  lept1_lv.SetPtEtaPhiM(0,0,0,0);
  lept2_lv.SetPtEtaPhiM(0,0,0,0);
  lept3_lv.SetPtEtaPhiM(0,0,0,0);
  
  MET_lv.SetPtEtaPhiM(0,0,0,0);
  
  lept1_lv.SetPtEtaPhiM(lept1Pt,lept1Eta,lept1Phi,leptons.at(0).at(3));
  lept2_lv.SetPtEtaPhiM(lept2Pt,lept2Eta,lept2Phi,leptons.at(1).at(3));
  MET_lv.SetPtEtaPhiM(EtMiss_tst,0,EtMiss_tstPhi,0);

  ComputeMT2 mycalc = ComputeMT2(lept1_lv, lept2_lv, MET_lv, 0., 0.);
  L2MT2=mycalc.Compute();

  if(L2MT2<60000) return EL::StatusCode::SUCCESS;
  
  if (nLeps==2){
  	TVector2 pbll;
    pbll.Set(MET_lv.Px()+lept1_lv.Px()+lept2_lv.Px() , MET_lv.Py()+lept1_lv.Py()+lept2_lv.Py());                           
    pbllmod = pbll.Mod();
  } 
  
  if (nLeps==3) {
    lept3_lv.SetPtEtaPhiM(lept3Pt,lept3Eta,lept3Phi,leptons.at(2).at(3));
    lept3E=lept3_lv.Energy();
  }
  lept1E=lept1_lv.Energy();
  lept2E=lept2_lv.Energy();
  
  L2Mll=(lept1_lv+lept2_lv).M(); 
  
  metPhi=MET_lv.Phi();
  MET=MET_lv.Pt();
  
  MET_ele = EtMiss_Ele;
  metPhi_ele = EtMiss_ElePhi;
  MET_mu = EtMiss_Mu;
  metPhi_mu = EtMiss_MuPhi;
  MET_jet = EtMiss_Jet;
  metPhi_jet = EtMiss_JetPhi;
  MET_gam = EtMiss_Gam;
  metPhi_gam = EtMiss_GamPhi;
  MET_soft = EtMiss_Soft;
  metPhi_soft = EtMiss_SoftPhi;

  METsig=EtMiss_significance;
  
  
//SKIMMING
  if (lept1Pt<25000 || lept2Pt<25000) return EL::StatusCode::SUCCESS;
  if(doCutFlow) {if (nLeps==2) {h_cutFlow->Fill("pt",eventWeight);}}
  
  if (nLeps==3 && lept3Pt<25000) return EL::StatusCode::SUCCESS;
  if (nLeps==2 && L2Mll<25000) return EL::StatusCode::SUCCESS;
  
  if(doCutFlow) {if (nLeps==2) {h_cutFlow->Fill("mll",eventWeight);}}


  
  //jets
  //same idea as the leptons


  for(int i=0;i<(int)ptjets->size();i++){
    if (ptjets->at(i)>20000){
      if(passORjet->size()==ptjets->size() && etajets->size()==ptjets->size()){
        if( passORjet->at(i) && fabs(etajets->at(i))<4.5 ){
          std::vector<float> jet;
          jet.push_back(ptjets->at(i)); //pt is jet.at(i).at(0)
          if (etajets->size()==ptjets->size()) jet.push_back(etajets->at(i)); 
          else jet.push_back(0); //eta is jet.at(i).at(1)
          if (phijets->size()==ptjets->size()) jet.push_back(phijets->at(i)); 
          else jet.push_back(0); //phi is jet.at(i).at(2)
          if (massjets->size()==ptjets->size()) jet.push_back(massjets->at(i));
          else jet.push_back(0); //mass is jet.at(i).at(3)
          if (isB85jets->size()==ptjets->size()) jet.push_back(isB85jets->at(i));
          else jet.push_back(0); //BTAG is jet.at(i).at(4)
            jets.push_back(jet);
            jet.clear();
          }
        }
    }
  }
  
  //sort based on pt
  std::sort(jets.rbegin(),jets.rend());
  
  //count the jets, we really only care about jet multiplicity
  L2nCentralBJet=0;
  L2nCentralLightJet=0;
  L2nForwardJet=0;
  L2nCentralBJet30=0;
  L2nCentralLightJet30=0;
  L2nForwardJet30=0;
  L2nCentralBJet40=0;
  L2nCentralLightJet40=0;
  L2nForwardJet40=0;
  L2nCentralBJet50=0;
  L2nCentralLightJet50=0;
  L2nForwardJet50=0;
  L2nCentralBJet60=0;
  L2nCentralLightJet60=0;
  L2nForwardJet60=0;
  
  for(int i=0;i<(int)jets.size();i++){
    if (fabs(jets.at(i).at(1))<2.4){
      if ((bool)jets.at(i).at(4)){
        L2nCentralBJet++;
      }
      else{
        L2nCentralLightJet++;
      }
    }
    else if (fabs(jets.at(i).at(1))>=2.4 && fabs(jets.at(i).at(1))<4.5){
      L2nForwardJet++;
    }
    else{
      std::cout << "DUDE THIS SHOULDN'T BE HAPPENING. SEND HELP!!!" << std::endl;
    }
  }
  
  for(int i=0;i<(int)jets.size();i++){
    if(jets.at(i).at(0)>30000){
      if (fabs(jets.at(i).at(1))<2.4){
        if ((bool)jets.at(i).at(4)){
          L2nCentralBJet30++;
        }
        else{
          L2nCentralLightJet30++;
        }
      }
      else if (fabs(jets.at(i).at(1))>=2.4 && fabs(jets.at(i).at(1))<4.5){
        L2nForwardJet30++;
      }
    }
    if(jets.at(i).at(0)>40000){
      if (fabs(jets.at(i).at(1))<2.4){
        if ((bool)jets.at(i).at(4)){
          L2nCentralBJet40++;
        }
        else{
          L2nCentralLightJet40++;
        }
      }
      else if (fabs(jets.at(i).at(1))>=2.4 && fabs(jets.at(i).at(1))<4.5){
        L2nForwardJet40++;
      }
    }
    if(jets.at(i).at(0)>50000){
      if (fabs(jets.at(i).at(1))<2.4){
        if ((bool)jets.at(i).at(4)){
          L2nCentralBJet50++;
        }
        else{
          L2nCentralLightJet50++;
        }
      }
      else if (fabs(jets.at(i).at(1))>=2.4 && fabs(jets.at(i).at(1))<4.5){
        L2nForwardJet50++;
      }
    }
    if(jets.at(i).at(0)>60000){
      if (fabs(jets.at(i).at(1))<2.4){
        if ((bool)jets.at(i).at(4)){
          L2nCentralBJet60++;
        }
        else{
          L2nCentralLightJet60++;
        }
      }
      else if (fabs(jets.at(i).at(1))>=2.4 && fabs(jets.at(i).at(1))<4.5){
        L2nForwardJet60++;
      }
    }
  }
  
  for (int i=0; i<3; i++){
   if (i==0){
      if (i<(int)jets.size()){
         jet1Pt=jets.at(i).at(0);
         jet1Eta=jets.at(i).at(1);
         jet1Phi=jets.at(i).at(2);
         jet1B=jets.at(i).at(4);
      }
      else{
         jet1Pt=0;
         jet1Eta=0;
         jet1Phi=0;
         jet1B=0;
      }
   }
   if (i==1){
      if (i<(int)jets.size()){
         jet2Pt=jets.at(i).at(0);
         jet2Eta=jets.at(i).at(1);
         jet2Phi=jets.at(i).at(2);
         jet2B=jets.at(i).at(4);
      }
      else{
         jet2Pt=0;
         jet2Eta=0;
         jet2Phi=0;
         jet2B=0;
      }
   }
   if (i==2){
      if (i<(int)jets.size()){
         jet3Pt=jets.at(i).at(0);
         jet3Eta=jets.at(i).at(1);
         jet3Phi=jets.at(i).at(2);
         jet3B=jets.at(i).at(4);
      }
      else{
         jet3Pt=0;
         jet3Eta=0;
         jet3Phi=0;
         jet3B=0;
      }
   }
  }
  
  if(doCutFlow) {
    if(nLeps==2){
      if(L2isEMU){
        h_cutFlow->Fill("DF",eventWeight);
        if (L2nCentralBJet==0) {
          h_cutFlow->Fill("DF0b",eventWeight);
          if (L2nCentralLightJet==0) {
            h_cutFlow->Fill("DF0b0j",eventWeight);
            if (MET>110000) {
              h_cutFlow->Fill("DF0b0jMET",eventWeight);
              if (METsig>10) {
                h_cutFlow->Fill("DF0b0jMETsig",eventWeight);
                if (L2MT2>100000) {
                  h_cutFlow->Fill("DF0b0jMETsigMT2",eventWeight);
                }
              }
            }
          } else if (L2nCentralLightJet==1) {
            h_cutFlow->Fill("DF0b1j",eventWeight);
            if (MET>110000) {
              h_cutFlow->Fill("DF0b1jMET",eventWeight);
              if (METsig>10) {
                h_cutFlow->Fill("DF0b1jMETsig",eventWeight);
                if (L2MT2>100000) {
                  h_cutFlow->Fill("DF0b1jMETsigMT2",eventWeight);
                }
              }
            }
          }
        }
      } else {
        h_cutFlow->Fill("SF",eventWeight);
        if (fabs(L2Mll-91200)>30000){
          h_cutFlow->Fill("SFmll",eventWeight);
          if (L2nCentralBJet==0) {
            h_cutFlow->Fill("SF0b",eventWeight);
            if (L2nCentralLightJet==0) {
              h_cutFlow->Fill("SF0b0j",eventWeight);
              if (MET>110000) {
                h_cutFlow->Fill("SF0b0jMET",eventWeight);
                if (METsig>10) {
                  h_cutFlow->Fill("SF0b0jMETsig",eventWeight);
                  if (L2MT2>100000) {
                    h_cutFlow->Fill("SF0b0jMETsigMT2",eventWeight);
                  }
                }
              }
            } else if (L2nCentralLightJet==1) {
              h_cutFlow->Fill("SF0b1j",eventWeight);
              if (MET>110000) {
                h_cutFlow->Fill("SF0b1jMET",eventWeight);
                if (METsig>10) {
                  h_cutFlow->Fill("SF0b1jMETsig",eventWeight);
                  if (L2MT2>100000) {
                    h_cutFlow->Fill("SF0b1jMETsigMT2",eventWeight);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  //if (L2MT2<60000) return EL::StatusCode::SUCCESS;
  newTree->Fill();
  if(verbose) std::cout << "-------Filled------" << std::endl;
  return EL::StatusCode::SUCCESS;
  
}



EL::StatusCode TreeSUSY :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeSUSY :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  //delete reader;
 


  const char *APP_NAME = "finalize()";
  Info( APP_NAME, "Distruttore");


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TreeSUSY :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  return EL::StatusCode::SUCCESS;
}

int TreeSUSY::Trigger(vector<int> v){

  //Check the year we are in.
  bool is15=false, is16AD3=false, is16D4L=false, is16=false, is17=false, is18=false;

  if(RunNumber >= 266904 && RunNumber <= 284484) is15=true;
  if(RunNumber >= 296939 && RunNumber <= 302872){
    is16=true;
    is16AD3=true;
  }
  if(RunNumber >= 302919 && RunNumber <= 311481){
    is16=true;
    is16D4L=true;
  }
  if(RunNumber >= 325713 && RunNumber <= 340453) is17=true;
  if(RunNumber >= 348885) is18=true;


  //Populate trigger and trigger matching vectors according to lowest unprescaled triggers for the year we are in.

  vector<vector<bool>* > trigMatchList;
  vector<int> trigList_dilep;
  vector<int> trigList_single;

  if(is15){
    trigList_dilep.push_back(HLT_e17_lhloose_mu14); 
    trigList_dilep.push_back(HLT_2e12_lhloose_L12EM10VH); 
    trigList_dilep.push_back(HLT_mu18_mu8noL1);
    if(dataORmc=="data"){
      trigMatchList.push_back(TrigMatch_e17_lhloose_mu14);
      trigMatchList.push_back(TrigMatch_2e12_lhloose_L12EM10VH);
      trigMatchList.push_back(TrigMatch_mu18_mu8noL1);
    }
    trigList_single.push_back(HLT_e24_lhmedium_L1EM20VH);
    trigList_single.push_back(HLT_mu20_iloose_L1MU15);
  }
  if(is16AD3){
    trigList_dilep.push_back(HLT_e17_lhloose_nod0_mu14); 
    trigList_dilep.push_back(HLT_2e15_lhvloose_nod0_L12EM13VH); 
    trigList_dilep.push_back(HLT_mu20_mu8noL1);
    trigList_single.push_back(HLT_e24_lhtight_nod0_ivarloose);
    if(dataORmc=="data"){
      trigMatchList.push_back(TrigMatch_e17_lhloose_nod0_mu14);
      trigMatchList.push_back(TrigMatch_2e15_lhvloose_nod0_L12EM13VH);
      trigMatchList.push_back(TrigMatch_mu20_mu8noL1);
      if(RunNumber>=297730 && RunNumber<=300279) trigList_single.push_back(HLT_mu24_ivarloose);
      if(RunNumber>=300345) trigList_single.push_back(HLT_mu24_ivarmedium);
    }
    else{
      if(RunNumber>=297730 && RunNumber<=300279) trigList_single.push_back(HLT_mu24_ivarloose_L1MU15);
      if(RunNumber>=300345) trigList_single.push_back(HLT_mu24_ivarmedium);
    }

  }
  if(is16D4L){
    trigList_dilep.push_back(HLT_e17_lhloose_nod0_mu14); 
    trigList_dilep.push_back(HLT_2e17_lhvloose_nod0); 
    trigList_dilep.push_back(HLT_mu22_mu8noL1); 
    if(dataORmc=="data"){
      trigMatchList.push_back(TrigMatch_e17_lhloose_nod0_mu14);
      trigMatchList.push_back(TrigMatch_2e17_lhvloose_nod0);
      trigMatchList.push_back(TrigMatch_mu22_mu8noL1);
    }
    trigList_single.push_back(HLT_e26_lhtight_nod0_ivarloose);
    trigList_single.push_back(HLT_mu24_ivarmedium);
  }
  if(is17 || is18){
    trigList_dilep.push_back(HLT_e17_lhloose_nod0_mu14); 
    trigList_dilep.push_back(HLT_2e24_lhvloose_nod0); 
    trigList_dilep.push_back(HLT_mu22_mu8noL1); 
    if(dataORmc=="data"){
      trigMatchList.push_back(TrigMatch_e17_lhloose_nod0_mu14);
      trigMatchList.push_back(TrigMatch_2e24_lhvloose_nod0);
      trigMatchList.push_back(TrigMatch_mu22_mu8noL1);
    }
    trigList_single.push_back(HLT_e26_lhtight_nod0_ivarloose);
    trigList_single.push_back(HLT_mu26_ivarmedium);
  }
  
  //check triggers. The position in the vectors (defined above) is:
  //SINGLE LEPTON: 0 is for e, 1 is for mu
  //DILEPTON: 0 is for emu, 1 is for ee, 2 is for mumu
  
  if(verbose) std::cout<<"Checking single lepton trigger"<<std::endl;
  if((fabs(lept1Flav)==11 || fabs(lept2Flav)==11 || fabs(lept3Flav)==11) && (bool)trigList_single.at(0)) pass_singlep=true;
  if((fabs(lept1Flav)==13 || fabs(lept2Flav)==13 || fabs(lept3Flav)==13) && (bool)trigList_single.at(1)) pass_singlep=true;
	pass_dilep=false;
  if(dataORmc=="data"){
    if(verbose) std::cout<<"Checking data dilep trigger"<<std::endl;
    //Data requires trigger matching
    
    //2 leptons is easy: just check that both matched the trigger
    //remember: flav is \pm 11 for e^{\pm}, \pm 13 for mu^{\pm}
    if(nLeps==2){
      if(fabs(lept1Flav*lept2Flav)==143){
        if((bool)trigList_dilep.at(0) && trigMatchList.at(0)->at(v.at(0)) && trigMatchList.at(0)->at(v.at(1))) pass_dilep=true;
      }
      if(fabs(lept1Flav*lept2Flav)==121){
        if((bool)trigList_dilep.at(1) && trigMatchList.at(1)->at(v.at(0)) && trigMatchList.at(1)->at(v.at(1))) pass_dilep=true;
      }
      if(fabs(lept1Flav*lept2Flav)==169){
        if((bool)trigList_dilep.at(2) && trigMatchList.at(2)->at(v.at(0)) && trigMatchList.at(2)->at(v.at(1))) pass_dilep=true;
      }
    }
    
    //3 leptons is trickier: more than 1 trigger can fire at the same time
    //for each trigger, check that at least 2 leptons matched that trigger.
    int n_triggered=0;
    
    if(nLeps==3){
      n_triggered=0;
      if((bool)trigList_dilep.at(0)){
        for(int i=0; i<3; i++){
          if (trigMatchList.at(0)->at(v.at(i))) n_triggered++;
        }
        if(n_triggered>=2) pass_dilep=true;
      }
  
      n_triggered=0;
      if((bool)trigList_dilep.at(1)){
        for(int i=0; i<3; i++){
          if (trigMatchList.at(1)->at(v.at(i))) n_triggered++;
        }
        if(n_triggered>=2) pass_dilep=true;
      }
      n_triggered=0;
      if((bool)trigList_dilep.at(2)){
        for(int i=0; i<3; i++){
          if (trigMatchList.at(2)->at(v.at(i))) n_triggered++;
        }
        if(n_triggered>=2) pass_dilep=true;
      }
    }
  }
  
  //MC trigger matching is garantied by ST, just ask for triggers
  if(dataORmc=="MC"){
    if(verbose) std::cout<<"Checking MC dilepton trigger"<<std::endl;
    if(nLeps==2){
      if(fabs(lept1Flav*lept2Flav)==143 && (bool)trigList_dilep.at(0)) pass_dilep=true;
      if(fabs(lept1Flav*lept2Flav)==121 && (bool)trigList_dilep.at(1)) pass_dilep=true;
      if(fabs(lept1Flav*lept2Flav)==169 && (bool)trigList_dilep.at(2)) pass_dilep=true;
    }
    if(nLeps==3){
      int n_e=0, n_m=0;
      if((bool)trigList_dilep.at(0)){
        for(int i=0; i<3; i++){
          if(fabs(flavlep->at(v.at(i)))==11 && ptleptons->at(v.at(i))>25000) n_e++;
          if(fabs(flavlep->at(v.at(i)))==13 && ptleptons->at(v.at(i))>25000) n_m++;
        }
        if(n_e>0 && n_m>0) pass_dilep=true;
      }
      n_e=0; n_m=0;
      if((bool)trigList_dilep.at(1)){
        for(int i=0; i<3; i++){
          if(fabs(flavlep->at(v.at(i)))==11 && ptleptons->at(v.at(i))>25000) n_e++;
          if(fabs(flavlep->at(v.at(i)))==13 && ptleptons->at(v.at(i))>25000) n_m++;
        }
        if(n_e>1) pass_dilep=true;
      }
      n_e=0; n_m=0;
      if((bool)trigList_dilep.at(2)){
        for(int i=0; i<3; i++){
          if(fabs(flavlep->at(v.at(i)))==11 && ptleptons->at(v.at(i))>25000) n_e++;
          if(fabs(flavlep->at(v.at(i)))==13 && ptleptons->at(v.at(i))>25000) n_m++;
        }
        if(n_m>1) pass_dilep=true;
      }
    }
  }
  
  //Next event if no trigger single or dilep
  if (pass_dilep) return 1;
  else return 0;
}


void TreeSUSY::SetVariation(){
  if (systname=="WEIGHTS" && dataORmc=="MC"){
  
  syst_FT_EFF_B_down = (WeightEventsbTag_FT_EFF_B_systematics__1down * WeightEventsPU_FT_EFF_B_systematics__1down) / (WeightEventsbTag * WeightEventsPU);
  syst_FT_EFF_B_up   = (WeightEventsbTag_FT_EFF_B_systematics__1up   * WeightEventsPU_FT_EFF_B_systematics__1up)   / (WeightEventsbTag * WeightEventsPU);
  
  syst_FT_EFF_C_down = (WeightEventsbTag_FT_EFF_C_systematics__1down * WeightEventsPU_FT_EFF_C_systematics__1down) / (WeightEventsbTag * WeightEventsPU);
  syst_FT_EFF_C_up   = (WeightEventsbTag_FT_EFF_C_systematics__1up   * WeightEventsPU_FT_EFF_C_systematics__1up)   / (WeightEventsbTag * WeightEventsPU);
  
  syst_FT_EFF_Light_down = (WeightEventsbTag_FT_EFF_Light_systematics__1down * WeightEventsPU_FT_EFF_Light_systematics__1down) / (WeightEventsbTag * WeightEventsPU);
  syst_FT_EFF_Light_up   = (WeightEventsbTag_FT_EFF_Light_systematics__1up   * WeightEventsPU_FT_EFF_Light_systematics__1up)   / (WeightEventsbTag * WeightEventsPU);
  
  syst_FT_EFF_extrapolation_down = (WeightEventsbTag_FT_EFF_extrapolation__1down * WeightEventsPU_FT_EFF_extrapolation__1down) / (WeightEventsbTag * WeightEventsPU);
  syst_FT_EFF_extrapolation_up   = (WeightEventsbTag_FT_EFF_extrapolation__1up   * WeightEventsPU_FT_EFF_extrapolation__1up)   / (WeightEventsbTag * WeightEventsPU);
  
  syst_FT_EFF_extrapolation_from_charm_down = (WeightEventsbTag_FT_EFF_extrapolation_from_charm__1down * WeightEventsPU_FT_EFF_extrapolation_from_charm__1down) / (WeightEventsbTag * WeightEventsPU);
  syst_FT_EFF_extrapolation_from_charm_up   = (WeightEventsbTag_FT_EFF_extrapolation_from_charm__1up   * WeightEventsPU_FT_EFF_extrapolation_from_charm__1up)   / (WeightEventsbTag * WeightEventsPU);
  
  syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down = (WeightEventsSF_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
  syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up = (WeightEventsSF_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up * WeightEvents_trigger_global_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up) / (WeightEventsSF_global * WeightEvents_trigger_global); 
  
    syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down = (WeightEventsSF_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up   = (WeightEventsSF_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down = (WeightEventsSF_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up   = (WeightEventsSF_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
        
    syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
        
    syst_MUON_EFF_BADMUON_STAT_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_BADMUON_STAT_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global);

    syst_MUON_EFF_BADMUON_SYS_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_BADMUON_SYS_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_ISO_STAT_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_ISO_STAT_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_ISO_SYS_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_ISO_SYS_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_RECO_STAT_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_RECO_STAT_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_RECO_STAT_LOWPT_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_RECO_STAT_LOWPT_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_RECO_SYS_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_RECO_SYS_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_RECO_SYS_LOWPT_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_RECO_SYS_LOWPT_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_TTVA_STAT_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_TTVA_STAT_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_TTVA_SYS_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_TTVA_SYS_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_TrigStatUncertainty_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_TrigStatUncertainty_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_MUON_EFF_TrigSystUncertainty_down = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down) / (WeightEventsSF_global * WeightEvents_trigger_global); 
    syst_MUON_EFF_TrigSystUncertainty_up   = (WeightEventsSF_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up   * WeightEvents_trigger_global_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up)   / (WeightEventsSF_global * WeightEvents_trigger_global); 
    
    syst_JET_JvtEfficiency_1down = (WeightEventsJVT_JET_JvtEfficiency__1down) / (WeightEventsJVT);
    syst_JET_JvtEfficiency_1up   = (WeightEventsJVT_JET_JvtEfficiency__1up) / (WeightEventsJVT);
    
    syst_JET_fJvtEfficiency_1down = (WeightEventsJVT_JET_fJvtEfficiency__1down) / (WeightEventsJVT);
    syst_JET_fJvtEfficiency_1up   = (WeightEventsJVT_JET_fJvtEfficiency__1up) / (WeightEventsJVT);
    
    syst_PU_down = WeightEventsPU_PRW_DATASF__1down / WeightEventsPU ;
    syst_PU_up   = WeightEventsPU_PRW_DATASF__1up / WeightEventsPU ;
  }
}
