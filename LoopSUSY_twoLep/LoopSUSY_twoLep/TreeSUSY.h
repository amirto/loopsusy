#ifndef LoopSUSY_twoLep_H
#define LoopSUSY_twoLep_H

#include "PileupReweighting/PileupReweightingTool.h"

#include <EventLoop/Algorithm.h>
#include <LoopSUSYCore/LoopSUSYCore.h>
#include <LoopSUSYCore/myhistos.h>
#include <SUSYTools/SUSYCrossSection.h>
// #include "DMXSecUtils/InfoHolder.h"
// #include "DMXSecUtils/DMXSecTool.h"
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TH1.h>
#include <TH2.h>
#include <TROOT.h>
#include <TChain.h>
#include <TLorentzVector.h>

#include <string>
#include <vector>
#include <algorithm>
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "GoodRunsLists/DQHelperFunctions.h"

using namespace std;

// GRL
class GoodRunsListSelectionTool;
class DQHelperFunctions;


class TreeSUSY : public LoopSUSYCore
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  GoodRunsListSelectionTool *grl; //!
  //CP::PileupReweightingTool* m_Pileup; //!
  //CP::PileupReweightingTool* m_Pileup_up; //!
  //CP::PileupReweightingTool* m_Pileup_down; //!
  //SUSY::CrossSectionDB *my_XsecDB; //!

public:
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

public:
  TTree *newTree; //! high lumi output tree
  TH1F  *h_cutFlow; //!
  string outtree;
  string dataORmc;
  string whichData;
  string systname;
  double targetLumi;
  double myEvents;  
  double xsec_local;
  string sample_name; 
  bool verbose;
  bool isTriboson;
  bool doCutFlow;
  
  
  	bool			_cleaningVeto; //!
	vector<float>	_ptleptons; //!
	vector<bool>	_passORlep; //!
	vector<int>     _Originlep; //!
	vector<int>     _flavlep; //!
	vector<float>   _etaleptons; //!
	vector<float>   _massleptons; //!
	vector<bool>    _isSignallep; //!
	vector<bool>    _LepIsoFixedCutLoose; //!
	
	vector<float>   _ptjets; //!
	vector<float>   _etajets; //!
	vector<float>   _phijets; //!
	vector<float>   _massjets; //!
	vector<bool>    _isB85jets; //!
	vector<bool>    _passORjet; //!
	double			_WeightEvents; //!
	double 			_WeightEventsPU; //!
	double 			_WeightEventsSF_global; //!
	double 			_WeightEvents_trigger_global; //!
	double 			_WeightEventsJVT; //!
	double			_WeightEventsbTag; //!
	double			_EtMiss_tst; //!
	double			_EtMiss_significance; //!
	double			_EtMiss_tstPhi; //!
  
  double lept1Pt; //!
  double lept2Pt; //!
  double lept3Pt; //!
  double lept1Phi; //!
  double lept2Phi; //!
  double lept3Phi; //!
  double lept1Eta; //!
  double lept2Eta; //!
  double lept3Eta; //!
  double lept1E; //!
  double lept2E; //!
  double lept3E; //!
  double lept1Flav; //!
  double lept2Flav; //!
  double lept3Flav; //!
  double pbllmod; //!
  double metPhi; //!
  double MET; //!
  double metPhi_ele; //!
  double MET_ele; //!
  double metPhi_mu; //!
  double MET_mu; //!
  double metPhi_jet; //!
  double MET_jet; //!
  double metPhi_gam; //!
  double MET_gam; //!
  double metPhi_soft; //!
  double MET_soft; //!
  double METsig; //!
  double eventWeight; //!
  double eventWeightNoPU; //!
  double L2Mll; //!
  double L2MT2; //!
  bool L2isEMU; //!
  bool L2isMUMU; //!
  bool L2isEE; //!
  bool pass_singlep; //!
  bool pass_dilep; //!
  int L2nCentralLightJet; //!
  int L2nCentralBJet; //!
  int L2nForwardJet; //!
  int L2nCentralLightJet30; //!
  int L2nCentralBJet30; //!
  int L2nForwardJet30; //!
  int L2nCentralLightJet40; //!
  int L2nCentralBJet40; //!
  int L2nForwardJet40; //!
  int L2nCentralLightJet50; //!
  int L2nCentralBJet50; //!
  int L2nForwardJet50; //!
  int L2nCentralLightJet60; //!
  int L2nCentralBJet60; //!
  int L2nForwardJet60; //!
  double jet1Pt; //!
  double jet2Pt; //!
  double jet3Pt; //!
  double jet1Phi; //!
  double jet2Phi; //!
  double jet3Phi; //!
  double jet1Eta; //!
  double jet2Eta; //!
  double jet3Eta; //!
  bool jet1B; //!
  bool jet2B; //!
  bool jet3B; //!
  int nLeps; //!
  
  	double syst_FT_EFF_B_down ; //!
	double syst_FT_EFF_B_up   ; //!
	double syst_FT_EFF_C_down ; //!
	double syst_FT_EFF_C_up   ; //!
	double syst_FT_EFF_Light_down ; //!
	double syst_FT_EFF_Light_up   ; //!
	double syst_FT_EFF_extrapolation_down ; //!
	double syst_FT_EFF_extrapolation_up   ; //!
	double syst_FT_EFF_extrapolation_from_charm_down ; //!
	double syst_FT_EFF_extrapolation_from_charm_up   ; //!
	double syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down ; //!
	double syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up ; //!
	double syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down ; //!
	double syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up   ; //!
	double syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down ; //!
	double syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up   ; //!
	double syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down ; //!
	double syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up   ; //!
	double syst_MUON_EFF_BADMUON_STAT_down ; //!
	double syst_MUON_EFF_BADMUON_STAT_up   ; //!
	double syst_MUON_EFF_BADMUON_SYS_down ; //!
	double syst_MUON_EFF_BADMUON_SYS_up   ; //!
	double syst_MUON_EFF_ISO_STAT_down ; //!
	double syst_MUON_EFF_ISO_STAT_up   ; //!
	double syst_MUON_EFF_ISO_SYS_down ; //!
	double syst_MUON_EFF_ISO_SYS_up   ; //!
	double syst_MUON_EFF_RECO_STAT_down ; //!
	double syst_MUON_EFF_RECO_STAT_up   ; //!
	double syst_MUON_EFF_RECO_STAT_LOWPT_down ; //!
	double syst_MUON_EFF_RECO_STAT_LOWPT_up   ; //!
	double syst_MUON_EFF_RECO_SYS_down ; //!
	double syst_MUON_EFF_RECO_SYS_up   ; //!
	double syst_MUON_EFF_RECO_SYS_LOWPT_down ; //!
	double syst_MUON_EFF_RECO_SYS_LOWPT_up   ; //!
	double syst_MUON_EFF_TTVA_STAT_down ; //!
	double syst_MUON_EFF_TTVA_STAT_up   ; //!
	double syst_MUON_EFF_TTVA_SYS_down ; //!
	double syst_MUON_EFF_TTVA_SYS_up   ; //!
	double syst_MUON_EFF_TrigStatUncertainty_down ; //!
	double syst_MUON_EFF_TrigStatUncertainty_up   ; //!
	double syst_MUON_EFF_TrigSystUncertainty_down ; //!
	double syst_MUON_EFF_TrigSystUncertainty_up   ; //!
	double syst_JET_JvtEfficiency_1down ; //!
	double syst_JET_JvtEfficiency_1up   ; //!
	double syst_JET_fJvtEfficiency_1down ; //!
	double syst_JET_fJvtEfficiency_1up   ; //!
	double syst_PU_down ; //!
	double syst_PU_up   ; //!



  TreeSUSY ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // inline void Fill(TH1* histo, float value);
  // inline void Fill(myhistos* histo, int channel, float value);
  // inline void Fill(myhistos* histo, int channel, float value, float value_2);
  // inline void Fill(myhistos* histo, int channel, float value, float value_2, double pesoaddit);
  // inline void Fill(myhistos* histo, int channel, std::vector<float>* value);

  inline int Trigger(vector<int> v);
  inline void SetVariation();

  // this is needed to distribute the algorithm to the workers
  ClassDef(TreeSUSY, 1);
};

#endif

